<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:270557f3-36cf-46df-9cab-7e28678872b1(de.kaialexhiller.canteen.units)">
  <persistence version="9" />
  <languages>
    <use id="a8f77375-6785-47a0-8a3d-142ab816cb53" name="de.kaialexhiller.canteen" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports />
  <registry>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="a8f77375-6785-47a0-8a3d-142ab816cb53" name="de.kaialexhiller.canteen">
      <concept id="1507031990613358785" name="de.kaialexhiller.canteen.structure.Unit" flags="ng" index="1PgFlt">
        <property id="5969773708200350918" name="symbol" index="2wpR58" />
      </concept>
    </language>
  </registry>
  <node concept="1PgFlt" id="5boToVMQyDe">
    <property role="2wpR58" value="g" />
    <property role="TrG5h" value="Gram" />
  </node>
  <node concept="1PgFlt" id="5boToVN25kF">
    <property role="TrG5h" value="Milliliter" />
    <property role="2wpR58" value="ml" />
  </node>
  <node concept="1PgFlt" id="5boToVNhcfE">
    <property role="TrG5h" value="Pieces" />
    <property role="2wpR58" value="pcs" />
  </node>
</model>


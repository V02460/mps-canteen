<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4e321182-9bcb-42b3-94db-47d29f7a99dc(de.kaialexhiller.canteen.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="7862711839422615209" name="jetbrains.mps.lang.structure.structure.DocumentedNodeAnnotation" flags="ng" index="t5JxF">
        <property id="7862711839422615217" name="text" index="t5JxN" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="role_DebugInfo" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="6dt6wfGrpXm">
    <property role="EcuMT" value="7159907586742787926" />
    <property role="TrG5h" value="Ingredient" />
    <property role="19KtqR" value="true" />
    <property role="R4oN_" value="Cooking ingredient" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6dt6wfGrvhM" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="6dt6wfGrvi5" role="1TKVEl">
      <property role="IQ2nx" value="7159907586742809733" />
      <property role="TrG5h" value="diet" />
      <ref role="AX2Wp" node="6dt6wfGrvhS" resolve="Diet" />
    </node>
    <node concept="1TJgyi" id="6dt6wfGrvi8" role="1TKVEl">
      <property role="IQ2nx" value="7159907586742809736" />
      <property role="TrG5h" value="calories" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
      <node concept="t5JxF" id="6dt6wfGrvib" role="lGtFl">
        <property role="t5JxN" value="Calories in kJ per 100g." />
      </node>
    </node>
    <node concept="1TJgyi" id="5boToVN275x" role="1TKVEl">
      <property role="IQ2nx" value="5969773708205125985" />
      <property role="TrG5h" value="to_gram_dividend" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="5boToVN27le" role="1TKVEl">
      <property role="IQ2nx" value="5969773708205126990" />
      <property role="TrG5h" value="to_gram_divisor" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyj" id="5boToVMSz92" role="1TKVEi">
      <property role="IQ2ns" value="5969773708202619458" />
      <property role="20kJfa" value="unit" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5boToVMCwmF" resolve="Unit" />
    </node>
  </node>
  <node concept="1TIwiD" id="6dt6wfGrvhK">
    <property role="EcuMT" value="7159907586742809712" />
    <property role="TrG5h" value="Recipe" />
    <property role="19KtqR" value="true" />
    <property role="R4oN_" value="Recipe for one serving" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="6dt6wfGrvix" role="1TKVEi">
      <property role="IQ2ns" value="7159907586742809761" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="ingredients" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="6dt6wfGrvip" resolve="MeasuredIngedient" />
    </node>
    <node concept="PrWs8" id="6dt6wfGrvhP" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1X3_iC" id="5mCwD4fPyV2" role="lGtFl">
      <property role="3V$3am" value="propertyDeclaration" />
      <property role="3V$3ak" value="c72da2b9-7cce-4447-8389-f407dc1158b7/1169125787135/1071489727084" />
      <node concept="1TJgyi" id="6dt6wfGrvi$" role="8Wnug">
        <property role="IQ2nx" value="7159907586742809764" />
        <property role="TrG5h" value="description" />
        <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
        <node concept="t5JxF" id="6dt6wfGrviD" role="lGtFl">
          <property role="t5JxN" value="Description of the recipe" />
        </node>
      </node>
    </node>
    <node concept="1TJgyi" id="6dt6wfGrviA" role="1TKVEl">
      <property role="IQ2nx" value="7159907586742809766" />
      <property role="TrG5h" value="instructions" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
      <node concept="t5JxF" id="6dt6wfGrviN" role="lGtFl">
        <property role="t5JxN" value="Step-by-step cooking instructions." />
      </node>
    </node>
  </node>
  <node concept="25R3W" id="6dt6wfGrvhS">
    <property role="3F6X1D" value="7159907586742809720" />
    <property role="TrG5h" value="Diet" />
    <node concept="25R33" id="6dt6wfGrvhT" role="25R1y">
      <property role="3tVfz5" value="7159907586742809721" />
      <property role="TrG5h" value="Vegan" />
    </node>
    <node concept="25R33" id="6dt6wfGrvhV" role="25R1y">
      <property role="3tVfz5" value="7159907586742809723" />
      <property role="TrG5h" value="Vegetarian" />
    </node>
    <node concept="25R33" id="6dt6wfGrvhY" role="25R1y">
      <property role="3tVfz5" value="7159907586742809726" />
      <property role="TrG5h" value="Omnivore" />
    </node>
  </node>
  <node concept="1TIwiD" id="6dt6wfGrvip">
    <property role="EcuMT" value="7159907586742809753" />
    <property role="TrG5h" value="MeasuredIngedient" />
    <property role="R4oN_" value="A given amount of an ingredient" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="6dt6wfGrviq" role="1TKVEl">
      <property role="IQ2nx" value="7159907586742809754" />
      <property role="TrG5h" value="amount" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
      <node concept="t5JxF" id="6dt6wfGrvis" role="lGtFl">
        <property role="t5JxN" value="Amount of the given ingredient in the ingredient’s unit." />
      </node>
    </node>
    <node concept="1TJgyj" id="6dt6wfGrviu" role="1TKVEi">
      <property role="IQ2ns" value="7159907586742809758" />
      <property role="20kJfa" value="ingedient" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6dt6wfGrpXm" resolve="Ingredient" />
    </node>
  </node>
  <node concept="1TIwiD" id="5mCwD4fNZLy">
    <property role="EcuMT" value="6172326848858422370" />
    <property role="TrG5h" value="MealPlan" />
    <property role="19KtqR" value="true" />
    <property role="R4oN_" value="Overview list of weekly meal plans" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5mCwD4fO8YZ" role="1TKVEi">
      <property role="IQ2ns" value="6172326848858460095" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="meal_plans" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="5mCwD4fO6Xa" resolve="WeekMealPlan" />
    </node>
    <node concept="1TJgyi" id="7moC6rm8Lp4" role="1TKVEl">
      <property role="IQ2nx" value="8473699062437320260" />
      <property role="TrG5h" value="number_of_persons" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="5mCwD4fO2$5">
    <property role="EcuMT" value="6172326848858433797" />
    <property role="TrG5h" value="Meal" />
    <property role="R4oN_" value="Canteen meal option prepared using the given recipe" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5mCwD4fO2GC" role="1TKVEi">
      <property role="IQ2ns" value="6172326848858434344" />
      <property role="20kJfa" value="recipe" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6dt6wfGrvhK" resolve="Recipe" />
    </node>
  </node>
  <node concept="1TIwiD" id="6dt6wfGrviQ">
    <property role="EcuMT" value="7159907586742809782" />
    <property role="TrG5h" value="DailyMenu" />
    <property role="R4oN_" value="List of meals offered on a given day" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5mCwD4fO2Nb" role="1TKVEi">
      <property role="IQ2ns" value="6172326848858434763" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="meals" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="5mCwD4fO2$5" resolve="Meal" />
    </node>
  </node>
  <node concept="1TIwiD" id="5mCwD4fO6Xa">
    <property role="EcuMT" value="6172326848858451786" />
    <property role="TrG5h" value="WeekMealPlan" />
    <property role="R4oN_" value="Meal plan for a specified (work) week" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5mCwD4fOH8F" role="1TKVEi">
      <property role="IQ2ns" value="6172326848858608171" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="monday" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6dt6wfGrviQ" resolve="DailyMenu" />
    </node>
    <node concept="1TJgyj" id="5mCwD4fOHfe" role="1TKVEi">
      <property role="IQ2ns" value="6172326848858608590" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="tuesday" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6dt6wfGrviQ" resolve="DailyMenu" />
    </node>
    <node concept="1TJgyj" id="5mCwD4fOHk$" role="1TKVEi">
      <property role="IQ2ns" value="6172326848858608932" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="wednesday" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6dt6wfGrviQ" resolve="DailyMenu" />
    </node>
    <node concept="1TJgyj" id="5mCwD4fOHuQ" role="1TKVEi">
      <property role="IQ2ns" value="6172326848858609590" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="thursday" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6dt6wfGrviQ" resolve="DailyMenu" />
    </node>
    <node concept="1TJgyj" id="5mCwD4fOHAf" role="1TKVEi">
      <property role="IQ2ns" value="6172326848858610063" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="friday" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="6dt6wfGrviQ" resolve="DailyMenu" />
    </node>
    <node concept="1TJgyi" id="5mCwD4fO8zh" role="1TKVEl">
      <property role="IQ2nx" value="6172326848858458321" />
      <property role="TrG5h" value="year" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="5mCwD4fO8Ed" role="1TKVEl">
      <property role="IQ2nx" value="6172326848858458765" />
      <property role="TrG5h" value="calendar_week" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="7moC6rm27Me">
    <property role="EcuMT" value="8473699062435576974" />
    <property role="TrG5h" value="Pantry" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7moC6rm27TX" role="1TKVEi">
      <property role="IQ2ns" value="8473699062435577469" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="staples" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="6dt6wfGrvip" resolve="MeasuredIngedient" />
    </node>
  </node>
  <node concept="1TIwiD" id="5boToVMCwmF">
    <property role="TrG5h" value="Unit" />
    <property role="EcuMT" value="1507031990613358785" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="5boToVMJTj6" role="1TKVEl">
      <property role="IQ2nx" value="5969773708200350918" />
      <property role="TrG5h" value="symbol" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="5boToVMQyLo" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
</model>


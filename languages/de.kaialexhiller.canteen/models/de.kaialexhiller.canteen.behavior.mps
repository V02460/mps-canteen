<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:04ec1dc0-bcb1-4130-ab2a-ea1eb63d2e83(de.kaialexhiller.canteen.behavior)">
  <persistence version="9" />
  <languages>
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="19" />
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="2" />
    <use id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc" version="2" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="u6v3" ref="r:4e321182-9bcb-42b3-94db-47d29f7a99dc(de.kaialexhiller.canteen.structure)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="cdrp" ref="r:270557f3-36cf-46df-9cab-7e28678872b1(de.kaialexhiller.canteen.units)" />
    <import index="mhbf" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.model(MPS.OpenAPI/)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="6496299201655527393" name="jetbrains.mps.lang.behavior.structure.LocalBehaviorMethodCall" flags="nn" index="BsUDl" />
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
      <concept id="1703835097132541506" name="jetbrains.mps.lang.behavior.structure.ThisConceptExpression" flags="ng" index="1fM9EW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1215695189714" name="jetbrains.mps.baseLanguage.structure.PlusAssignmentExpression" flags="nn" index="d57v9" />
      <concept id="1153422305557" name="jetbrains.mps.baseLanguage.structure.LessThanOrEqualsExpression" flags="nn" index="2dkUwp" />
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1076505808687" name="jetbrains.mps.baseLanguage.structure.WhileStatement" flags="nn" index="2$JKZl">
        <child id="1076505808688" name="condition" index="2$JKZa" />
      </concept>
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1095950406618" name="jetbrains.mps.baseLanguage.structure.DivExpression" flags="nn" index="FJ1c_" />
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534513062" name="jetbrains.mps.baseLanguage.structure.DoubleType" flags="in" index="10P55v" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1092119917967" name="jetbrains.mps.baseLanguage.structure.MulExpression" flags="nn" index="17qRlL" />
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1225271283259" name="jetbrains.mps.baseLanguage.structure.NPEEqualsExpression" flags="nn" index="17R0WA" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1111509017652" name="jetbrains.mps.baseLanguage.structure.FloatingPointConstant" flags="nn" index="3b6qkQ">
        <property id="1113006610751" name="value" index="$nhwW" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="1082113931046" name="jetbrains.mps.baseLanguage.structure.ContinueStatement" flags="nn" index="3N13vt" />
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="8356039341262087992" name="line" index="1aUNEU" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1200830824066" name="jetbrains.mps.baseLanguage.closures.structure.YieldStatement" flags="nn" index="2n63Yl">
        <child id="1200830928149" name="expression" index="2n6tg2" />
      </concept>
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
      <concept id="1225797177491" name="jetbrains.mps.baseLanguage.closures.structure.InvokeFunctionOperation" flags="nn" index="1Bd96e" />
    </language>
    <language id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc">
      <concept id="5858074156537516430" name="jetbrains.mps.baseLanguage.javadoc.structure.ReturnBlockDocTag" flags="ng" index="x79VA">
        <property id="5858074156537516431" name="text" index="x79VB" />
      </concept>
      <concept id="6832197706140518104" name="jetbrains.mps.baseLanguage.javadoc.structure.DocMethodParameterReference" flags="ng" index="zr_55" />
      <concept id="6832197706140518103" name="jetbrains.mps.baseLanguage.javadoc.structure.BaseParameterReference" flags="ng" index="zr_5a">
        <reference id="6832197706140518108" name="param" index="zr_51" />
      </concept>
      <concept id="5349172909345501395" name="jetbrains.mps.baseLanguage.javadoc.structure.BaseDocComment" flags="ng" index="P$AiS">
        <child id="8465538089690331502" name="body" index="TZ5H$" />
        <child id="5383422241790532083" name="tags" index="3nqlJM" />
      </concept>
      <concept id="5349172909345532724" name="jetbrains.mps.baseLanguage.javadoc.structure.MethodDocComment" flags="ng" index="P$JXv" />
      <concept id="8465538089690881930" name="jetbrains.mps.baseLanguage.javadoc.structure.ParameterBlockDocTag" flags="ng" index="TUZQ0">
        <property id="8465538089690881934" name="text" index="TUZQ4" />
        <child id="6832197706140518123" name="parameter" index="zr_5Q" />
      </concept>
      <concept id="8465538089690331500" name="jetbrains.mps.baseLanguage.javadoc.structure.CommentLine" flags="ng" index="TZ5HA">
        <child id="8970989240999019149" name="part" index="1dT_Ay" />
      </concept>
      <concept id="8970989240999019143" name="jetbrains.mps.baseLanguage.javadoc.structure.TextCommentLinePart" flags="ng" index="1dT_AC">
        <property id="8970989240999019144" name="text" index="1dT_AB" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
        <child id="1595412875168045827" name="initValue" index="28nt2d" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="8182547171709752110" name="jetbrains.mps.lang.quotation.structure.NodeBuilderExpression" flags="nn" index="36biLy">
        <child id="8182547171709752112" name="expression" index="36biLW" />
      </concept>
    </language>
    <language id="446c26eb-2b7b-4bf0-9b35-f83fa582753e" name="jetbrains.mps.lang.modelapi">
      <concept id="4733039728785194814" name="jetbrains.mps.lang.modelapi.structure.NamedNodeReference" flags="ng" index="ZC_QK">
        <reference id="7256306938026143658" name="target" index="2aWVGs" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7400021826774799413" name="jetbrains.mps.lang.smodel.structure.NodePointerExpression" flags="ng" index="2tJFMh">
        <child id="7400021826774799510" name="ref" index="2tJFKM" />
      </concept>
      <concept id="4065387505485742749" name="jetbrains.mps.lang.smodel.structure.AbstractPointerResolveOperation" flags="ng" index="2yCiFS">
        <child id="3648723375513868575" name="repositoryArg" index="Vysub" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1143234257716" name="jetbrains.mps.lang.smodel.structure.Node_GetModelOperation" flags="nn" index="I4A8Y" />
      <concept id="1145383075378" name="jetbrains.mps.lang.smodel.structure.SNodeListType" flags="in" index="2I9FWS">
        <reference id="1145383142433" name="elementConcept" index="2I9WkF" />
      </concept>
      <concept id="1145404486709" name="jetbrains.mps.lang.smodel.structure.SemanticDowncastExpression" flags="nn" index="2JrnkZ">
        <child id="1145404616321" name="leftExpression" index="2JrQYb" />
      </concept>
      <concept id="1966870290088668512" name="jetbrains.mps.lang.smodel.structure.Enum_MemberLiteral" flags="ng" index="2ViDtV">
        <reference id="1966870290088668516" name="memberDeclaration" index="2ViDtZ" />
      </concept>
      <concept id="3648723375513868532" name="jetbrains.mps.lang.smodel.structure.NodePointer_ResolveOperation" flags="ng" index="Vyspw" />
      <concept id="1240170042401" name="jetbrains.mps.lang.smodel.structure.SEnumerationMemberType" flags="in" index="2ZThk1">
        <reference id="1240170836027" name="enum" index="2ZWj4r" />
      </concept>
      <concept id="3562215692195599741" name="jetbrains.mps.lang.smodel.structure.SLinkImplicitSelect" flags="nn" index="13MTOL">
        <reference id="3562215692195600259" name="link" index="13MTZf" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <property id="1238684351431" name="asCast" index="1BlNFB" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
      <concept id="2453008993612717253" name="jetbrains.mps.lang.smodel.structure.EnumSwitchCaseBody_Expression" flags="ng" index="3X5gDF">
        <child id="2453008993612717254" name="expression" index="3X5gDC" />
      </concept>
      <concept id="2453008993612559843" name="jetbrains.mps.lang.smodel.structure.EnumSwitchCase" flags="ng" index="3X5Udd">
        <child id="2453008993612717146" name="body" index="3X5gFO" />
        <child id="2453008993612559844" name="members" index="3X5Uda" />
      </concept>
      <concept id="2453008993612559839" name="jetbrains.mps.lang.smodel.structure.EnumSwitchExpression" flags="ng" index="3X5UdL">
        <child id="2453008993612714935" name="cases" index="3X5gkp" />
        <child id="2453008993612559840" name="enumExpression" index="3X5Ude" />
      </concept>
      <concept id="5779574625830813396" name="jetbrains.mps.lang.smodel.structure.EnumerationIdRefExpression" flags="ng" index="1XH99k">
        <reference id="5779574625830813397" name="enumDeclaration" index="1XH99l" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="c7fb639f-be78-4307-89b0-b5959c3fa8c8" name="jetbrains.mps.lang.text">
      <concept id="155656958578482948" name="jetbrains.mps.lang.text.structure.Word" flags="nn" index="3oM_SD">
        <property id="155656958578482949" name="value" index="3oM_SC" />
      </concept>
      <concept id="2535923850359271782" name="jetbrains.mps.lang.text.structure.Line" flags="nn" index="1PaTwC">
        <child id="2535923850359271783" name="elements" index="1PaTwD" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1176906603202" name="jetbrains.mps.baseLanguage.collections.structure.BinaryOperation" flags="nn" index="56pJg">
        <child id="1176906787974" name="rightExpression" index="576Qk" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1176921879268" name="jetbrains.mps.baseLanguage.collections.structure.IntersectOperation" flags="nn" index="60FfQ" />
      <concept id="1172650591544" name="jetbrains.mps.baseLanguage.collections.structure.SkipOperation" flags="nn" index="7r0gD">
        <child id="1172658456740" name="elementsToSkip" index="7T0AP" />
      </concept>
      <concept id="1172667724288" name="jetbrains.mps.baseLanguage.collections.structure.PageOperation" flags="nn" index="8snch">
        <child id="1172667737868" name="fromElement" index="8sqot" />
        <child id="1172667748353" name="toElement" index="8st4g" />
      </concept>
      <concept id="1226511727824" name="jetbrains.mps.baseLanguage.collections.structure.SetType" flags="in" index="2hMVRd">
        <child id="1226511765987" name="elementType" index="2hN53Y" />
      </concept>
      <concept id="1226516258405" name="jetbrains.mps.baseLanguage.collections.structure.HashSetCreator" flags="nn" index="2i4dXS" />
      <concept id="1207233427108" name="jetbrains.mps.baseLanguage.collections.structure.MapRemoveOperation" flags="nn" index="kI3uX">
        <child id="1207233489861" name="key" index="kIiFs" />
      </concept>
      <concept id="1224414427926" name="jetbrains.mps.baseLanguage.collections.structure.SequenceCreator" flags="nn" index="kMnCb">
        <child id="1224414456414" name="elementType" index="kMuH3" />
        <child id="1224414466839" name="initializer" index="kMx8a" />
      </concept>
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1235566554328" name="jetbrains.mps.baseLanguage.collections.structure.AnyOperation" flags="nn" index="2HwmR7" />
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
        <child id="1237731803878" name="copyFrom" index="I$8f6" />
      </concept>
      <concept id="1201306600024" name="jetbrains.mps.baseLanguage.collections.structure.ContainsKeyOperation" flags="nn" index="2Nt0df">
        <child id="1201654602639" name="key" index="38cxEo" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1160666733551" name="jetbrains.mps.baseLanguage.collections.structure.AddAllElementsOperation" flags="nn" index="X8dFx" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1201792049884" name="jetbrains.mps.baseLanguage.collections.structure.TranslateOperation" flags="nn" index="3goQfb" />
      <concept id="1197683403723" name="jetbrains.mps.baseLanguage.collections.structure.MapType" flags="in" index="3rvAFt">
        <child id="1197683466920" name="keyType" index="3rvQeY" />
        <child id="1197683475734" name="valueType" index="3rvSg0" />
      </concept>
      <concept id="1197686869805" name="jetbrains.mps.baseLanguage.collections.structure.HashMapCreator" flags="nn" index="3rGOSV">
        <child id="1197687026896" name="keyType" index="3rHrn6" />
        <child id="1197687035757" name="valueType" index="3rHtpV" />
        <child id="1206655950512" name="initializer" index="3Mj9YC" />
      </concept>
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
      <concept id="1202128969694" name="jetbrains.mps.baseLanguage.collections.structure.SelectOperation" flags="nn" index="3$u5V9" />
      <concept id="1240824834947" name="jetbrains.mps.baseLanguage.collections.structure.ValueAccessOperation" flags="nn" index="3AV6Ez" />
      <concept id="1240825616499" name="jetbrains.mps.baseLanguage.collections.structure.KeyAccessOperation" flags="nn" index="3AY5_j" />
      <concept id="1197932370469" name="jetbrains.mps.baseLanguage.collections.structure.MapElement" flags="nn" index="3EllGN">
        <child id="1197932505799" name="map" index="3ElQJh" />
        <child id="1197932525128" name="key" index="3ElVtu" />
      </concept>
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
      <concept id="1206655653991" name="jetbrains.mps.baseLanguage.collections.structure.MapInitializer" flags="ng" index="3Mi1_Z">
        <child id="1206655902276" name="entries" index="3MiYds" />
      </concept>
      <concept id="1206655735055" name="jetbrains.mps.baseLanguage.collections.structure.MapEntry" flags="ng" index="3Milgn">
        <child id="1206655844556" name="key" index="3MiK7k" />
        <child id="1206655853135" name="value" index="3MiMdn" />
      </concept>
      <concept id="1180964022718" name="jetbrains.mps.baseLanguage.collections.structure.ConcatOperation" flags="nn" index="3QWeyG" />
    </language>
  </registry>
  <node concept="13h7C7" id="8EeBQEta$O">
    <ref role="13h7C2" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
    <node concept="13i0hz" id="8EeBQEta_7" role="13h7CS">
      <property role="TrG5h" value="diet" />
      <node concept="3Tm1VV" id="8EeBQEta_8" role="1B3o_S" />
      <node concept="3clFbS" id="8EeBQEta_a" role="3clF47">
        <node concept="3clFbJ" id="8EeBQEtyhV" role="3cqZAp">
          <node concept="3clFbS" id="8EeBQEtyhX" role="3clFbx">
            <node concept="3cpWs6" id="8EeBQEt_Sp" role="3cqZAp">
              <node concept="2OqwBi" id="8EeBQEt_Xn" role="3cqZAk">
                <node concept="1XH99k" id="8EeBQEt_Xo" role="2Oq$k0">
                  <ref role="1XH99l" to="u6v3:6dt6wfGrvhS" resolve="Diet" />
                </node>
                <node concept="2ViDtV" id="8EeBQEt_Xp" role="2OqNvi">
                  <ref role="2ViDtZ" to="u6v3:6dt6wfGrvhY" resolve="Omnivore" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="8EeBQEt$eV" role="3clFbw">
            <node concept="2OqwBi" id="8EeBQEtyo2" role="2Oq$k0">
              <node concept="2OqwBi" id="8EeBQEtyo3" role="2Oq$k0">
                <node concept="13iPFW" id="8EeBQEtyo4" role="2Oq$k0" />
                <node concept="3Tsc0h" id="8EeBQEtyo5" role="2OqNvi">
                  <ref role="3TtcxE" to="u6v3:6dt6wfGrvix" resolve="ingredients" />
                </node>
              </node>
              <node concept="13MTOL" id="8EeBQEtyo6" role="2OqNvi">
                <ref role="13MTZf" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
              </node>
            </node>
            <node concept="2HwmR7" id="8EeBQEt$PF" role="2OqNvi">
              <node concept="1bVj0M" id="8EeBQEt$PH" role="23t8la">
                <node concept="3clFbS" id="8EeBQEt$PI" role="1bW5cS">
                  <node concept="3clFbF" id="8EeBQEt$Yg" role="3cqZAp">
                    <node concept="3clFbC" id="8EeBQEt_E3" role="3clFbG">
                      <node concept="2OqwBi" id="8EeBQEt_fq" role="3uHU7B">
                        <node concept="37vLTw" id="8EeBQEt$Yf" role="2Oq$k0">
                          <ref role="3cqZAo" node="8EeBQEt$PJ" resolve="it" />
                        </node>
                        <node concept="3TrcHB" id="8EeBQEt_wb" role="2OqNvi">
                          <ref role="3TsBF5" to="u6v3:6dt6wfGrvi5" resolve="diet" />
                        </node>
                      </node>
                      <node concept="2OqwBi" id="8EeBQEt_Nr" role="3uHU7w">
                        <node concept="1XH99k" id="8EeBQEt_Ns" role="2Oq$k0">
                          <ref role="1XH99l" to="u6v3:6dt6wfGrvhS" resolve="Diet" />
                        </node>
                        <node concept="2ViDtV" id="8EeBQEt_Nt" role="2OqNvi">
                          <ref role="2ViDtZ" to="u6v3:6dt6wfGrvhY" resolve="Omnivore" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="8EeBQEt$PJ" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="8EeBQEt$PK" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="8EeBQEtAeb" role="3cqZAp">
          <node concept="3clFbS" id="8EeBQEtAec" role="3clFbx">
            <node concept="3cpWs6" id="8EeBQEtAed" role="3cqZAp">
              <node concept="2OqwBi" id="8EeBQEtAee" role="3cqZAk">
                <node concept="1XH99k" id="8EeBQEtAef" role="2Oq$k0">
                  <ref role="1XH99l" to="u6v3:6dt6wfGrvhS" resolve="Diet" />
                </node>
                <node concept="2ViDtV" id="8EeBQEtAeg" role="2OqNvi">
                  <ref role="2ViDtZ" to="u6v3:6dt6wfGrvhV" resolve="Vegetarian" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="8EeBQEtAeh" role="3clFbw">
            <node concept="2OqwBi" id="8EeBQEtAei" role="2Oq$k0">
              <node concept="2OqwBi" id="8EeBQEtAej" role="2Oq$k0">
                <node concept="13iPFW" id="8EeBQEtAek" role="2Oq$k0" />
                <node concept="3Tsc0h" id="8EeBQEtAel" role="2OqNvi">
                  <ref role="3TtcxE" to="u6v3:6dt6wfGrvix" resolve="ingredients" />
                </node>
              </node>
              <node concept="13MTOL" id="8EeBQEtAem" role="2OqNvi">
                <ref role="13MTZf" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
              </node>
            </node>
            <node concept="2HwmR7" id="8EeBQEtAen" role="2OqNvi">
              <node concept="1bVj0M" id="8EeBQEtAeo" role="23t8la">
                <node concept="3clFbS" id="8EeBQEtAep" role="1bW5cS">
                  <node concept="3clFbF" id="8EeBQEtAeq" role="3cqZAp">
                    <node concept="3clFbC" id="8EeBQEtAer" role="3clFbG">
                      <node concept="2OqwBi" id="8EeBQEtAes" role="3uHU7B">
                        <node concept="37vLTw" id="8EeBQEtAet" role="2Oq$k0">
                          <ref role="3cqZAo" node="8EeBQEtAey" resolve="it" />
                        </node>
                        <node concept="3TrcHB" id="8EeBQEtAeu" role="2OqNvi">
                          <ref role="3TsBF5" to="u6v3:6dt6wfGrvi5" resolve="diet" />
                        </node>
                      </node>
                      <node concept="2OqwBi" id="8EeBQEtAev" role="3uHU7w">
                        <node concept="1XH99k" id="8EeBQEtAew" role="2Oq$k0">
                          <ref role="1XH99l" to="u6v3:6dt6wfGrvhS" resolve="Diet" />
                        </node>
                        <node concept="2ViDtV" id="8EeBQEtAex" role="2OqNvi">
                          <ref role="2ViDtZ" to="u6v3:6dt6wfGrvhV" resolve="Vegetarian" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="8EeBQEtAey" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="8EeBQEtAez" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="8EeBQEtBjp" role="3cqZAp">
          <node concept="2OqwBi" id="8EeBQEtBHx" role="3cqZAk">
            <node concept="1XH99k" id="8EeBQEtBHy" role="2Oq$k0">
              <ref role="1XH99l" to="u6v3:6dt6wfGrvhS" resolve="Diet" />
            </node>
            <node concept="2ViDtV" id="8EeBQEtBHz" role="2OqNvi">
              <ref role="2ViDtZ" to="u6v3:6dt6wfGrvhT" resolve="Vegan" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2ZThk1" id="8EeBQEtitJ" role="3clF45">
        <ref role="2ZWj4r" to="u6v3:6dt6wfGrvhS" resolve="Diet" />
      </node>
    </node>
    <node concept="13i0hz" id="8EeBQEukI3" role="13h7CS">
      <property role="TrG5h" value="diet_emoji" />
      <node concept="3Tm1VV" id="8EeBQEukI4" role="1B3o_S" />
      <node concept="17QB3L" id="8EeBQEul9f" role="3clF45" />
      <node concept="3clFbS" id="8EeBQEukI6" role="3clF47">
        <node concept="3cpWs8" id="8EeBQEtS5J" role="3cqZAp">
          <node concept="3cpWsn" id="8EeBQEtS5M" role="3cpWs9">
            <property role="TrG5h" value="ret" />
            <node concept="17QB3L" id="8EeBQEtS5H" role="1tU5fm" />
            <node concept="Xl_RD" id="8EeBQEtU6z" role="33vP2m">
              <property role="Xl_RC" value="" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="8EeBQEtDwh" role="3cqZAp">
          <node concept="3X5UdL" id="8EeBQEtDwf" role="3clFbG">
            <node concept="3X5Udd" id="8EeBQEtEtR" role="3X5gkp">
              <node concept="21nZrQ" id="8EeBQEtEtQ" role="3X5Uda">
                <ref role="21nZrZ" to="u6v3:6dt6wfGrvhT" resolve="Vegan" />
              </node>
              <node concept="3X5gDF" id="8EeBQEtELz" role="3X5gFO">
                <node concept="37vLTI" id="8EeBQEtTEA" role="3X5gDC">
                  <node concept="Xl_RD" id="8EeBQEtTGv" role="37vLTx">
                    <property role="Xl_RC" value="🌱" />
                  </node>
                  <node concept="37vLTw" id="8EeBQEtTh3" role="37vLTJ">
                    <ref role="3cqZAo" node="8EeBQEtS5M" resolve="ret" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3X5Udd" id="8EeBQEtEv2" role="3X5gkp">
              <node concept="21nZrQ" id="8EeBQEtEv3" role="3X5Uda">
                <ref role="21nZrZ" to="u6v3:6dt6wfGrvhV" resolve="Vegetarian" />
              </node>
              <node concept="3X5gDF" id="8EeBQEtEJy" role="3X5gFO">
                <node concept="37vLTI" id="8EeBQEtTXk" role="3X5gDC">
                  <node concept="37vLTw" id="8EeBQEtTYZ" role="37vLTJ">
                    <ref role="3cqZAo" node="8EeBQEtS5M" resolve="ret" />
                  </node>
                  <node concept="Xl_RD" id="8EeBQEtEJx" role="37vLTx">
                    <property role="Xl_RC" value="🧀" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3X5Udd" id="8EeBQEtED4" role="3X5gkp">
              <node concept="21nZrQ" id="8EeBQEtED5" role="3X5Uda">
                <ref role="21nZrZ" to="u6v3:6dt6wfGrvhY" resolve="Omnivore" />
              </node>
              <node concept="3X5gDF" id="8EeBQEtTLu" role="3X5gFO">
                <node concept="37vLTI" id="8EeBQEtTNd" role="3X5gDC">
                  <node concept="Xl_RD" id="8EeBQEtTOV" role="37vLTx">
                    <property role="Xl_RC" value="" />
                  </node>
                  <node concept="37vLTw" id="8EeBQEtTLs" role="37vLTJ">
                    <ref role="3cqZAo" node="8EeBQEtS5M" resolve="ret" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="8EeBQEumsP" role="3X5Ude">
              <node concept="13iPFW" id="8EeBQEumhR" role="2Oq$k0" />
              <node concept="2qgKlT" id="8EeBQEumWQ" role="2OqNvi">
                <ref role="37wK5l" node="8EeBQEta_7" resolve="diet" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="8EeBQEtSAK" role="3cqZAp">
          <node concept="37vLTw" id="8EeBQEtSPz" role="3cqZAk">
            <ref role="3cqZAo" node="8EeBQEtS5M" resolve="ret" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="8EeBQEuDge" role="13h7CS">
      <property role="TrG5h" value="calories" />
      <node concept="3Tm1VV" id="8EeBQEuDgf" role="1B3o_S" />
      <node concept="10Oyi0" id="8EeBQEuDM6" role="3clF45" />
      <node concept="3clFbS" id="8EeBQEuDgh" role="3clF47">
        <node concept="3cpWs8" id="8EeBQEuDNC" role="3cqZAp">
          <node concept="3cpWsn" id="8EeBQEuDNF" role="3cpWs9">
            <property role="TrG5h" value="calories" />
            <node concept="10Oyi0" id="8EeBQEuDNB" role="1tU5fm" />
            <node concept="3cmrfG" id="8EeBQEuDR$" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="8EeBQEuDWp" role="3cqZAp">
          <node concept="2GrKxI" id="8EeBQEuDWr" role="2Gsz3X">
            <property role="TrG5h" value="ingredient" />
          </node>
          <node concept="2OqwBi" id="8EeBQEuEgB" role="2GsD0m">
            <node concept="13iPFW" id="8EeBQEuE5k" role="2Oq$k0" />
            <node concept="3Tsc0h" id="8EeBQEuEMq" role="2OqNvi">
              <ref role="3TtcxE" to="u6v3:6dt6wfGrvix" resolve="ingredients" />
            </node>
          </node>
          <node concept="3clFbS" id="8EeBQEuDWv" role="2LFqv$">
            <node concept="3clFbF" id="8EeBQEuEPE" role="3cqZAp">
              <node concept="d57v9" id="8EeBQEuFTd" role="3clFbG">
                <node concept="37vLTw" id="8EeBQEuEPD" role="37vLTJ">
                  <ref role="3cqZAo" node="8EeBQEuDNF" resolve="calories" />
                </node>
                <node concept="2OqwBi" id="5boToVNhjdu" role="37vLTx">
                  <node concept="2GrUjf" id="5boToVNhj1g" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="8EeBQEuDWr" resolve="ingredient" />
                  </node>
                  <node concept="2qgKlT" id="5boToVNhjqu" role="2OqNvi">
                    <ref role="37wK5l" node="5boToVNcH3S" resolve="calories" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="8EeBQEuIcw" role="3cqZAp">
          <node concept="37vLTw" id="8EeBQEuIf9" role="3cqZAk">
            <ref role="3cqZAo" node="8EeBQEuDNF" resolve="calories" />
          </node>
        </node>
      </node>
      <node concept="P$JXv" id="8EeBQEuLQy" role="lGtFl">
        <node concept="TZ5HA" id="8EeBQEuLQz" role="TZ5H$">
          <node concept="1dT_AC" id="8EeBQEuLQ$" role="1dT_Ay">
            <property role="1dT_AB" value="Returns the total calories of one cooked meal." />
          </node>
        </node>
        <node concept="x79VA" id="8EeBQEuLQ_" role="3nqlJM">
          <property role="x79VB" value="Calories in kJ" />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="8EeBQEta$P" role="13h7CW">
      <node concept="3clFbS" id="8EeBQEta$Q" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="2_H6PIKNGMi">
    <ref role="13h7C2" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
    <node concept="13i0hz" id="2_H6PIKNGOd" role="13h7CS">
      <property role="TrG5h" value="duplicate_meal_days" />
      <node concept="3Tm1VV" id="2_H6PIKNGOe" role="1B3o_S" />
      <node concept="3clFbS" id="2_H6PIKNGOg" role="3clF47">
        <node concept="3cpWs8" id="2_H6PIKNNYF" role="3cqZAp">
          <node concept="3cpWsn" id="2_H6PIKNNYG" role="3cpWs9">
            <property role="TrG5h" value="weekday_list" />
            <node concept="_YKpA" id="2_H6PIKNNNf" role="1tU5fm">
              <node concept="17QB3L" id="2_H6PIKNO4m" role="_ZDj9" />
            </node>
            <node concept="2ShNRf" id="2_H6PIKNNYH" role="33vP2m">
              <node concept="Tc6Ow" id="2_H6PIKNNYI" role="2ShVmc">
                <node concept="17QB3L" id="2_H6PIKNQD0" role="HW$YZ" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="3mpKCXQ02nF" role="3cqZAp" />
        <node concept="3clFbJ" id="2_H6PIKNZ4u" role="3cqZAp">
          <node concept="3clFbS" id="2_H6PIKNZ4w" role="3clFbx">
            <node concept="3cpWs6" id="2_H6PIKO126" role="3cqZAp">
              <node concept="37vLTw" id="2_H6PIKO1Pb" role="3cqZAk">
                <ref role="3cqZAo" node="2_H6PIKNNYG" resolve="weekday_list" />
              </node>
            </node>
          </node>
          <node concept="3fqX7Q" id="2_H6PIKO0Nn" role="3clFbw">
            <node concept="2OqwBi" id="2_H6PIKO0Np" role="3fr31v">
              <node concept="2OqwBi" id="2_H6PIKO0Nq" role="2Oq$k0">
                <node concept="13iPFW" id="2_H6PIKO0Nr" role="2Oq$k0" />
                <node concept="1mfA1w" id="2_H6PIKO0Ns" role="2OqNvi" />
              </node>
              <node concept="1mIQ4w" id="2_H6PIKO0Nt" role="2OqNvi">
                <node concept="chp4Y" id="2_H6PIKO0VA" role="cj9EA">
                  <ref role="cht4Q" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2_H6PIKNY$x" role="3cqZAp">
          <node concept="3cpWsn" id="2_H6PIKNY$$" role="3cpWs9">
            <property role="TrG5h" value="weekPlan" />
            <node concept="3Tqbb2" id="2_H6PIKNIfJ" role="1tU5fm">
              <ref role="ehGHo" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
            </node>
            <node concept="1PxgMI" id="2_H6PIKO3p$" role="33vP2m">
              <property role="1BlNFB" value="true" />
              <node concept="chp4Y" id="2_H6PIKO3xl" role="3oSUPX">
                <ref role="cht4Q" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
              </node>
              <node concept="2OqwBi" id="2_H6PIKO2pV" role="1m5AlR">
                <node concept="13iPFW" id="2_H6PIKO2aJ" role="2Oq$k0" />
                <node concept="1mfA1w" id="2_H6PIKO2G_" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2_H6PIKO22K" role="3cqZAp" />
        <node concept="3cpWs8" id="2_H6PIKOg8W" role="3cqZAp">
          <node concept="3cpWsn" id="2_H6PIKOg8Z" role="3cpWs9">
            <property role="TrG5h" value="todays_recipes" />
            <node concept="2hMVRd" id="2_H6PIKOg8S" role="1tU5fm">
              <node concept="3Tqbb2" id="2_H6PIKOvBM" role="2hN53Y">
                <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
              </node>
            </node>
            <node concept="2ShNRf" id="2_H6PIKOpJP" role="33vP2m">
              <node concept="2i4dXS" id="2_H6PIKOqfj" role="2ShVmc">
                <node concept="3Tqbb2" id="2_H6PIKOvKa" role="HW$YZ">
                  <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                </node>
                <node concept="2OqwBi" id="2_H6PIKOe0M" role="I$8f6">
                  <node concept="2OqwBi" id="2_H6PIKObIA" role="2Oq$k0">
                    <node concept="13iPFW" id="2_H6PIKObvx" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="2_H6PIKObZu" role="2OqNvi">
                      <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                    </node>
                  </node>
                  <node concept="13MTOL" id="2_H6PIKOfIC" role="2OqNvi">
                    <ref role="13MTZf" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2_H6PIKNWzs" role="3cqZAp">
          <node concept="3cpWsn" id="2_H6PIKNWzt" role="3cpWs9">
            <property role="TrG5h" value="dow_by_daily_menu" />
            <node concept="3rvAFt" id="2_H6PIKNSN2" role="1tU5fm">
              <node concept="3Tqbb2" id="2_H6PIKNYhE" role="3rvQeY">
                <ref role="ehGHo" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
              </node>
              <node concept="17QB3L" id="2_H6PIKNSN8" role="3rvSg0" />
            </node>
            <node concept="2ShNRf" id="2_H6PIKNWzu" role="33vP2m">
              <node concept="3rGOSV" id="2_H6PIKNWzv" role="2ShVmc">
                <node concept="3Tqbb2" id="2_H6PIKNY5d" role="3rHrn6">
                  <ref role="ehGHo" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
                </node>
                <node concept="17QB3L" id="2_H6PIKNWzx" role="3rHtpV" />
                <node concept="3Mi1_Z" id="2_H6PIKNWzy" role="3Mj9YC">
                  <node concept="3Milgn" id="2_H6PIKNWzz" role="3MiYds">
                    <node concept="2OqwBi" id="2_H6PIKNWz$" role="3MiK7k">
                      <node concept="37vLTw" id="2_H6PIKNWz_" role="2Oq$k0">
                        <ref role="3cqZAo" node="2_H6PIKNY$$" resolve="weekPlan" />
                      </node>
                      <node concept="3TrEf2" id="2_H6PIKNWzA" role="2OqNvi">
                        <ref role="3Tt5mk" to="u6v3:5mCwD4fOH8F" resolve="monday" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="2_H6PIKNWzB" role="3MiMdn">
                      <property role="Xl_RC" value="Monday" />
                    </node>
                  </node>
                  <node concept="3Milgn" id="2_H6PIKNWzC" role="3MiYds">
                    <node concept="2OqwBi" id="2_H6PIKNWzD" role="3MiK7k">
                      <node concept="37vLTw" id="2_H6PIKNWzE" role="2Oq$k0">
                        <ref role="3cqZAo" node="2_H6PIKNY$$" resolve="weekPlan" />
                      </node>
                      <node concept="3TrEf2" id="2_H6PIKNWzF" role="2OqNvi">
                        <ref role="3Tt5mk" to="u6v3:5mCwD4fOHfe" resolve="tuesday" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="2_H6PIKNWzG" role="3MiMdn">
                      <property role="Xl_RC" value="Tuesday" />
                    </node>
                  </node>
                  <node concept="3Milgn" id="2_H6PIKNWzH" role="3MiYds">
                    <node concept="2OqwBi" id="2_H6PIKNWzI" role="3MiK7k">
                      <node concept="37vLTw" id="2_H6PIKNWzJ" role="2Oq$k0">
                        <ref role="3cqZAo" node="2_H6PIKNY$$" resolve="weekPlan" />
                      </node>
                      <node concept="3TrEf2" id="2_H6PIKNWzK" role="2OqNvi">
                        <ref role="3Tt5mk" to="u6v3:5mCwD4fOHk$" resolve="wednesday" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="2_H6PIKNWzL" role="3MiMdn">
                      <property role="Xl_RC" value="Wednesday" />
                    </node>
                  </node>
                  <node concept="3Milgn" id="2_H6PIKNWzM" role="3MiYds">
                    <node concept="2OqwBi" id="2_H6PIKNWzN" role="3MiK7k">
                      <node concept="37vLTw" id="2_H6PIKNWzO" role="2Oq$k0">
                        <ref role="3cqZAo" node="2_H6PIKNY$$" resolve="weekPlan" />
                      </node>
                      <node concept="3TrEf2" id="2_H6PIKNWzP" role="2OqNvi">
                        <ref role="3Tt5mk" to="u6v3:5mCwD4fOHuQ" resolve="thursday" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="2_H6PIKNWzQ" role="3MiMdn">
                      <property role="Xl_RC" value="Thursday" />
                    </node>
                  </node>
                  <node concept="3Milgn" id="2_H6PIKNWzR" role="3MiYds">
                    <node concept="2OqwBi" id="2_H6PIKNWzS" role="3MiK7k">
                      <node concept="37vLTw" id="2_H6PIKNWzT" role="2Oq$k0">
                        <ref role="3cqZAo" node="2_H6PIKNY$$" resolve="weekPlan" />
                      </node>
                      <node concept="3TrEf2" id="2_H6PIKNWzU" role="2OqNvi">
                        <ref role="3Tt5mk" to="u6v3:5mCwD4fOHAf" resolve="friday" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="2_H6PIKNWzV" role="3MiMdn">
                      <property role="Xl_RC" value="Friday" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="3mpKCXPZX$V" role="3cqZAp">
          <node concept="1PaTwC" id="3mpKCXPZX$W" role="1aUNEU">
            <node concept="3oM_SD" id="3mpKCXPZXVd" role="1PaTwD">
              <property role="3oM_SC" value="Do" />
            </node>
            <node concept="3oM_SD" id="3mpKCXPZYYv" role="1PaTwD">
              <property role="3oM_SC" value="not" />
            </node>
            <node concept="3oM_SD" id="3mpKCXPZZ1o" role="1PaTwD">
              <property role="3oM_SC" value="include" />
            </node>
            <node concept="3oM_SD" id="3mpKCXPZZ78" role="1PaTwD">
              <property role="3oM_SC" value="our" />
            </node>
            <node concept="3oM_SD" id="3mpKCXPZZ81" role="1PaTwD">
              <property role="3oM_SC" value="own" />
            </node>
            <node concept="3oM_SD" id="3mpKCXQ008x" role="1PaTwD">
              <property role="3oM_SC" value="day" />
            </node>
            <node concept="3oM_SD" id="3mpKCXPZZ8V" role="1PaTwD">
              <property role="3oM_SC" value="of" />
            </node>
            <node concept="3oM_SD" id="3mpKCXPZZ9Q" role="1PaTwD">
              <property role="3oM_SC" value="week." />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2_H6PIKNRzJ" role="3cqZAp">
          <node concept="2OqwBi" id="2_H6PIKNXk7" role="3clFbG">
            <node concept="37vLTw" id="2_H6PIKNWzW" role="2Oq$k0">
              <ref role="3cqZAo" node="2_H6PIKNWzt" resolve="dow_by_daily_menu" />
            </node>
            <node concept="kI3uX" id="2_H6PIKNXMs" role="2OqNvi">
              <node concept="13iPFW" id="2_H6PIKNXRO" role="kIiFs" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2_H6PIKO7yT" role="3cqZAp" />
        <node concept="2Gpval" id="2_H6PIKO7KG" role="3cqZAp">
          <node concept="2GrKxI" id="2_H6PIKO7KI" role="2Gsz3X">
            <property role="TrG5h" value="dow_menu" />
          </node>
          <node concept="37vLTw" id="2_H6PIKO7ZX" role="2GsD0m">
            <ref role="3cqZAo" node="2_H6PIKNWzt" resolve="dow_by_daily_menu" />
          </node>
          <node concept="3clFbS" id="2_H6PIKO7KM" role="2LFqv$">
            <node concept="3cpWs8" id="2_H6PIKOwg8" role="3cqZAp">
              <node concept="3cpWsn" id="2_H6PIKOwgb" role="3cpWs9">
                <property role="TrG5h" value="other_days_recipes" />
                <node concept="2hMVRd" id="2_H6PIKOwg6" role="1tU5fm">
                  <node concept="3Tqbb2" id="2_H6PIKOwrT" role="2hN53Y">
                    <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                  </node>
                </node>
                <node concept="2ShNRf" id="2_H6PIKOxFC" role="33vP2m">
                  <node concept="2i4dXS" id="2_H6PIKOxFD" role="2ShVmc">
                    <node concept="3Tqbb2" id="2_H6PIKOxFE" role="HW$YZ">
                      <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                    </node>
                    <node concept="2OqwBi" id="2_H6PIKOAyq" role="I$8f6">
                      <node concept="2OqwBi" id="2_H6PIKO$uo" role="2Oq$k0">
                        <node concept="2OqwBi" id="2_H6PIKOzE$" role="2Oq$k0">
                          <node concept="2GrUjf" id="2_H6PIKOzgf" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="2_H6PIKO7KI" resolve="dow_menu" />
                          </node>
                          <node concept="3AY5_j" id="2_H6PIKO$5H" role="2OqNvi" />
                        </node>
                        <node concept="3Tsc0h" id="2_H6PIKO$Vb" role="2OqNvi">
                          <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        </node>
                      </node>
                      <node concept="13MTOL" id="2_H6PIKOD$H" role="2OqNvi">
                        <ref role="13MTZf" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="2_H6PIKODY0" role="3cqZAp">
              <node concept="3clFbS" id="2_H6PIKODY2" role="3clFbx">
                <node concept="3clFbF" id="2_H6PIKOIcr" role="3cqZAp">
                  <node concept="2OqwBi" id="2_H6PIKOJ7L" role="3clFbG">
                    <node concept="37vLTw" id="2_H6PIKOIcp" role="2Oq$k0">
                      <ref role="3cqZAo" node="2_H6PIKNNYG" resolve="weekday_list" />
                    </node>
                    <node concept="TSZUe" id="2_H6PIKOK6F" role="2OqNvi">
                      <node concept="2OqwBi" id="2_H6PIKOKOC" role="25WWJ7">
                        <node concept="2GrUjf" id="2_H6PIKOKkb" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="2_H6PIKO7KI" resolve="dow_menu" />
                        </node>
                        <node concept="3AV6Ez" id="2_H6PIKOLlE" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="2_H6PIKOGMk" role="3clFbw">
                <node concept="2OqwBi" id="2_H6PIKOFkw" role="2Oq$k0">
                  <node concept="37vLTw" id="2_H6PIKOEbH" role="2Oq$k0">
                    <ref role="3cqZAo" node="2_H6PIKOg8Z" resolve="todays_recipes" />
                  </node>
                  <node concept="60FfQ" id="2_H6PIKQiOT" role="2OqNvi">
                    <node concept="37vLTw" id="2_H6PIKQje8" role="576Qk">
                      <ref role="3cqZAo" node="2_H6PIKOwgb" resolve="other_days_recipes" />
                    </node>
                  </node>
                </node>
                <node concept="3GX2aA" id="2_H6PIKOHO4" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2_H6PIKO761" role="3cqZAp" />
        <node concept="3clFbF" id="2_H6PIKNNF9" role="3cqZAp">
          <node concept="37vLTw" id="2_H6PIKNNYJ" role="3clFbG">
            <ref role="3cqZAo" node="2_H6PIKNNYG" resolve="weekday_list" />
          </node>
        </node>
      </node>
      <node concept="_YKpA" id="2_H6PIKNOsr" role="3clF45">
        <node concept="17QB3L" id="2_H6PIKNOBX" role="_ZDj9" />
      </node>
      <node concept="P$JXv" id="3mpKCXPZy9T" role="lGtFl">
        <node concept="TZ5HA" id="3mpKCXPZy9U" role="TZ5H$">
          <node concept="1dT_AC" id="3mpKCXPZy9V" role="1dT_Ay">
            <property role="1dT_AB" value="Returns the other week days with one or more meals in common with this menu." />
          </node>
        </node>
        <node concept="TZ5HA" id="3mpKCXPZMvt" role="TZ5H$">
          <node concept="1dT_AC" id="3mpKCXPZMvu" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="TZ5HA" id="3mpKCXPZMRr" role="TZ5H$">
          <node concept="1dT_AC" id="3mpKCXPZMRs" role="1dT_Ay">
            <property role="1dT_AB" value="If the parent node’s type is not WeekMealPlan, the empty list is returned." />
          </node>
        </node>
        <node concept="x79VA" id="3mpKCXPZy9W" role="3nqlJM">
          <property role="x79VB" value="List of weekday strings." />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="2_H6PIKNGMj" role="13h7CW">
      <node concept="3clFbS" id="2_H6PIKNGMk" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="3mpKCXP7K3Y">
    <ref role="13h7C2" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
    <node concept="13i0hz" id="3mpKCXP7K5v" role="13h7CS">
      <property role="TrG5h" value="all_meals" />
      <node concept="3Tm1VV" id="3mpKCXP7K5w" role="1B3o_S" />
      <node concept="2I9FWS" id="3mpKCXP7KcL" role="3clF45">
        <ref role="2I9WkF" to="u6v3:5mCwD4fO2$5" resolve="Meal" />
      </node>
      <node concept="3clFbS" id="3mpKCXP7K5y" role="3clF47">
        <node concept="3cpWs8" id="3mpKCXP7U9K" role="3cqZAp">
          <node concept="3cpWsn" id="3mpKCXP7U9N" role="3cpWs9">
            <property role="TrG5h" value="result" />
            <node concept="2I9FWS" id="3mpKCXP7U9I" role="1tU5fm">
              <ref role="2I9WkF" to="u6v3:5mCwD4fO2$5" resolve="Meal" />
            </node>
            <node concept="2ShNRf" id="3mpKCXP8foW" role="33vP2m">
              <node concept="Tc6Ow" id="3mpKCXP8hzR" role="2ShVmc" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3mpKCXP7Ush" role="3cqZAp">
          <node concept="2OqwBi" id="3mpKCXP7WDc" role="3clFbG">
            <node concept="37vLTw" id="3mpKCXP7Usf" role="2Oq$k0">
              <ref role="3cqZAo" node="3mpKCXP7U9N" resolve="result" />
            </node>
            <node concept="X8dFx" id="3mpKCXP80RK" role="2OqNvi">
              <node concept="2OqwBi" id="3mpKCXP88FX" role="25WWJ7">
                <node concept="2OqwBi" id="3mpKCXP83bA" role="2Oq$k0">
                  <node concept="13iPFW" id="3mpKCXP82hX" role="2Oq$k0" />
                  <node concept="3TrEf2" id="3mpKCXP85Im" role="2OqNvi">
                    <ref role="3Tt5mk" to="u6v3:5mCwD4fOH8F" resolve="monday" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="3mpKCXP8aH2" role="2OqNvi">
                  <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3mpKCXP8joU" role="3cqZAp">
          <node concept="2OqwBi" id="3mpKCXP8joV" role="3clFbG">
            <node concept="37vLTw" id="3mpKCXP8joW" role="2Oq$k0">
              <ref role="3cqZAo" node="3mpKCXP7U9N" resolve="result" />
            </node>
            <node concept="X8dFx" id="3mpKCXP8joX" role="2OqNvi">
              <node concept="2OqwBi" id="3mpKCXP8joY" role="25WWJ7">
                <node concept="2OqwBi" id="3mpKCXP8joZ" role="2Oq$k0">
                  <node concept="13iPFW" id="3mpKCXP8jp0" role="2Oq$k0" />
                  <node concept="3TrEf2" id="3mpKCXP8jp1" role="2OqNvi">
                    <ref role="3Tt5mk" to="u6v3:5mCwD4fOHfe" resolve="tuesday" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="3mpKCXP8jp2" role="2OqNvi">
                  <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3mpKCXP8npo" role="3cqZAp">
          <node concept="2OqwBi" id="3mpKCXP8npp" role="3clFbG">
            <node concept="37vLTw" id="3mpKCXP8npq" role="2Oq$k0">
              <ref role="3cqZAo" node="3mpKCXP7U9N" resolve="result" />
            </node>
            <node concept="X8dFx" id="3mpKCXP8npr" role="2OqNvi">
              <node concept="2OqwBi" id="3mpKCXP8nps" role="25WWJ7">
                <node concept="2OqwBi" id="3mpKCXP8npt" role="2Oq$k0">
                  <node concept="13iPFW" id="3mpKCXP8npu" role="2Oq$k0" />
                  <node concept="3TrEf2" id="3mpKCXP8npv" role="2OqNvi">
                    <ref role="3Tt5mk" to="u6v3:5mCwD4fOHk$" resolve="wednesday" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="3mpKCXP8npw" role="2OqNvi">
                  <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3mpKCXP8qza" role="3cqZAp">
          <node concept="2OqwBi" id="3mpKCXP8qzb" role="3clFbG">
            <node concept="37vLTw" id="3mpKCXP8qzc" role="2Oq$k0">
              <ref role="3cqZAo" node="3mpKCXP7U9N" resolve="result" />
            </node>
            <node concept="X8dFx" id="3mpKCXP8qzd" role="2OqNvi">
              <node concept="2OqwBi" id="3mpKCXP8qze" role="25WWJ7">
                <node concept="2OqwBi" id="3mpKCXP8qzf" role="2Oq$k0">
                  <node concept="13iPFW" id="3mpKCXP8qzg" role="2Oq$k0" />
                  <node concept="3TrEf2" id="3mpKCXP8qzh" role="2OqNvi">
                    <ref role="3Tt5mk" to="u6v3:5mCwD4fOHuQ" resolve="thursday" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="3mpKCXP8qzi" role="2OqNvi">
                  <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3mpKCXP8vYU" role="3cqZAp">
          <node concept="2OqwBi" id="3mpKCXP8vYV" role="3clFbG">
            <node concept="37vLTw" id="3mpKCXP8vYW" role="2Oq$k0">
              <ref role="3cqZAo" node="3mpKCXP7U9N" resolve="result" />
            </node>
            <node concept="X8dFx" id="3mpKCXP8vYX" role="2OqNvi">
              <node concept="2OqwBi" id="3mpKCXP8vYY" role="25WWJ7">
                <node concept="2OqwBi" id="3mpKCXP8vYZ" role="2Oq$k0">
                  <node concept="13iPFW" id="3mpKCXP8vZ0" role="2Oq$k0" />
                  <node concept="3TrEf2" id="3mpKCXP8vZ1" role="2OqNvi">
                    <ref role="3Tt5mk" to="u6v3:5mCwD4fOHAf" resolve="friday" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="3mpKCXP8vZ2" role="2OqNvi">
                  <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="3mpKCXP8JLd" role="3cqZAp">
          <node concept="37vLTw" id="3mpKCXP8OwF" role="3cqZAk">
            <ref role="3cqZAo" node="3mpKCXP7U9N" resolve="result" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="3mpKCXP7K3Z" role="13h7CW">
      <node concept="3clFbS" id="3mpKCXP7K40" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="1jE3pLu_6sr" role="13h7CS">
      <property role="TrG5h" value="suggest_week_meal_plan" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="1jE3pLu_6ss" role="1B3o_S" />
      <node concept="3Tqbb2" id="1jE3pLuAELO" role="3clF45">
        <ref role="ehGHo" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
      </node>
      <node concept="3clFbS" id="1jE3pLu_6su" role="3clF47">
        <node concept="3cpWs8" id="1jE3pLuAl2h" role="3cqZAp">
          <node concept="3cpWsn" id="1jE3pLuAl2i" role="3cpWs9">
            <property role="TrG5h" value="veg_recipes" />
            <node concept="A3Dl8" id="1jE3pLuBY9r" role="1tU5fm">
              <node concept="3Tqbb2" id="1jE3pLuBY9t" role="A3Ik2">
                <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
              </node>
            </node>
            <node concept="2OqwBi" id="1jE3pLuAoJ8" role="33vP2m">
              <node concept="37vLTw" id="1jE3pLuG2Rj" role="2Oq$k0">
                <ref role="3cqZAo" node="1jE3pLuFWYI" resolve="available_recipes" />
              </node>
              <node concept="3zZkjj" id="1jE3pLuAqXI" role="2OqNvi">
                <node concept="1bVj0M" id="1jE3pLuAqXK" role="23t8la">
                  <node concept="3clFbS" id="1jE3pLuAqXL" role="1bW5cS">
                    <node concept="3clFbF" id="1jE3pLuArcP" role="3cqZAp">
                      <node concept="3y3z36" id="1jE3pLuAsGY" role="3clFbG">
                        <node concept="2OqwBi" id="1jE3pLuAuaO" role="3uHU7w">
                          <node concept="1XH99k" id="1jE3pLuAtcK" role="2Oq$k0">
                            <ref role="1XH99l" to="u6v3:6dt6wfGrvhS" resolve="Diet" />
                          </node>
                          <node concept="2ViDtV" id="1jE3pLuAuym" role="2OqNvi">
                            <ref role="2ViDtZ" to="u6v3:6dt6wfGrvhY" resolve="Omnivore" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="1jE3pLuAryw" role="3uHU7B">
                          <node concept="37vLTw" id="1jE3pLuArcO" role="2Oq$k0">
                            <ref role="3cqZAo" node="1jE3pLuAqXM" resolve="it" />
                          </node>
                          <node concept="2qgKlT" id="1jE3pLuArXp" role="2OqNvi">
                            <ref role="37wK5l" node="8EeBQEta_7" resolve="diet" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="1jE3pLuAqXM" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="1jE3pLuAqXN" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1jE3pLuFA$S" role="3cqZAp" />
        <node concept="3cpWs8" id="1jE3pLvyeNa" role="3cqZAp">
          <node concept="3cpWsn" id="1jE3pLvyeNb" role="3cpWs9">
            <property role="TrG5h" value="combinations" />
            <node concept="A3Dl8" id="1jE3pLvycqt" role="1tU5fm">
              <node concept="A3Dl8" id="1jE3pLvycq$" role="A3Ik2">
                <node concept="3Tqbb2" id="1jE3pLvycq_" role="A3Ik2">
                  <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="1jE3pLvyeNc" role="33vP2m">
              <node concept="1fM9EW" id="1jE3pLvyeNd" role="2Oq$k0" />
              <node concept="2qgKlT" id="1jE3pLvyeNe" role="2OqNvi">
                <ref role="37wK5l" node="1jE3pLu_zgp" resolve="combinations" />
                <node concept="3cmrfG" id="1jE3pLvyeNf" role="37wK5m">
                  <property role="3cmrfH" value="5" />
                </node>
                <node concept="37vLTw" id="1jE3pLvyeNg" role="37wK5m">
                  <ref role="3cqZAo" node="1jE3pLuAl2i" resolve="veg_recipes" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2xdQw9" id="1jE3pLvyi2_" role="3cqZAp">
          <node concept="3cpWs3" id="1jE3pLvyj8q" role="9lYJi">
            <node concept="2OqwBi" id="1jE3pLvyjW5" role="3uHU7w">
              <node concept="37vLTw" id="1jE3pLvyjum" role="2Oq$k0">
                <ref role="3cqZAo" node="1jE3pLvyeNb" resolve="combinations" />
              </node>
              <node concept="34oBXx" id="1jE3pLvyk$R" role="2OqNvi" />
            </node>
            <node concept="Xl_RD" id="1jE3pLvyi2B" role="3uHU7B">
              <property role="Xl_RC" value="#combinations: " />
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="1jE3pLuAKL6" role="3cqZAp">
          <node concept="2GrKxI" id="1jE3pLuAKL8" role="2Gsz3X">
            <property role="TrG5h" value="recipes" />
          </node>
          <node concept="3clFbS" id="1jE3pLuAKLc" role="2LFqv$">
            <node concept="3clFbJ" id="1jE3pLuEGue" role="3cqZAp">
              <node concept="3clFbS" id="1jE3pLuEGug" role="3clFbx">
                <node concept="3N13vt" id="1jE3pLuFzPo" role="3cqZAp" />
              </node>
              <node concept="3fqX7Q" id="1jE3pLuFzv1" role="3clFbw">
                <node concept="2OqwBi" id="1jE3pLuFzv3" role="3fr31v">
                  <node concept="37vLTw" id="1jE3pLuFzv4" role="2Oq$k0">
                    <ref role="3cqZAo" node="1jE3pLuDl_S" resolve="pantry" />
                  </node>
                  <node concept="2qgKlT" id="1jE3pLuFzv5" role="2OqNvi">
                    <ref role="37wK5l" node="1jE3pLuDzFw" resolve="can_provide" />
                    <node concept="37vLTw" id="1jE3pLuFzv6" role="37wK5m">
                      <ref role="3cqZAo" node="1jE3pLuDkA7" resolve="daily_visitors" />
                    </node>
                    <node concept="2GrUjf" id="1jE3pLuFzv7" role="37wK5m">
                      <ref role="2Gs0qQ" node="1jE3pLuAKL8" resolve="recipes" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="1jE3pLuF_Uy" role="3cqZAp" />
            <node concept="3cpWs8" id="1jE3pLuBvvs" role="3cqZAp">
              <node concept="3cpWsn" id="1jE3pLuBvvt" role="3cpWs9">
                <property role="TrG5h" value="meals" />
                <node concept="A3Dl8" id="1jE3pLuBvps" role="1tU5fm">
                  <node concept="3Tqbb2" id="1jE3pLuBvpv" role="A3Ik2">
                    <ref role="ehGHo" to="u6v3:5mCwD4fO2$5" resolve="Meal" />
                  </node>
                </node>
                <node concept="2OqwBi" id="1jE3pLuBvvu" role="33vP2m">
                  <node concept="2GrUjf" id="1jE3pLuBvvv" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="1jE3pLuAKL8" resolve="recipes" />
                  </node>
                  <node concept="3$u5V9" id="1jE3pLuBvvw" role="2OqNvi">
                    <node concept="1bVj0M" id="1jE3pLuBvvx" role="23t8la">
                      <node concept="3clFbS" id="1jE3pLuBvvy" role="1bW5cS">
                        <node concept="3clFbF" id="1jE3pLuBvvz" role="3cqZAp">
                          <node concept="2pJPEk" id="1jE3pLuBvv$" role="3clFbG">
                            <node concept="2pJPED" id="1jE3pLuBvv_" role="2pJPEn">
                              <ref role="2pJxaS" to="u6v3:5mCwD4fO2$5" resolve="Meal" />
                              <node concept="2pIpSj" id="1jE3pLuBvvA" role="2pJxcM">
                                <ref role="2pIpSl" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                                <node concept="36biLy" id="1jE3pLuBvvB" role="28nt2d">
                                  <node concept="37vLTw" id="1jE3pLuBvvC" role="36biLW">
                                    <ref role="3cqZAo" node="1jE3pLuBvvD" resolve="it" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Rh6nW" id="1jE3pLuBvvD" role="1bW2Oz">
                        <property role="TrG5h" value="it" />
                        <node concept="2jxLKc" id="1jE3pLuBvvE" role="1tU5fm" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs6" id="1jE3pLuF_6V" role="3cqZAp">
              <node concept="2pJPEk" id="1jE3pLuBY_N" role="3cqZAk">
                <node concept="2pJPED" id="1jE3pLuBY_O" role="2pJPEn">
                  <ref role="2pJxaS" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
                  <node concept="2pIpSj" id="1jE3pLuBY_P" role="2pJxcM">
                    <ref role="2pIpSl" to="u6v3:5mCwD4fOH8F" resolve="monday" />
                    <node concept="2pJPED" id="1jE3pLuBY_Q" role="28nt2d">
                      <ref role="2pJxaS" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
                      <node concept="2pIpSj" id="1jE3pLuBY_R" role="2pJxcM">
                        <ref role="2pIpSl" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        <node concept="36biLy" id="1jE3pLuBY_S" role="28nt2d">
                          <node concept="2OqwBi" id="1jE3pLuBY_T" role="36biLW">
                            <node concept="37vLTw" id="1jE3pLuBY_U" role="2Oq$k0">
                              <ref role="3cqZAo" node="1jE3pLuBvvt" resolve="meals" />
                            </node>
                            <node concept="8snch" id="1jE3pLuBY_V" role="2OqNvi">
                              <node concept="3cmrfG" id="1jE3pLuBY_W" role="8sqot">
                                <property role="3cmrfH" value="0" />
                              </node>
                              <node concept="3cmrfG" id="1jE3pLuBY_X" role="8st4g">
                                <property role="3cmrfH" value="1" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2pIpSj" id="1jE3pLuBY_Y" role="2pJxcM">
                    <ref role="2pIpSl" to="u6v3:5mCwD4fOHfe" resolve="tuesday" />
                    <node concept="2pJPED" id="1jE3pLuBY_Z" role="28nt2d">
                      <ref role="2pJxaS" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
                      <node concept="2pIpSj" id="1jE3pLuBYA0" role="2pJxcM">
                        <ref role="2pIpSl" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        <node concept="36biLy" id="1jE3pLuBYA1" role="28nt2d">
                          <node concept="2OqwBi" id="1jE3pLuBYA2" role="36biLW">
                            <node concept="37vLTw" id="1jE3pLuBYA3" role="2Oq$k0">
                              <ref role="3cqZAo" node="1jE3pLuBvvt" resolve="meals" />
                            </node>
                            <node concept="8snch" id="1jE3pLuBYA4" role="2OqNvi">
                              <node concept="3cmrfG" id="1jE3pLuBYA5" role="8sqot">
                                <property role="3cmrfH" value="1" />
                              </node>
                              <node concept="3cmrfG" id="1jE3pLuBYA6" role="8st4g">
                                <property role="3cmrfH" value="2" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2pIpSj" id="1jE3pLuBYA7" role="2pJxcM">
                    <ref role="2pIpSl" to="u6v3:5mCwD4fOHk$" resolve="wednesday" />
                    <node concept="2pJPED" id="1jE3pLuBYA8" role="28nt2d">
                      <ref role="2pJxaS" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
                      <node concept="2pIpSj" id="1jE3pLuBYA9" role="2pJxcM">
                        <ref role="2pIpSl" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        <node concept="36biLy" id="1jE3pLuBYAa" role="28nt2d">
                          <node concept="2OqwBi" id="1jE3pLuBYAb" role="36biLW">
                            <node concept="37vLTw" id="1jE3pLuBYAc" role="2Oq$k0">
                              <ref role="3cqZAo" node="1jE3pLuBvvt" resolve="meals" />
                            </node>
                            <node concept="8snch" id="1jE3pLuBYAd" role="2OqNvi">
                              <node concept="3cmrfG" id="1jE3pLuBYAe" role="8sqot">
                                <property role="3cmrfH" value="2" />
                              </node>
                              <node concept="3cmrfG" id="1jE3pLuBYAf" role="8st4g">
                                <property role="3cmrfH" value="3" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2pIpSj" id="1jE3pLuBYAg" role="2pJxcM">
                    <ref role="2pIpSl" to="u6v3:5mCwD4fOHuQ" resolve="thursday" />
                    <node concept="2pJPED" id="1jE3pLuBYAh" role="28nt2d">
                      <ref role="2pJxaS" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
                      <node concept="2pIpSj" id="1jE3pLuBYAi" role="2pJxcM">
                        <ref role="2pIpSl" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        <node concept="36biLy" id="1jE3pLuBYAj" role="28nt2d">
                          <node concept="2OqwBi" id="1jE3pLuBYAk" role="36biLW">
                            <node concept="37vLTw" id="1jE3pLuBYAl" role="2Oq$k0">
                              <ref role="3cqZAo" node="1jE3pLuBvvt" resolve="meals" />
                            </node>
                            <node concept="8snch" id="1jE3pLuBYAm" role="2OqNvi">
                              <node concept="3cmrfG" id="1jE3pLuBYAn" role="8sqot">
                                <property role="3cmrfH" value="3" />
                              </node>
                              <node concept="3cmrfG" id="1jE3pLuBYAo" role="8st4g">
                                <property role="3cmrfH" value="4" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2pIpSj" id="1jE3pLuBYAp" role="2pJxcM">
                    <ref role="2pIpSl" to="u6v3:5mCwD4fOHAf" resolve="friday" />
                    <node concept="2pJPED" id="1jE3pLuBYAq" role="28nt2d">
                      <ref role="2pJxaS" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
                      <node concept="2pIpSj" id="1jE3pLuBYAr" role="2pJxcM">
                        <ref role="2pIpSl" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        <node concept="36biLy" id="1jE3pLuBYAs" role="28nt2d">
                          <node concept="2OqwBi" id="1jE3pLuBYAt" role="36biLW">
                            <node concept="37vLTw" id="1jE3pLuBYAu" role="2Oq$k0">
                              <ref role="3cqZAo" node="1jE3pLuBvvt" resolve="meals" />
                            </node>
                            <node concept="8snch" id="1jE3pLuBYAv" role="2OqNvi">
                              <node concept="3cmrfG" id="1jE3pLuBYAw" role="8sqot">
                                <property role="3cmrfH" value="4" />
                              </node>
                              <node concept="3cmrfG" id="1jE3pLuBYAx" role="8st4g">
                                <property role="3cmrfH" value="5" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="1jE3pLvyeNh" role="2GsD0m">
            <ref role="3cqZAo" node="1jE3pLvyeNb" resolve="combinations" />
          </node>
        </node>
        <node concept="3cpWs6" id="1jE3pLuAMGS" role="3cqZAp">
          <node concept="10Nm6u" id="1jE3pLuAMYZ" role="3cqZAk" />
        </node>
      </node>
      <node concept="2AHcQZ" id="1jE3pLuCaVF" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
      </node>
      <node concept="P$JXv" id="1jE3pLuCZUB" role="lGtFl">
        <node concept="TZ5HA" id="1jE3pLuCZUC" role="TZ5H$">
          <node concept="1dT_AC" id="1jE3pLuCZUD" role="1dT_Ay">
            <property role="1dT_AB" value="Suggests a simple week meal plan for the current stock." />
          </node>
        </node>
        <node concept="TZ5HA" id="1jE3pLuDafF" role="TZ5H$">
          <node concept="1dT_AC" id="1jE3pLuDafG" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="TZ5HA" id="1jE3pLuDagb" role="TZ5H$">
          <node concept="1dT_AC" id="1jE3pLuDagc" role="1dT_Ay">
            <property role="1dT_AB" value="This method only suggests vegetarian meal plans with exactly one, distinct meal per day." />
          </node>
        </node>
        <node concept="TZ5HA" id="1jE3pLuDgdv" role="TZ5H$">
          <node concept="1dT_AC" id="1jE3pLuDgdw" role="1dT_Ay">
            <property role="1dT_AB" value="It guarantees to find such a meal plan if it exists under the given constraints." />
          </node>
        </node>
        <node concept="TUZQ0" id="1jE3pLuDoaP" role="3nqlJM">
          <property role="TUZQ4" value="All available recipes." />
          <node concept="zr_55" id="1jE3pLuFXDd" role="zr_5Q">
            <ref role="zr_51" node="1jE3pLuFWYI" resolve="available_recipes" />
          </node>
        </node>
        <node concept="TUZQ0" id="1jE3pLuDp6u" role="3nqlJM">
          <property role="TUZQ4" value="Number of persons to be served each day of the week." />
          <node concept="zr_55" id="1jE3pLuFY0W" role="zr_5Q">
            <ref role="zr_51" node="1jE3pLuDkA7" resolve="daily_visitors" />
          </node>
        </node>
        <node concept="TUZQ0" id="1jE3pLuG0bd" role="3nqlJM">
          <property role="TUZQ4" value="Contains the maximum available amount of ingredients." />
          <node concept="zr_55" id="1jE3pLuG0wy" role="zr_5Q">
            <ref role="zr_51" node="1jE3pLuDl_S" resolve="pantry" />
          </node>
        </node>
        <node concept="x79VA" id="1jE3pLuCZUE" role="3nqlJM">
          <property role="x79VB" value="The suggested meal plan, or null if no simple plan exists." />
        </node>
      </node>
      <node concept="37vLTG" id="1jE3pLuFWYI" role="3clF46">
        <property role="TrG5h" value="available_recipes" />
        <node concept="A3Dl8" id="1jE3pLuFXlY" role="1tU5fm">
          <node concept="3Tqbb2" id="1jE3pLuFXq2" role="A3Ik2">
            <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1jE3pLuDkA7" role="3clF46">
        <property role="TrG5h" value="daily_visitors" />
        <node concept="10Oyi0" id="1jE3pLuDlhE" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="1jE3pLuDl_S" role="3clF46">
        <property role="TrG5h" value="pantry" />
        <node concept="3Tqbb2" id="1jE3pLuDlXL" role="1tU5fm">
          <ref role="ehGHo" to="u6v3:7moC6rm27Me" resolve="Pantry" />
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="1jE3pLu_zgp" role="13h7CS">
      <property role="TrG5h" value="combinations" />
      <property role="2Ki8OM" value="true" />
      <node concept="3Tm1VV" id="1jE3pLu_zgq" role="1B3o_S" />
      <node concept="A3Dl8" id="1jE3pLu_FYT" role="3clF45">
        <node concept="A3Dl8" id="1jE3pLu_G24" role="A3Ik2">
          <node concept="3Tqbb2" id="1jE3pLu_G9j" role="A3Ik2">
            <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="1jE3pLu_zgs" role="3clF47">
        <node concept="3clFbJ" id="1jE3pLu_GiN" role="3cqZAp">
          <node concept="2dkUwp" id="1jE3pLu_Hkr" role="3clFbw">
            <node concept="37vLTw" id="1jE3pLu_Gk3" role="3uHU7B">
              <ref role="3cqZAo" node="1jE3pLu_Edp" resolve="k" />
            </node>
            <node concept="3cmrfG" id="1jE3pLu_HoA" role="3uHU7w">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3clFbS" id="1jE3pLu_GiP" role="3clFbx">
            <node concept="3cpWs6" id="1jE3pLu_HzM" role="3cqZAp">
              <node concept="2ShNRf" id="1jE3pLvzIdU" role="3cqZAk">
                <node concept="kMnCb" id="1jE3pLvzJ1V" role="2ShVmc">
                  <node concept="A3Dl8" id="1jE3pLvzJqX" role="kMuH3">
                    <node concept="3Tqbb2" id="1jE3pLvzJOS" role="A3Ik2">
                      <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                    </node>
                  </node>
                  <node concept="1bVj0M" id="1jE3pLvABLX" role="kMx8a">
                    <node concept="3clFbS" id="1jE3pLvABLY" role="1bW5cS">
                      <node concept="2n63Yl" id="1jE3pLvACip" role="3cqZAp">
                        <node concept="2ShNRf" id="1jE3pLvAD61" role="2n6tg2">
                          <node concept="kMnCb" id="1jE3pLvAE3L" role="2ShVmc">
                            <node concept="3Tqbb2" id="1jE3pLvAE$s" role="kMuH3">
                              <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1jE3pLvOWYx" role="3cqZAp" />
        <node concept="3cpWs8" id="1jE3pLvOHlg" role="3cqZAp">
          <node concept="3cpWsn" id="1jE3pLvOHlj" role="3cpWs9">
            <property role="TrG5h" value="tails" />
            <node concept="A3Dl8" id="1jE3pLvOHld" role="1tU5fm">
              <node concept="A3Dl8" id="1jE3pLvOHHQ" role="A3Ik2">
                <node concept="3Tqbb2" id="1jE3pLvOI4m" role="A3Ik2">
                  <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="1jE3pLvOTqY" role="33vP2m">
              <node concept="1bVj0M" id="1jE3pLvOJs1" role="2Oq$k0">
                <node concept="3clFbS" id="1jE3pLvOJs3" role="1bW5cS">
                  <node concept="3cpWs8" id="1jE3pLvOKEF" role="3cqZAp">
                    <node concept="3cpWsn" id="1jE3pLvOKEI" role="3cpWs9">
                      <property role="TrG5h" value="tail" />
                      <node concept="A3Dl8" id="1jE3pLvOKEK" role="1tU5fm">
                        <node concept="3Tqbb2" id="1jE3pLvOKEL" role="A3Ik2">
                          <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="1jE3pLvP3Ax" role="33vP2m">
                        <ref role="3cqZAo" node="1jE3pLu_Eez" resolve="list" />
                      </node>
                    </node>
                  </node>
                  <node concept="2$JKZl" id="1jE3pLvOLJC" role="3cqZAp">
                    <node concept="3clFbS" id="1jE3pLvOLJE" role="2LFqv$">
                      <node concept="2n63Yl" id="1jE3pLvOR7r" role="3cqZAp">
                        <node concept="37vLTw" id="1jE3pLvORvf" role="2n6tg2">
                          <ref role="3cqZAo" node="1jE3pLvOKEI" resolve="tail" />
                        </node>
                      </node>
                      <node concept="3clFbF" id="1jE3pLvONCX" role="3cqZAp">
                        <node concept="37vLTI" id="1jE3pLvONZ1" role="3clFbG">
                          <node concept="2OqwBi" id="1jE3pLvOOSx" role="37vLTx">
                            <node concept="37vLTw" id="1jE3pLvOOtN" role="2Oq$k0">
                              <ref role="3cqZAo" node="1jE3pLvOKEI" resolve="tail" />
                            </node>
                            <node concept="7r0gD" id="1jE3pLvOPMw" role="2OqNvi">
                              <node concept="3cmrfG" id="1jE3pLvOQ8x" role="7T0AP">
                                <property role="3cmrfH" value="1" />
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTw" id="1jE3pLvONCW" role="37vLTJ">
                            <ref role="3cqZAo" node="1jE3pLvOKEI" resolve="tail" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="1jE3pLvOMBs" role="2$JKZa">
                      <node concept="37vLTw" id="1jE3pLvOM4H" role="2Oq$k0">
                        <ref role="3cqZAo" node="1jE3pLvOKEI" resolve="tail" />
                      </node>
                      <node concept="3GX2aA" id="1jE3pLvONjq" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1Bd96e" id="1jE3pLvOUe8" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1jE3pLvEd4O" role="3cqZAp" />
        <node concept="3clFbF" id="1jE3pLw3hjU" role="3cqZAp">
          <node concept="2OqwBi" id="1jE3pLw3hQO" role="3clFbG">
            <node concept="37vLTw" id="1jE3pLw3hjS" role="2Oq$k0">
              <ref role="3cqZAo" node="1jE3pLvOHlj" resolve="tails" />
            </node>
            <node concept="3goQfb" id="1jE3pLw3iyt" role="2OqNvi">
              <node concept="1bVj0M" id="1jE3pLw3iyv" role="23t8la">
                <node concept="3clFbS" id="1jE3pLw3iyw" role="1bW5cS">
                  <node concept="3cpWs8" id="1jE3pLvF7Rm" role="3cqZAp">
                    <node concept="3cpWsn" id="1jE3pLvF7Rn" role="3cpWs9">
                      <property role="TrG5h" value="x" />
                      <node concept="3Tqbb2" id="1jE3pLvF7Ro" role="1tU5fm">
                        <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                      </node>
                      <node concept="2OqwBi" id="1jE3pLvF7Rp" role="33vP2m">
                        <node concept="1uHKPH" id="1jE3pLvF7Rr" role="2OqNvi" />
                        <node concept="37vLTw" id="1jE3pLvHLnA" role="2Oq$k0">
                          <ref role="3cqZAo" node="1jE3pLw3iyx" resolve="tail" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3cpWs8" id="1jE3pLvF7Rs" role="3cqZAp">
                    <node concept="3cpWsn" id="1jE3pLvF7Rt" role="3cpWs9">
                      <property role="TrG5h" value="xs" />
                      <node concept="A3Dl8" id="1jE3pLvF7Ru" role="1tU5fm">
                        <node concept="3Tqbb2" id="1jE3pLvF7Rv" role="A3Ik2">
                          <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                        </node>
                      </node>
                      <node concept="2OqwBi" id="1jE3pLvF7Rw" role="33vP2m">
                        <node concept="7r0gD" id="1jE3pLvF7Ry" role="2OqNvi">
                          <node concept="3cmrfG" id="1jE3pLvF7Rz" role="7T0AP">
                            <property role="3cmrfH" value="1" />
                          </node>
                        </node>
                        <node concept="37vLTw" id="1jE3pLvHNxb" role="2Oq$k0">
                          <ref role="3cqZAo" node="1jE3pLw3iyx" resolve="tail" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="1jE3pLwt4aA" role="3cqZAp" />
                  <node concept="3cpWs8" id="1jE3pLvF7R_" role="3cqZAp">
                    <node concept="3cpWsn" id="1jE3pLvF7RA" role="3cpWs9">
                      <property role="TrG5h" value="combinations" />
                      <node concept="A3Dl8" id="1jE3pLvF7RB" role="1tU5fm">
                        <node concept="A3Dl8" id="1jE3pLvF7RC" role="A3Ik2">
                          <node concept="3Tqbb2" id="1jE3pLvF7RD" role="A3Ik2">
                            <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="1jE3pLvF7RE" role="33vP2m">
                        <node concept="1fM9EW" id="1jE3pLvF7RF" role="2Oq$k0" />
                        <node concept="2qgKlT" id="1jE3pLvF7RG" role="2OqNvi">
                          <ref role="37wK5l" node="1jE3pLu_zgp" resolve="combinations" />
                          <node concept="3cpWsd" id="1jE3pLvF7RH" role="37wK5m">
                            <node concept="3cmrfG" id="1jE3pLvF7RI" role="3uHU7w">
                              <property role="3cmrfH" value="1" />
                            </node>
                            <node concept="37vLTw" id="1jE3pLvF7RJ" role="3uHU7B">
                              <ref role="3cqZAo" node="1jE3pLu_Edp" resolve="k" />
                            </node>
                          </node>
                          <node concept="37vLTw" id="1jE3pLvF7RK" role="37wK5m">
                            <ref role="3cqZAo" node="1jE3pLvF7Rt" resolve="xs" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="1jE3pLwdV6x" role="3cqZAp">
                    <node concept="2OqwBi" id="1jE3pLwdV6z" role="3clFbG">
                      <node concept="37vLTw" id="1jE3pLwdV6$" role="2Oq$k0">
                        <ref role="3cqZAo" node="1jE3pLvF7RA" resolve="combinations" />
                      </node>
                      <node concept="3$u5V9" id="1jE3pLwdV6_" role="2OqNvi">
                        <node concept="1bVj0M" id="1jE3pLwdV6A" role="23t8la">
                          <node concept="3clFbS" id="1jE3pLwdV6B" role="1bW5cS">
                            <node concept="3clFbF" id="1jE3pLwdV6C" role="3cqZAp">
                              <node concept="2OqwBi" id="1jE3pLwdV6D" role="3clFbG">
                                <node concept="2OqwBi" id="1jE3pLwdV6E" role="2Oq$k0">
                                  <node concept="1bVj0M" id="1jE3pLwdV6F" role="2Oq$k0">
                                    <node concept="3clFbS" id="1jE3pLwdV6G" role="1bW5cS">
                                      <node concept="2n63Yl" id="1jE3pLwdV6H" role="3cqZAp">
                                        <node concept="37vLTw" id="1jE3pLwdV6I" role="2n6tg2">
                                          <ref role="3cqZAo" node="1jE3pLvF7Rn" resolve="x" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                  <node concept="1Bd96e" id="1jE3pLwdV6J" role="2OqNvi" />
                                </node>
                                <node concept="3QWeyG" id="1jE3pLwdV6K" role="2OqNvi">
                                  <node concept="37vLTw" id="1jE3pLwdV6L" role="576Qk">
                                    <ref role="3cqZAo" node="1jE3pLwdV6M" resolve="it" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="Rh6nW" id="1jE3pLwdV6M" role="1bW2Oz">
                            <property role="TrG5h" value="it" />
                            <node concept="2jxLKc" id="1jE3pLwdV6N" role="1tU5fm" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="1jE3pLw3iyx" role="1bW2Oz">
                  <property role="TrG5h" value="tail" />
                  <node concept="2jxLKc" id="1jE3pLw3iyy" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1jE3pLu_Edp" role="3clF46">
        <property role="TrG5h" value="k" />
        <node concept="10Oyi0" id="1jE3pLu_Edo" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="1jE3pLu_Eez" role="3clF46">
        <property role="TrG5h" value="list" />
        <node concept="A3Dl8" id="1jE3pLu_Eid" role="1tU5fm">
          <node concept="3Tqbb2" id="1jE3pLu_FGn" role="A3Ik2">
            <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
          </node>
        </node>
      </node>
      <node concept="P$JXv" id="1jE3pLuG5Oc" role="lGtFl">
        <node concept="TZ5HA" id="1jE3pLuG5Od" role="TZ5H$">
          <node concept="1dT_AC" id="1jE3pLuG5Oe" role="1dT_Ay">
            <property role="1dT_AB" value="Returns each n-combination of Recipes once." />
          </node>
        </node>
        <node concept="TUZQ0" id="1jE3pLuG5Of" role="3nqlJM">
          <property role="TUZQ4" value="Number of elements to choose from for a combination." />
          <node concept="zr_55" id="1jE3pLuG5Oh" role="zr_5Q">
            <ref role="zr_51" node="1jE3pLu_Edp" resolve="k" />
          </node>
        </node>
        <node concept="TUZQ0" id="1jE3pLuG5Oi" role="3nqlJM">
          <property role="TUZQ4" value="Recipes to choose from." />
          <node concept="zr_55" id="1jE3pLuG5Ok" role="zr_5Q">
            <ref role="zr_51" node="1jE3pLu_Eez" resolve="list" />
          </node>
        </node>
        <node concept="x79VA" id="1jE3pLuG5Ol" role="3nqlJM">
          <property role="x79VB" value="Sequence of each n-combination, or the empty list if not possible." />
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="3mpKCXQvqBM">
    <ref role="13h7C2" to="u6v3:5mCwD4fNZLy" resolve="MealPlan" />
    <node concept="13i0hz" id="3mpKCXQvy9U" role="13h7CS">
      <property role="TrG5h" value="all_meals" />
      <node concept="3Tm1VV" id="3mpKCXQvy9V" role="1B3o_S" />
      <node concept="A3Dl8" id="3mpKCXQvCyx" role="3clF45">
        <node concept="3Tqbb2" id="3mpKCXQvCyz" role="A3Ik2">
          <ref role="ehGHo" to="u6v3:5mCwD4fO2$5" resolve="Meal" />
        </node>
      </node>
      <node concept="3clFbS" id="3mpKCXQvy9X" role="3clF47">
        <node concept="3clFbF" id="3mpKCXQvB3X" role="3cqZAp">
          <node concept="2OqwBi" id="3mpKCXQs89J" role="3clFbG">
            <node concept="2OqwBi" id="3mpKCXQs89K" role="2Oq$k0">
              <node concept="13iPFW" id="3mpKCXQvxOx" role="2Oq$k0" />
              <node concept="3Tsc0h" id="3mpKCXQs89M" role="2OqNvi">
                <ref role="3TtcxE" to="u6v3:5mCwD4fO8YZ" resolve="meal_plans" />
              </node>
            </node>
            <node concept="3goQfb" id="3mpKCXQs89N" role="2OqNvi">
              <node concept="1bVj0M" id="3mpKCXQs89O" role="23t8la">
                <node concept="3clFbS" id="3mpKCXQs89P" role="1bW5cS">
                  <node concept="3clFbF" id="3mpKCXQs89Q" role="3cqZAp">
                    <node concept="2OqwBi" id="3mpKCXQs89R" role="3clFbG">
                      <node concept="37vLTw" id="3mpKCXQs89S" role="2Oq$k0">
                        <ref role="3cqZAo" node="3mpKCXQs89U" resolve="it" />
                      </node>
                      <node concept="2qgKlT" id="3mpKCXQs89T" role="2OqNvi">
                        <ref role="37wK5l" node="3mpKCXP7K5v" resolve="all_meals" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="3mpKCXQs89U" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="3mpKCXQs89V" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="3mpKCXQvqDH" role="13h7CS">
      <property role="TrG5h" value="all_recipes" />
      <node concept="3Tm1VV" id="3mpKCXQvqDI" role="1B3o_S" />
      <node concept="2hMVRd" id="3mpKCXQvIQM" role="3clF45">
        <node concept="3Tqbb2" id="3mpKCXQvIQO" role="2hN53Y">
          <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
        </node>
      </node>
      <node concept="3clFbS" id="3mpKCXQvqDK" role="3clF47">
        <node concept="3clFbF" id="7mpslIOnZCI" role="3cqZAp">
          <node concept="2ShNRf" id="3mpKCXQs8xu" role="3clFbG">
            <node concept="2i4dXS" id="3mpKCXQs9C5" role="2ShVmc">
              <node concept="2OqwBi" id="3mpKCXQsbyn" role="I$8f6">
                <node concept="BsUDl" id="3mpKCXQvCLC" role="2Oq$k0">
                  <ref role="37wK5l" node="3mpKCXQvy9U" resolve="all_meals" />
                </node>
                <node concept="13MTOL" id="3mpKCXQsc3t" role="2OqNvi">
                  <ref role="13MTZf" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                </node>
              </node>
              <node concept="3Tqbb2" id="3mpKCXQsf56" role="HW$YZ">
                <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="3mpKCXQvqBN" role="13h7CW">
      <node concept="3clFbS" id="3mpKCXQvqBO" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="1jE3pLuDzDZ">
    <ref role="13h7C2" to="u6v3:7moC6rm27Me" resolve="Pantry" />
    <node concept="13i0hz" id="1jE3pLuDzFw" role="13h7CS">
      <property role="TrG5h" value="can_provide" />
      <node concept="3Tm1VV" id="1jE3pLuDzFx" role="1B3o_S" />
      <node concept="10P_77" id="1jE3pLuEIr$" role="3clF45" />
      <node concept="3clFbS" id="1jE3pLuDzFz" role="3clF47">
        <node concept="3SKdUt" id="1jE3pLuFkYi" role="3cqZAp">
          <node concept="1PaTwC" id="1jE3pLuFkYj" role="1aUNEU">
            <node concept="3oM_SD" id="1jE3pLuFl9B" role="1PaTwD">
              <property role="3oM_SC" value="Calculate" />
            </node>
            <node concept="3oM_SD" id="1jE3pLuFle7" role="1PaTwD">
              <property role="3oM_SC" value="required" />
            </node>
            <node concept="3oM_SD" id="1jE3pLuFliC" role="1PaTwD">
              <property role="3oM_SC" value="amount" />
            </node>
            <node concept="3oM_SD" id="1jE3pLuFlly" role="1PaTwD">
              <property role="3oM_SC" value="for" />
            </node>
            <node concept="3oM_SD" id="1jE3pLuFlmr" role="1PaTwD">
              <property role="3oM_SC" value="each" />
            </node>
            <node concept="3oM_SD" id="1jE3pLuFlsB" role="1PaTwD">
              <property role="3oM_SC" value="ingredient." />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1jE3pLuEK05" role="3cqZAp">
          <node concept="3cpWsn" id="1jE3pLuEK06" role="3cpWs9">
            <property role="TrG5h" value="required_stock" />
            <node concept="3rvAFt" id="1jE3pLuEK07" role="1tU5fm">
              <node concept="3Tqbb2" id="1jE3pLuEK08" role="3rvQeY">
                <ref role="ehGHo" to="u6v3:6dt6wfGrpXm" resolve="Ingredient" />
              </node>
              <node concept="10Oyi0" id="1jE3pLuEK09" role="3rvSg0" />
            </node>
            <node concept="2ShNRf" id="1jE3pLuEK0a" role="33vP2m">
              <node concept="3rGOSV" id="1jE3pLuEK0b" role="2ShVmc">
                <node concept="3Tqbb2" id="1jE3pLuEK0c" role="3rHrn6">
                  <ref role="ehGHo" to="u6v3:6dt6wfGrpXm" resolve="Ingredient" />
                </node>
                <node concept="10Oyi0" id="1jE3pLuEK0d" role="3rHtpV" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="1jE3pLuEK0e" role="3cqZAp">
          <node concept="2GrKxI" id="1jE3pLuEK0f" role="2Gsz3X">
            <property role="TrG5h" value="ingredient" />
          </node>
          <node concept="2OqwBi" id="1jE3pLuEK0g" role="2GsD0m">
            <node concept="37vLTw" id="1jE3pLuENA1" role="2Oq$k0">
              <ref role="3cqZAo" node="1jE3pLuEJ12" resolve="recipes" />
            </node>
            <node concept="13MTOL" id="1jE3pLuEK0i" role="2OqNvi">
              <ref role="13MTZf" to="u6v3:6dt6wfGrvix" resolve="ingredients" />
            </node>
          </node>
          <node concept="3clFbS" id="1jE3pLuEK0j" role="2LFqv$">
            <node concept="3clFbF" id="1jE3pLuEK0k" role="3cqZAp">
              <node concept="37vLTI" id="1jE3pLuEK0l" role="3clFbG">
                <node concept="3cpWs3" id="1jE3pLwL$Ml" role="37vLTx">
                  <node concept="1eOMI4" id="1jE3pLuEK0n" role="3uHU7B">
                    <node concept="3K4zz7" id="1jE3pLuEK0o" role="1eOMHV">
                      <node concept="2OqwBi" id="1jE3pLuEK0p" role="3K4Cdx">
                        <node concept="37vLTw" id="1jE3pLuEK0q" role="2Oq$k0">
                          <ref role="3cqZAo" node="1jE3pLuEK06" resolve="required_stock" />
                        </node>
                        <node concept="2Nt0df" id="1jE3pLuEK0r" role="2OqNvi">
                          <node concept="2OqwBi" id="1jE3pLuEK0s" role="38cxEo">
                            <node concept="2GrUjf" id="1jE3pLuEK0t" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="1jE3pLuEK0f" resolve="ingredient" />
                            </node>
                            <node concept="3TrEf2" id="1jE3pLuEK0u" role="2OqNvi">
                              <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3cmrfG" id="1jE3pLuEK0v" role="3K4GZi">
                        <property role="3cmrfH" value="0" />
                      </node>
                      <node concept="3EllGN" id="1jE3pLuEK0w" role="3K4E3e">
                        <node concept="2OqwBi" id="1jE3pLuEK0x" role="3ElVtu">
                          <node concept="2GrUjf" id="1jE3pLuEK0y" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="1jE3pLuEK0f" resolve="ingredient" />
                          </node>
                          <node concept="3TrEf2" id="1jE3pLuEK0z" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                          </node>
                        </node>
                        <node concept="37vLTw" id="1jE3pLuEK0$" role="3ElQJh">
                          <ref role="3cqZAo" node="1jE3pLuEK06" resolve="required_stock" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="17qRlL" id="1jE3pLuEK0_" role="3uHU7w">
                    <node concept="2OqwBi" id="1jE3pLuEK0A" role="3uHU7w">
                      <node concept="2GrUjf" id="1jE3pLuEK0B" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="1jE3pLuEK0f" resolve="ingredient" />
                      </node>
                      <node concept="3TrcHB" id="1jE3pLuEK0C" role="2OqNvi">
                        <ref role="3TsBF5" to="u6v3:6dt6wfGrviq" resolve="amount" />
                      </node>
                    </node>
                    <node concept="37vLTw" id="1jE3pLuEK0D" role="3uHU7B">
                      <ref role="3cqZAo" node="1jE3pLuEIuk" resolve="meals_per_recipe" />
                    </node>
                  </node>
                </node>
                <node concept="3EllGN" id="1jE3pLuEK0E" role="37vLTJ">
                  <node concept="2OqwBi" id="1jE3pLuEK0F" role="3ElVtu">
                    <node concept="2GrUjf" id="1jE3pLuEK0G" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="1jE3pLuEK0f" resolve="ingredient" />
                    </node>
                    <node concept="3TrEf2" id="1jE3pLuEK0H" role="2OqNvi">
                      <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="1jE3pLuEK0I" role="3ElQJh">
                    <ref role="3cqZAo" node="1jE3pLuEK06" resolve="required_stock" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1jE3pLuFcmQ" role="3cqZAp" />
        <node concept="3SKdUt" id="1jE3pLwLFcC" role="3cqZAp">
          <node concept="1PaTwC" id="1jE3pLwLFcD" role="1aUNEU">
            <node concept="3oM_SD" id="1jE3pLwLFgn" role="1PaTwD">
              <property role="3oM_SC" value="" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFwp" role="1PaTwD">
              <property role="3oM_SC" value="Calculate" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFjD" role="1PaTwD">
              <property role="3oM_SC" value="available" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFyT" role="1PaTwD">
              <property role="3oM_SC" value="amount" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFCu" role="1PaTwD">
              <property role="3oM_SC" value="for" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFDe" role="1PaTwD">
              <property role="3oM_SC" value="each" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFEp" role="1PaTwD">
              <property role="3oM_SC" value="ingredient" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFJ5" role="1PaTwD">
              <property role="3oM_SC" value="in" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFK2" role="1PaTwD">
              <property role="3oM_SC" value="the" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwLFLg" role="1PaTwD">
              <property role="3oM_SC" value="pantry." />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1jE3pLwLstF" role="3cqZAp">
          <node concept="3cpWsn" id="1jE3pLwLstI" role="3cpWs9">
            <property role="TrG5h" value="available_stock" />
            <node concept="3rvAFt" id="1jE3pLwLstJ" role="1tU5fm">
              <node concept="3Tqbb2" id="1jE3pLwLstK" role="3rvQeY">
                <ref role="ehGHo" to="u6v3:6dt6wfGrpXm" resolve="Ingredient" />
              </node>
              <node concept="10Oyi0" id="1jE3pLwLstL" role="3rvSg0" />
            </node>
            <node concept="2ShNRf" id="1jE3pLwLstM" role="33vP2m">
              <node concept="3rGOSV" id="1jE3pLwLstN" role="2ShVmc">
                <node concept="3Tqbb2" id="1jE3pLwLstO" role="3rHrn6">
                  <ref role="ehGHo" to="u6v3:6dt6wfGrpXm" resolve="Ingredient" />
                </node>
                <node concept="10Oyi0" id="1jE3pLwLstP" role="3rHtpV" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="1jE3pLwLtUu" role="3cqZAp">
          <node concept="2GrKxI" id="1jE3pLwLtUv" role="2Gsz3X">
            <property role="TrG5h" value="ingredient" />
          </node>
          <node concept="2OqwBi" id="1jE3pLwLvUF" role="2GsD0m">
            <node concept="13iPFW" id="1jE3pLwLv_x" role="2Oq$k0" />
            <node concept="3Tsc0h" id="1jE3pLwLwho" role="2OqNvi">
              <ref role="3TtcxE" to="u6v3:7moC6rm27TX" resolve="staples" />
            </node>
          </node>
          <node concept="3clFbS" id="1jE3pLwLtUz" role="2LFqv$">
            <node concept="3clFbF" id="1jE3pLwLtU$" role="3cqZAp">
              <node concept="37vLTI" id="1jE3pLwLtU_" role="3clFbG">
                <node concept="3cpWs3" id="1jE3pLwLCeC" role="37vLTx">
                  <node concept="1eOMI4" id="1jE3pLwLtUB" role="3uHU7B">
                    <node concept="3K4zz7" id="1jE3pLwLtUC" role="1eOMHV">
                      <node concept="2OqwBi" id="1jE3pLwLtUD" role="3K4Cdx">
                        <node concept="37vLTw" id="1jE3pLwLtUE" role="2Oq$k0">
                          <ref role="3cqZAo" node="1jE3pLwLstI" resolve="available_stock" />
                        </node>
                        <node concept="2Nt0df" id="1jE3pLwLtUF" role="2OqNvi">
                          <node concept="2OqwBi" id="1jE3pLwLtUG" role="38cxEo">
                            <node concept="2GrUjf" id="1jE3pLwLtUH" role="2Oq$k0">
                              <ref role="2Gs0qQ" node="1jE3pLwLtUv" resolve="ingredient" />
                            </node>
                            <node concept="3TrEf2" id="1jE3pLwLtUI" role="2OqNvi">
                              <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3cmrfG" id="1jE3pLwLtUJ" role="3K4GZi">
                        <property role="3cmrfH" value="0" />
                      </node>
                      <node concept="3EllGN" id="1jE3pLwLtUK" role="3K4E3e">
                        <node concept="2OqwBi" id="1jE3pLwLtUL" role="3ElVtu">
                          <node concept="2GrUjf" id="1jE3pLwLtUM" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="1jE3pLwLtUv" resolve="ingredient" />
                          </node>
                          <node concept="3TrEf2" id="1jE3pLwLtUN" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                          </node>
                        </node>
                        <node concept="37vLTw" id="1jE3pLwLtUO" role="3ElQJh">
                          <ref role="3cqZAo" node="1jE3pLwLstI" resolve="available_stock" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="1jE3pLwLtUQ" role="3uHU7w">
                    <node concept="2GrUjf" id="1jE3pLwLtUR" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="1jE3pLwLtUv" resolve="ingredient" />
                    </node>
                    <node concept="3TrcHB" id="1jE3pLwLtUS" role="2OqNvi">
                      <ref role="3TsBF5" to="u6v3:6dt6wfGrviq" resolve="amount" />
                    </node>
                  </node>
                </node>
                <node concept="3EllGN" id="1jE3pLwLtUU" role="37vLTJ">
                  <node concept="2OqwBi" id="1jE3pLwLtUV" role="3ElVtu">
                    <node concept="2GrUjf" id="1jE3pLwLtUW" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="1jE3pLwLtUv" resolve="ingredient" />
                    </node>
                    <node concept="3TrEf2" id="1jE3pLwLtUX" role="2OqNvi">
                      <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="1jE3pLwLtUY" role="3ElQJh">
                    <ref role="3cqZAo" node="1jE3pLwLstI" resolve="available_stock" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1jE3pLuFbAY" role="3cqZAp" />
        <node concept="3SKdUt" id="1jE3pLwMd_L" role="3cqZAp">
          <node concept="1PaTwC" id="1jE3pLwMd_M" role="1aUNEU">
            <node concept="3oM_SD" id="1jE3pLwMdUG" role="1PaTwD">
              <property role="3oM_SC" value="Check" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMdDN" role="1PaTwD">
              <property role="3oM_SC" value="if" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMdRx" role="1PaTwD">
              <property role="3oM_SC" value="pantry" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMdVO" role="1PaTwD">
              <property role="3oM_SC" value="contains" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMdWX" role="1PaTwD">
              <property role="3oM_SC" value="the" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMdXR" role="1PaTwD">
              <property role="3oM_SC" value="ingredient" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMe4P" role="1PaTwD">
              <property role="3oM_SC" value="and" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMe5n" role="1PaTwD">
              <property role="3oM_SC" value="has" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMe6a" role="1PaTwD">
              <property role="3oM_SC" value="enough" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMe7y" role="1PaTwD">
              <property role="3oM_SC" value="of" />
            </node>
            <node concept="3oM_SD" id="1jE3pLwMe8L" role="1PaTwD">
              <property role="3oM_SC" value="it." />
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="1jE3pLwLSHs" role="3cqZAp">
          <node concept="2GrKxI" id="1jE3pLwLSHu" role="2Gsz3X">
            <property role="TrG5h" value="ingredient" />
          </node>
          <node concept="37vLTw" id="1jE3pLwLVGV" role="2GsD0m">
            <ref role="3cqZAo" node="1jE3pLuEK06" resolve="required_stock" />
          </node>
          <node concept="3clFbS" id="1jE3pLwLSHy" role="2LFqv$">
            <node concept="3clFbJ" id="1jE3pLwLWDm" role="3cqZAp">
              <node concept="3fqX7Q" id="1jE3pLwM32f" role="3clFbw">
                <node concept="2OqwBi" id="1jE3pLwM32h" role="3fr31v">
                  <node concept="37vLTw" id="1jE3pLwM32i" role="2Oq$k0">
                    <ref role="3cqZAo" node="1jE3pLwLstI" resolve="available_stock" />
                  </node>
                  <node concept="2Nt0df" id="1jE3pLwM32j" role="2OqNvi">
                    <node concept="2OqwBi" id="1jE3pLwM4lP" role="38cxEo">
                      <node concept="2GrUjf" id="1jE3pLwM3Wu" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="1jE3pLwLSHu" resolve="ingredient" />
                      </node>
                      <node concept="3AY5_j" id="1jE3pLwM5c6" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbS" id="1jE3pLwLWDo" role="3clFbx">
                <node concept="3cpWs6" id="1jE3pLwM3iX" role="3cqZAp">
                  <node concept="3clFbT" id="1jE3pLwM3y2" role="3cqZAk" />
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="1jE3pLwM5Bl" role="3cqZAp">
              <node concept="3cpWsn" id="1jE3pLwM5Bm" role="3cpWs9">
                <property role="TrG5h" value="too_little_stock" />
                <node concept="10P_77" id="1jE3pLwM5Bn" role="1tU5fm" />
                <node concept="3eOSWO" id="1jE3pLwM5Bo" role="33vP2m">
                  <node concept="2OqwBi" id="1jE3pLwM7GK" role="3uHU7B">
                    <node concept="2GrUjf" id="1jE3pLwM7aM" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="1jE3pLwLSHu" resolve="ingredient" />
                    </node>
                    <node concept="3AV6Ez" id="1jE3pLwM8OM" role="2OqNvi" />
                  </node>
                  <node concept="3EllGN" id="1jE3pLwMakc" role="3uHU7w">
                    <node concept="2OqwBi" id="1jE3pLwMaUN" role="3ElVtu">
                      <node concept="2GrUjf" id="1jE3pLwMaBT" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="1jE3pLwLSHu" resolve="ingredient" />
                      </node>
                      <node concept="3AY5_j" id="1jE3pLwMby4" role="2OqNvi" />
                    </node>
                    <node concept="37vLTw" id="1jE3pLwM9sl" role="3ElQJh">
                      <ref role="3cqZAo" node="1jE3pLwLstI" resolve="available_stock" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="1jE3pLwM5Bx" role="3cqZAp">
              <node concept="37vLTw" id="1jE3pLwM5By" role="3clFbw">
                <ref role="3cqZAo" node="1jE3pLwM5Bm" resolve="too_little_stock" />
              </node>
              <node concept="3clFbS" id="1jE3pLwM5Bz" role="3clFbx">
                <node concept="3cpWs6" id="1jE3pLwM5B$" role="3cqZAp">
                  <node concept="3clFbT" id="1jE3pLwM5B_" role="3cqZAk" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1jE3pLuENak" role="3cqZAp">
          <node concept="3clFbT" id="1jE3pLuENdf" role="3cqZAk">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="1jE3pLuEIuk" role="3clF46">
        <property role="TrG5h" value="meals_per_recipe" />
        <node concept="10Oyi0" id="1jE3pLuEIuj" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="1jE3pLuEJ12" role="3clF46">
        <property role="TrG5h" value="recipes" />
        <node concept="A3Dl8" id="1jE3pLuEJ3Q" role="1tU5fm">
          <node concept="3Tqbb2" id="1jE3pLuEJ7w" role="A3Ik2">
            <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
          </node>
        </node>
      </node>
      <node concept="P$JXv" id="1jE3pLwSEVi" role="lGtFl">
        <node concept="TZ5HA" id="1jE3pLwSEVj" role="TZ5H$">
          <node concept="1dT_AC" id="1jE3pLwSEVk" role="1dT_Ay">
            <property role="1dT_AB" value="Checks whether the pantry can provide the requested meals from stock." />
          </node>
        </node>
        <node concept="TUZQ0" id="1jE3pLwSEVl" role="3nqlJM">
          <property role="TUZQ4" value="Number of portions to be prepared per meal" />
          <node concept="zr_55" id="1jE3pLwSEVn" role="zr_5Q">
            <ref role="zr_51" node="1jE3pLuEIuk" resolve="meals_per_recipe" />
          </node>
        </node>
        <node concept="TUZQ0" id="1jE3pLwSEVo" role="3nqlJM">
          <property role="TUZQ4" value="Recipes for each meal to be cooked" />
          <node concept="zr_55" id="1jE3pLwSEVq" role="zr_5Q">
            <ref role="zr_51" node="1jE3pLuEJ12" resolve="recipes" />
          </node>
        </node>
        <node concept="x79VA" id="1jE3pLwSEVr" role="3nqlJM">
          <property role="x79VB" value="true iff the pantry can provide the meals" />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="1jE3pLuDzE0" role="13h7CW">
      <node concept="3clFbS" id="1jE3pLuDzE1" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="5boToVNcGTB">
    <ref role="13h7C2" to="u6v3:6dt6wfGrvip" resolve="MeasuredIngedient" />
    <node concept="13i0hz" id="5boToVNcH3S" role="13h7CS">
      <property role="TrG5h" value="calories" />
      <node concept="3Tm1VV" id="5boToVNcH3T" role="1B3o_S" />
      <node concept="10Oyi0" id="5boToVNcHik" role="3clF45" />
      <node concept="3clFbS" id="5boToVNcH3V" role="3clF47">
        <node concept="3cpWs8" id="5boToVNcLpw" role="3cqZAp">
          <node concept="3cpWsn" id="5boToVNcLpz" role="3cpWs9">
            <property role="TrG5h" value="weight_in_gram" />
            <node concept="10Oyi0" id="5boToVNcLpu" role="1tU5fm" />
            <node concept="10QFUN" id="5boToVNdZp$" role="33vP2m">
              <node concept="10Oyi0" id="5boToVNdZsr" role="10QFUM" />
              <node concept="1eOMI4" id="5boToVNe0qY" role="10QFUP">
                <node concept="17qRlL" id="5boToVNcMLx" role="1eOMHV">
                  <node concept="2OqwBi" id="5boToVNcJ3X" role="3uHU7B">
                    <node concept="13iPFW" id="5boToVNcIRT" role="2Oq$k0" />
                    <node concept="3TrcHB" id="5boToVNcJfi" role="2OqNvi">
                      <ref role="3TsBF5" to="u6v3:6dt6wfGrviq" resolve="amount" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="5boToVNcNiP" role="3uHU7w">
                    <node concept="2OqwBi" id="5boToVNcMY4" role="2Oq$k0">
                      <node concept="13iPFW" id="5boToVNcMNH" role="2Oq$k0" />
                      <node concept="3TrEf2" id="5boToVNcN7j" role="2OqNvi">
                        <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                      </node>
                    </node>
                    <node concept="2qgKlT" id="5boToVNdYe5" role="2OqNvi">
                      <ref role="37wK5l" node="5boToVNcPSD" resolve="to_gram_factor" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5boToVNcKpm" role="3cqZAp">
          <node concept="FJ1c_" id="5boToVNdWUo" role="3clFbG">
            <node concept="3cmrfG" id="5boToVNdWV6" role="3uHU7w">
              <property role="3cmrfH" value="100" />
            </node>
            <node concept="17qRlL" id="5boToVNcP9T" role="3uHU7B">
              <node concept="2OqwBi" id="5boToVNcL0A" role="3uHU7B">
                <node concept="2OqwBi" id="5boToVNcKzO" role="2Oq$k0">
                  <node concept="13iPFW" id="5boToVNcKpk" role="2Oq$k0" />
                  <node concept="3TrEf2" id="5boToVNcKO$" role="2OqNvi">
                    <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                  </node>
                </node>
                <node concept="3TrcHB" id="5boToVNcLfD" role="2OqNvi">
                  <ref role="3TsBF5" to="u6v3:6dt6wfGrvi8" resolve="calories" />
                </node>
              </node>
              <node concept="37vLTw" id="5boToVNcPyK" role="3uHU7w">
                <ref role="3cqZAo" node="5boToVNcLpz" resolve="weight_in_gram" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="P$JXv" id="5boToVNcHzT" role="lGtFl">
        <node concept="TZ5HA" id="5boToVNcHzU" role="TZ5H$">
          <node concept="1dT_AC" id="5boToVNcHzV" role="1dT_Ay">
            <property role="1dT_AB" value="Returns the total calories." />
          </node>
        </node>
        <node concept="x79VA" id="5boToVNcIx2" role="3nqlJM">
          <property role="x79VB" value="Calories in kJ" />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="5boToVNcGTC" role="13h7CW">
      <node concept="3clFbS" id="5boToVNcGTD" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="5boToVNcPQI">
    <ref role="13h7C2" to="u6v3:6dt6wfGrpXm" resolve="Ingredient" />
    <node concept="13i0hz" id="5boToVNcPSD" role="13h7CS">
      <property role="TrG5h" value="to_gram_factor" />
      <node concept="3Tm1VV" id="5boToVNcPSE" role="1B3o_S" />
      <node concept="10P55v" id="5boToVNcQbW" role="3clF45" />
      <node concept="3clFbS" id="5boToVNcPSG" role="3clF47">
        <node concept="3cpWs8" id="5boToVNn3Ni" role="3cqZAp">
          <node concept="3cpWsn" id="5boToVNn3Nl" role="3cpWs9">
            <property role="TrG5h" value="gram" />
            <node concept="3Tqbb2" id="5boToVNn3Ng" role="1tU5fm">
              <ref role="ehGHo" to="u6v3:5boToVMCwmF" resolve="Unit" />
            </node>
            <node concept="2OqwBi" id="5boToVNne$J" role="33vP2m">
              <node concept="2tJFMh" id="5boToVNndGV" role="2Oq$k0">
                <node concept="ZC_QK" id="5boToVNndOp" role="2tJFKM">
                  <ref role="2aWVGs" to="cdrp:5boToVMQyDe" resolve="Gram" />
                </node>
              </node>
              <node concept="Vyspw" id="5boToVNnfi6" role="2OqNvi">
                <node concept="2OqwBi" id="5boToVNnjJE" role="Vysub">
                  <node concept="2JrnkZ" id="5boToVNnjAZ" role="2Oq$k0">
                    <node concept="2OqwBi" id="5boToVNnf$r" role="2JrQYb">
                      <node concept="13iPFW" id="5boToVNnfny" role="2Oq$k0" />
                      <node concept="I4A8Y" id="5boToVNnfN$" role="2OqNvi" />
                    </node>
                  </node>
                  <node concept="liA8E" id="5boToVNnkkJ" role="2OqNvi">
                    <ref role="37wK5l" to="mhbf:~SModel.getRepository()" resolve="getRepository" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5boToVNndzs" role="3cqZAp" />
        <node concept="3clFbJ" id="5boToVNcQz$" role="3cqZAp">
          <node concept="3clFbS" id="5boToVNcQzA" role="3clFbx">
            <node concept="3cpWs6" id="5boToVNiP5l" role="3cqZAp">
              <node concept="3b6qkQ" id="5boToVNiPYu" role="3cqZAk">
                <property role="$nhwW" value="1.0" />
              </node>
            </node>
          </node>
          <node concept="17R0WA" id="5boToVNcR6R" role="3clFbw">
            <node concept="2OqwBi" id="5boToVNcQBH" role="3uHU7B">
              <node concept="13iPFW" id="5boToVNcQ_1" role="2Oq$k0" />
              <node concept="3TrEf2" id="5boToVNcQJK" role="2OqNvi">
                <ref role="3Tt5mk" to="u6v3:5boToVMSz92" resolve="unit" />
              </node>
            </node>
            <node concept="37vLTw" id="5boToVNnlg4" role="3uHU7w">
              <ref role="3cqZAo" node="5boToVNn3Nl" resolve="gram" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5boToVNdnUP" role="3cqZAp">
          <node concept="FJ1c_" id="5boToVNdp9G" role="3cqZAk">
            <node concept="2OqwBi" id="5boToVNdpuo" role="3uHU7w">
              <node concept="13iPFW" id="5boToVNdpbC" role="2Oq$k0" />
              <node concept="3TrcHB" id="5boToVNdp$6" role="2OqNvi">
                <ref role="3TsBF5" to="u6v3:5boToVN27le" resolve="to_gram_divisor" />
              </node>
            </node>
            <node concept="2OqwBi" id="5boToVNcQtW" role="3uHU7B">
              <node concept="13iPFW" id="5boToVNcQh7" role="2Oq$k0" />
              <node concept="3TrcHB" id="5boToVNdoam" role="2OqNvi">
                <ref role="3TsBF5" to="u6v3:5boToVN275x" resolve="to_gram_dividend" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="5boToVNcPQJ" role="13h7CW">
      <node concept="3clFbS" id="5boToVNcPQK" role="2VODD2" />
    </node>
  </node>
</model>


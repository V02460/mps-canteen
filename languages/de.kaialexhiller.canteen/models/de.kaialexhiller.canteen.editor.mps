<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0642a07a-168e-4d07-b0e6-40365c3d69c5(de.kaialexhiller.canteen.editor)">
  <persistence version="9" />
  <attribute name="doNotGenerate" value="false" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="14" />
    <use id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging" version="0" />
    <use id="1a8554c4-eb84-43ba-8c34-6f0d90c6e75a" name="jetbrains.mps.lang.smodel.query" version="3" />
    <use id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation" version="5" />
  </languages>
  <imports>
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="lui2" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.module(MPS.OpenAPI/)" />
    <import index="hyam" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.event(JDK/)" />
    <import index="cdrp" ref="r:270557f3-36cf-46df-9cab-7e28678872b1(de.kaialexhiller.canteen.units)" />
    <import index="u6v3" ref="r:4e321182-9bcb-42b3-94db-47d29f7a99dc(de.kaialexhiller.canteen.structure)" implicit="true" />
    <import index="cj4x" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.openapi.editor(MPS.Editor/)" implicit="true" />
    <import index="tpcu" ref="r:00000000-0000-4000-0000-011c89590282(jetbrains.mps.lang.core.behavior)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="xqb1" ref="r:04ec1dc0-bcb1-4130-ab2a-ea1eb63d2e83(de.kaialexhiller.canteen.behavior)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1226339813308" name="jetbrains.mps.lang.editor.structure.PaddingBottomStyleClassItem" flags="ln" index="27z8qx" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="928328222691832421" name="separatorTextQuery" index="2gpyvW" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="709996738298806197" name="jetbrains.mps.lang.editor.structure.QueryFunction_SeparatorText" flags="in" index="2o9xnK" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1186414976055" name="jetbrains.mps.lang.editor.structure.DrawBorderStyleClassItem" flags="ln" index="VPXOz" />
      <concept id="1186415722038" name="jetbrains.mps.lang.editor.structure.FontSizeStyleClassItem" flags="ln" index="VSNWy">
        <property id="1221209241505" name="value" index="1lJzqX" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1233759184865" name="jetbrains.mps.lang.editor.structure.PunctuationRightStyleClassItem" flags="ln" index="11LMrY" />
      <concept id="8313721352726366579" name="jetbrains.mps.lang.editor.structure.CellModel_Empty" flags="ng" index="35HoNQ" />
      <concept id="1103016434866" name="jetbrains.mps.lang.editor.structure.CellModel_JComponent" flags="sg" stub="8104358048506731196" index="3gTLQM">
        <child id="1176475119347" name="componentProvider" index="3FoqZy" />
      </concept>
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1215007762405" name="jetbrains.mps.lang.editor.structure.FloatStyleClassItem" flags="ln" index="3$6MrZ">
        <property id="1215007802031" name="value" index="3$6WeP" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1214560368769" name="emptyNoTargetText" index="39s7Ar" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1176474535556" name="jetbrains.mps.lang.editor.structure.QueryFunction_JComponent" flags="in" index="3Fmcul" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1161622981231" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1Q80Hx" />
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="8118189177080264853" name="jetbrains.mps.baseLanguage.structure.AlternativeType" flags="ig" index="nSUau">
        <child id="8118189177080264854" name="alternative" index="nSUat" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="4952749571008284462" name="jetbrains.mps.baseLanguage.structure.CatchVariable" flags="ng" index="XOnhg" />
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1513279640923991009" name="jetbrains.mps.baseLanguage.structure.IGenericClassCreator" flags="ng" index="366HgL">
        <property id="1513279640906337053" name="inferTypeParams" index="373rjd" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271221393" name="jetbrains.mps.baseLanguage.structure.NPENotEqualsExpression" flags="nn" index="17QLQc" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="3093926081414150598" name="jetbrains.mps.baseLanguage.structure.MultipleCatchClause" flags="ng" index="3uVAMA">
        <child id="8276990574895933173" name="catchBody" index="1zc67A" />
        <child id="8276990574895933172" name="throwable" index="1zc67B" />
      </concept>
      <concept id="5351203823916750322" name="jetbrains.mps.baseLanguage.structure.TryUniversalStatement" flags="nn" index="3J1_TO">
        <child id="8276990574886367510" name="catchClause" index="1zxBo5" />
        <child id="8276990574886367508" name="body" index="1zxBo7" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157994012186" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitLink" flags="ng" index="2pIpSj">
        <reference id="5455284157994012188" name="link" index="2pIpSl" />
        <child id="1595412875168045827" name="initValue" index="28nt2d" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="8182547171709752110" name="jetbrains.mps.lang.quotation.structure.NodeBuilderExpression" flags="nn" index="36biLy">
        <child id="8182547171709752112" name="expression" index="36biLW" />
      </concept>
    </language>
    <language id="446c26eb-2b7b-4bf0-9b35-f83fa582753e" name="jetbrains.mps.lang.modelapi">
      <concept id="4733039728785194814" name="jetbrains.mps.lang.modelapi.structure.NamedNodeReference" flags="ng" index="ZC_QK">
        <reference id="7256306938026143658" name="target" index="2aWVGs" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
        <child id="5721587534047265375" name="throwable" index="9lYJj" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7400021826774799413" name="jetbrains.mps.lang.smodel.structure.NodePointerExpression" flags="ng" index="2tJFMh">
        <child id="7400021826774799510" name="ref" index="2tJFKM" />
      </concept>
      <concept id="4065387505485742749" name="jetbrains.mps.lang.smodel.structure.AbstractPointerResolveOperation" flags="ng" index="2yCiFS">
        <child id="3648723375513868575" name="repositoryArg" index="Vysub" />
      </concept>
      <concept id="1143234257716" name="jetbrains.mps.lang.smodel.structure.Node_GetModelOperation" flags="nn" index="I4A8Y" />
      <concept id="3648723375513868532" name="jetbrains.mps.lang.smodel.structure.NodePointer_ResolveOperation" flags="ng" index="Vyspw" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="role_DebugInfo" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
    <language id="1a8554c4-eb84-43ba-8c34-6f0d90c6e75a" name="jetbrains.mps.lang.smodel.query">
      <concept id="7738379549910147341" name="jetbrains.mps.lang.smodel.query.structure.InstancesExpression" flags="ng" index="qVDSY">
        <child id="7738379549910147342" name="conceptArg" index="qVDSX" />
      </concept>
      <concept id="4234138103881610891" name="jetbrains.mps.lang.smodel.query.structure.WithStatement" flags="ng" index="L3pyB">
        <child id="4234138103881610935" name="scope" index="L3pyr" />
        <child id="4234138103881610892" name="stmts" index="L3pyw" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1240687580870" name="jetbrains.mps.baseLanguage.collections.structure.JoinOperation" flags="nn" index="3uJxvA">
        <child id="1240687658305" name="delimiter" index="3uJOhx" />
      </concept>
      <concept id="1165595910856" name="jetbrains.mps.baseLanguage.collections.structure.GetLastOperation" flags="nn" index="1yVyf7" />
      <concept id="1202128969694" name="jetbrains.mps.baseLanguage.collections.structure.SelectOperation" flags="nn" index="3$u5V9" />
    </language>
  </registry>
  <node concept="24kQdi" id="5mCwD4fO9Ur">
    <ref role="1XX52x" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
    <node concept="3EZMnI" id="5mCwD4fO9Xj" role="2wV5jI">
      <node concept="2iRkQZ" id="5mCwD4fO9Xk" role="2iSdaV" />
      <node concept="3EZMnI" id="5mCwD4fOacS" role="3EZMnx">
        <node concept="VPM3Z" id="5mCwD4fOacU" role="3F10Kt" />
        <node concept="VSNWy" id="8EeBQEq$yt" role="3F10Kt">
          <property role="1lJzqX" value="14" />
        </node>
        <node concept="3F0A7n" id="5mCwD4fOah7" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5mCwD4fO8zh" resolve="year" />
        </node>
        <node concept="3F0ifn" id="5mCwD4fOazk" role="3EZMnx">
          <property role="3F0ifm" value="-W" />
          <node concept="11L4FC" id="5mCwD4fOaIP" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="11LMrY" id="5mCwD4fOaKy" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0ifn" id="7moC6rlMjOU" role="3EZMnx">
          <property role="3F0ifm" value="0" />
          <node concept="pkWqt" id="7moC6rlMjSI" role="pqm2j">
            <node concept="3clFbS" id="7moC6rlMjSJ" role="2VODD2">
              <node concept="3clFbF" id="7moC6rlMjYY" role="3cqZAp">
                <node concept="3eOVzh" id="7moC6rlMmuR" role="3clFbG">
                  <node concept="3cmrfG" id="7moC6rlMmxa" role="3uHU7w">
                    <property role="3cmrfH" value="10" />
                  </node>
                  <node concept="2OqwBi" id="7moC6rlMkdl" role="3uHU7B">
                    <node concept="pncrf" id="7moC6rlMjYX" role="2Oq$k0" />
                    <node concept="3TrcHB" id="7moC6rlMk_i" role="2OqNvi">
                      <ref role="3TsBF5" to="u6v3:5mCwD4fO8Ed" resolve="calendar_week" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="11LMrY" id="7moC6rlTDo9" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0A7n" id="5mCwD4fOanl" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5mCwD4fO8Ed" resolve="calendar_week" />
        </node>
        <node concept="2iRfu4" id="5mCwD4fOacX" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="5mCwD4fOIJV" role="3EZMnx">
        <node concept="3F0ifn" id="5mCwD4fOIY9" role="3EZMnx">
          <property role="3F0ifm" value="1" />
        </node>
        <node concept="2iRfu4" id="5mCwD4fOIJW" role="2iSdaV" />
        <node concept="3F1sOY" id="5mCwD4fOIPx" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5mCwD4fOH8F" resolve="monday" />
        </node>
      </node>
      <node concept="3EZMnI" id="5mCwD4fOJ49" role="3EZMnx">
        <node concept="3F0ifn" id="5mCwD4fOJ4a" role="3EZMnx">
          <property role="3F0ifm" value="2" />
        </node>
        <node concept="2iRfu4" id="5mCwD4fOJ4b" role="2iSdaV" />
        <node concept="3F1sOY" id="5mCwD4fOJ4c" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5mCwD4fOHfe" resolve="tuesday" />
        </node>
      </node>
      <node concept="3EZMnI" id="5mCwD4fOJ6t" role="3EZMnx">
        <node concept="3F0ifn" id="5mCwD4fOJ6u" role="3EZMnx">
          <property role="3F0ifm" value="3" />
        </node>
        <node concept="2iRfu4" id="5mCwD4fOJ6v" role="2iSdaV" />
        <node concept="3F1sOY" id="5mCwD4fOJ6w" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5mCwD4fOHk$" resolve="wednesday" />
        </node>
      </node>
      <node concept="3EZMnI" id="5mCwD4fOJ8x" role="3EZMnx">
        <node concept="3F0ifn" id="5mCwD4fOJ8y" role="3EZMnx">
          <property role="3F0ifm" value="4" />
        </node>
        <node concept="2iRfu4" id="5mCwD4fOJ8z" role="2iSdaV" />
        <node concept="3F1sOY" id="5mCwD4fOJ8$" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5mCwD4fOHuQ" resolve="thursday" />
        </node>
      </node>
      <node concept="3EZMnI" id="5mCwD4fOJbz" role="3EZMnx">
        <node concept="3F0ifn" id="5mCwD4fOJb$" role="3EZMnx">
          <property role="3F0ifm" value="5" />
        </node>
        <node concept="2iRfu4" id="5mCwD4fOJb_" role="2iSdaV" />
        <node concept="3F1sOY" id="5mCwD4fOJbA" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5mCwD4fOHAf" resolve="friday" />
        </node>
      </node>
      <node concept="35HoNQ" id="5k$jhsKyGok" role="3EZMnx" />
    </node>
  </node>
  <node concept="24kQdi" id="5mCwD4fOqnO">
    <ref role="1XX52x" to="u6v3:5mCwD4fNZLy" resolve="MealPlan" />
    <node concept="3EZMnI" id="7moC6rm5AOM" role="2wV5jI">
      <node concept="2iRkQZ" id="7moC6rm5AON" role="2iSdaV" />
      <node concept="3F2HdR" id="5mCwD4fOqsq" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:5mCwD4fO8YZ" resolve="meal_plans" />
        <node concept="2iRkQZ" id="5mCwD4fOvwg" role="2czzBx" />
      </node>
      <node concept="35HoNQ" id="7moC6rm5B39" role="3EZMnx" />
      <node concept="3EZMnI" id="1jE3pLwXH_x" role="3EZMnx">
        <node concept="2iRkQZ" id="1jE3pLwXH_y" role="2iSdaV" />
        <node concept="3F0ifn" id="1jE3pLuO9RU" role="3EZMnx">
          <property role="3F0ifm" value="Meal Planner" />
          <node concept="VSNWy" id="1jE3pLuOaw1" role="3F10Kt">
            <property role="1lJzqX" value="14" />
          </node>
        </node>
        <node concept="3EZMnI" id="7moC6rm8L9e" role="3EZMnx">
          <node concept="l2Vlx" id="7moC6rm8L9f" role="2iSdaV" />
          <node concept="3F0ifn" id="7moC6rm8L4b" role="3EZMnx">
            <property role="3F0ifm" value="Suggest a week meal plan with the current pantry" />
          </node>
        </node>
        <node concept="3EZMnI" id="1jE3pLuL$r5" role="3EZMnx">
          <node concept="3F0ifn" id="1jE3pLx4oca" role="3EZMnx">
            <property role="3F0ifm" value="for" />
          </node>
          <node concept="3F0A7n" id="1jE3pLx34C_" role="3EZMnx">
            <ref role="1NtTu8" to="u6v3:7moC6rm8Lp4" resolve="number_of_persons" />
          </node>
          <node concept="l2Vlx" id="1jE3pLuL$vf" role="2iSdaV" />
          <node concept="3F0ifn" id="7moC6rm8Le1" role="3EZMnx">
            <property role="3F0ifm" value="daily canteen visitors." />
            <node concept="27z8qx" id="1jE3pLuTEKx" role="3F10Kt">
              <property role="3$6WeP" value="1" />
            </node>
          </node>
        </node>
        <node concept="3gTLQM" id="1jE3pLuGlou" role="3EZMnx">
          <node concept="3Fmcul" id="1jE3pLuGlow" role="3FoqZy">
            <node concept="3clFbS" id="1jE3pLuGloy" role="2VODD2">
              <node concept="3cpWs8" id="1jE3pLv2LSH" role="3cqZAp">
                <node concept="3cpWsn" id="1jE3pLv2LSI" role="3cpWs9">
                  <property role="TrG5h" value="model_access" />
                  <property role="3TUv4t" value="true" />
                  <node concept="3uibUv" id="1jE3pLv2LOx" role="1tU5fm">
                    <ref role="3uigEE" to="lui2:~ModelAccess" resolve="ModelAccess" />
                  </node>
                  <node concept="2OqwBi" id="1jE3pLv2LSJ" role="33vP2m">
                    <node concept="liA8E" id="1jE3pLv2LSN" role="2OqNvi">
                      <ref role="37wK5l" to="lui2:~SRepository.getModelAccess()" resolve="getModelAccess" />
                    </node>
                    <node concept="2OqwBi" id="1jE3pLv34br" role="2Oq$k0">
                      <node concept="1Q80Hx" id="1jE3pLv34bs" role="2Oq$k0" />
                      <node concept="liA8E" id="1jE3pLv34bt" role="2OqNvi">
                        <ref role="37wK5l" to="cj4x:~EditorContext.getRepository()" resolve="getRepository" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="1jE3pLuULP9" role="3cqZAp">
                <node concept="3cpWsn" id="1jE3pLuULPa" role="3cpWs9">
                  <property role="TrG5h" value="button" />
                  <property role="3TUv4t" value="true" />
                  <node concept="3uibUv" id="1jE3pLuULjI" role="1tU5fm">
                    <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
                  </node>
                  <node concept="2ShNRf" id="1jE3pLuULPb" role="33vP2m">
                    <node concept="1pGfFk" id="1jE3pLuULPc" role="2ShVmc">
                      <property role="373rjd" value="true" />
                      <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                      <node concept="Xl_RD" id="1jE3pLuULPd" role="37wK5m">
                        <property role="Xl_RC" value="Create suggestion" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="1jE3pLv4bup" role="3cqZAp" />
              <node concept="L3pyB" id="1jE3pLv43me" role="3cqZAp">
                <node concept="3clFbS" id="1jE3pLv43mg" role="L3pyw">
                  <node concept="3cpWs8" id="1jE3pLv4g$N" role="3cqZAp">
                    <node concept="3cpWsn" id="1jE3pLv4g$O" role="3cpWs9">
                      <property role="TrG5h" value="recipes" />
                      <node concept="A3Dl8" id="1jE3pLv4jMt" role="1tU5fm">
                        <node concept="3Tqbb2" id="1jE3pLv4jMv" role="A3Ik2">
                          <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                        </node>
                      </node>
                      <node concept="qVDSY" id="1jE3pLv4g$P" role="33vP2m">
                        <node concept="chp4Y" id="1jE3pLv4g$Q" role="qVDSX">
                          <ref role="cht4Q" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3cpWs8" id="1jE3pLv4oZg" role="3cqZAp">
                    <node concept="3cpWsn" id="1jE3pLv4oZj" role="3cpWs9">
                      <property role="TrG5h" value="pantry" />
                      <node concept="3Tqbb2" id="1jE3pLv4oZe" role="1tU5fm">
                        <ref role="ehGHo" to="u6v3:7moC6rm27Me" resolve="Pantry" />
                      </node>
                      <node concept="2OqwBi" id="1jE3pLv4q_P" role="33vP2m">
                        <node concept="qVDSY" id="1jE3pLv4pX6" role="2Oq$k0">
                          <node concept="chp4Y" id="1jE3pLv4q5d" role="qVDSX">
                            <ref role="cht4Q" to="u6v3:7moC6rm27Me" resolve="Pantry" />
                          </node>
                        </node>
                        <node concept="1uHKPH" id="1jE3pLv4r1B" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="1jE3pLveug9" role="3cqZAp" />
                  <node concept="3clFbF" id="1jE3pLuGAx5" role="3cqZAp">
                    <node concept="2OqwBi" id="1jE3pLuUMKr" role="3clFbG">
                      <node concept="37vLTw" id="1jE3pLuULPe" role="2Oq$k0">
                        <ref role="3cqZAo" node="1jE3pLuULPa" resolve="button" />
                      </node>
                      <node concept="liA8E" id="1jE3pLuUOXv" role="2OqNvi">
                        <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener)" resolve="addActionListener" />
                        <node concept="1bVj0M" id="1jE3pLuUTka" role="37wK5m">
                          <node concept="3clFbS" id="1jE3pLuUTkb" role="1bW5cS">
                            <node concept="3clFbF" id="1jE3pLv2MDN" role="3cqZAp">
                              <node concept="2OqwBi" id="1jE3pLv3$jw" role="3clFbG">
                                <node concept="37vLTw" id="1jE3pLv2MDL" role="2Oq$k0">
                                  <ref role="3cqZAo" node="1jE3pLv2LSI" resolve="model_access" />
                                </node>
                                <node concept="liA8E" id="1jE3pLv3$In" role="2OqNvi">
                                  <ref role="37wK5l" to="lui2:~ModelAccess.executeCommand(java.lang.Runnable)" resolve="executeCommand" />
                                  <node concept="1bVj0M" id="1jE3pLv3$OM" role="37wK5m">
                                    <node concept="3clFbS" id="1jE3pLv3$ON" role="1bW5cS">
                                      <node concept="2xdQw9" id="1jE3pLv9yvx" role="3cqZAp">
                                        <property role="2xdLsb" value="gZ5fh_4/error" />
                                        <node concept="3cpWs3" id="1jE3pLv9zK3" role="9lYJi">
                                          <node concept="2OqwBi" id="1jE3pLvaLFz" role="3uHU7w">
                                            <node concept="37vLTw" id="1jE3pLvaLeH" role="2Oq$k0">
                                              <ref role="3cqZAo" node="1jE3pLv4g$O" resolve="recipes" />
                                            </node>
                                            <node concept="34oBXx" id="1jE3pLvaMad" role="2OqNvi" />
                                          </node>
                                          <node concept="Xl_RD" id="1jE3pLv9yvz" role="3uHU7B">
                                            <property role="Xl_RC" value="Recipes: " />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2xdQw9" id="1jE3pLvaJsE" role="3cqZAp">
                                        <property role="2xdLsb" value="gZ5fh_4/error" />
                                        <node concept="3cpWs3" id="1jE3pLvaJsF" role="9lYJi">
                                          <node concept="2OqwBi" id="1jE3pLvaJsG" role="3uHU7w">
                                            <node concept="37vLTw" id="1jE3pLvaJsH" role="2Oq$k0">
                                              <ref role="3cqZAo" node="1jE3pLv4oZj" resolve="pantry" />
                                            </node>
                                            <node concept="2qgKlT" id="1jE3pLvaJsI" role="2OqNvi">
                                              <ref role="37wK5l" to="tpcu:22G2W3WJ92t" resolve="getDetailedPresentation" />
                                            </node>
                                          </node>
                                          <node concept="Xl_RD" id="1jE3pLvaJsJ" role="3uHU7B">
                                            <property role="Xl_RC" value="Pantry: " />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="3cpWs8" id="1jE3pLuUUEH" role="3cqZAp">
                                        <node concept="3cpWsn" id="1jE3pLuUUEK" role="3cpWs9">
                                          <property role="TrG5h" value="suggested_meal_plan" />
                                          <node concept="3Tqbb2" id="1jE3pLuUUEF" role="1tU5fm">
                                            <ref role="ehGHo" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
                                          </node>
                                          <node concept="10Nm6u" id="1jE3pLvjggx" role="33vP2m" />
                                        </node>
                                      </node>
                                      <node concept="3J1_TO" id="1jE3pLvaNI1" role="3cqZAp">
                                        <node concept="3uVAMA" id="1jE3pLvaNJH" role="1zxBo5">
                                          <node concept="XOnhg" id="1jE3pLvaNJI" role="1zc67B">
                                            <property role="TrG5h" value="e" />
                                            <node concept="nSUau" id="1jE3pLvaNJJ" role="1tU5fm">
                                              <node concept="3uibUv" id="1jE3pLvaNYd" role="nSUat">
                                                <ref role="3uigEE" to="wyt6:~Exception" resolve="Exception" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="3clFbS" id="1jE3pLvaNJK" role="1zc67A">
                                            <node concept="2xdQw9" id="1jE3pLvaOec" role="3cqZAp">
                                              <property role="2xdLsb" value="gZ5fh_4/error" />
                                              <node concept="Xl_RD" id="1jE3pLvaOee" role="9lYJi">
                                                <property role="Xl_RC" value="Failed suggestion" />
                                              </node>
                                              <node concept="37vLTw" id="1jE3pLvaP5a" role="9lYJj">
                                                <ref role="3cqZAo" node="1jE3pLvaNJI" resolve="e" />
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="3clFbS" id="1jE3pLvaNI3" role="1zxBo7">
                                          <node concept="1X3_iC" id="1jE3pLwt89r" role="lGtFl">
                                            <property role="3V$3am" value="statement" />
                                            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                                            <node concept="3cpWs8" id="1jE3pLvuiZf" role="8Wnug">
                                              <node concept="3cpWsn" id="1jE3pLvuiZg" role="3cpWs9">
                                                <property role="TrG5h" value="combinations" />
                                                <node concept="A3Dl8" id="1jE3pLvuhT$" role="1tU5fm">
                                                  <node concept="A3Dl8" id="1jE3pLvuhTF" role="A3Ik2">
                                                    <node concept="3Tqbb2" id="1jE3pLvuhTG" role="A3Ik2">
                                                      <ref role="ehGHo" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
                                                    </node>
                                                  </node>
                                                </node>
                                                <node concept="2OqwBi" id="1jE3pLvuiZh" role="33vP2m">
                                                  <node concept="35c_gC" id="1jE3pLvuiZi" role="2Oq$k0">
                                                    <ref role="35c_gD" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
                                                  </node>
                                                  <node concept="2qgKlT" id="1jE3pLvuiZj" role="2OqNvi">
                                                    <ref role="37wK5l" to="xqb1:1jE3pLu_zgp" resolve="combinations" />
                                                    <node concept="3cmrfG" id="1jE3pLvuiZk" role="37wK5m">
                                                      <property role="3cmrfH" value="5" />
                                                    </node>
                                                    <node concept="37vLTw" id="1jE3pLvuiZl" role="37wK5m">
                                                      <ref role="3cqZAo" node="1jE3pLv4g$O" resolve="recipes" />
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="1X3_iC" id="1jE3pLwt8tz" role="lGtFl">
                                            <property role="3V$3am" value="statement" />
                                            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                                            <node concept="2xdQw9" id="1jE3pLvujIB" role="8Wnug">
                                              <property role="2xdLsb" value="gZ5fh_4/error" />
                                              <node concept="3cpWs3" id="1jE3pLvuloT" role="9lYJi">
                                                <node concept="2OqwBi" id="1jE3pLvum$0" role="3uHU7w">
                                                  <node concept="37vLTw" id="1jE3pLvulIE" role="2Oq$k0">
                                                    <ref role="3cqZAo" node="1jE3pLvuiZg" resolve="combinations" />
                                                  </node>
                                                  <node concept="34oBXx" id="1jE3pLvunbQ" role="2OqNvi" />
                                                </node>
                                                <node concept="Xl_RD" id="1jE3pLvujID" role="3uHU7B">
                                                  <property role="Xl_RC" value="Combinations:" />
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="1X3_iC" id="1jE3pLwohCq" role="lGtFl">
                                            <property role="3V$3am" value="statement" />
                                            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
                                            <node concept="2Gpval" id="1jE3pLvpXtc" role="8Wnug">
                                              <node concept="2GrKxI" id="1jE3pLvpXte" role="2Gsz3X">
                                                <property role="TrG5h" value="c" />
                                              </node>
                                              <node concept="3clFbS" id="1jE3pLvpXti" role="2LFqv$">
                                                <node concept="2xdQw9" id="1jE3pLvrsvj" role="3cqZAp">
                                                  <property role="2xdLsb" value="gZ5fh_4/error" />
                                                  <node concept="2OqwBi" id="1jE3pLvrsvl" role="9lYJi">
                                                    <node concept="2OqwBi" id="1jE3pLvrsvm" role="2Oq$k0">
                                                      <node concept="2GrUjf" id="1jE3pLvrsvn" role="2Oq$k0">
                                                        <ref role="2Gs0qQ" node="1jE3pLvpXte" resolve="c" />
                                                      </node>
                                                      <node concept="3$u5V9" id="1jE3pLvrsvo" role="2OqNvi">
                                                        <node concept="1bVj0M" id="1jE3pLvrsvp" role="23t8la">
                                                          <node concept="3clFbS" id="1jE3pLvrsvq" role="1bW5cS">
                                                            <node concept="3clFbF" id="1jE3pLvrsvr" role="3cqZAp">
                                                              <node concept="2OqwBi" id="1jE3pLvrsvs" role="3clFbG">
                                                                <node concept="37vLTw" id="1jE3pLvrsvt" role="2Oq$k0">
                                                                  <ref role="3cqZAo" node="1jE3pLvrsvv" resolve="it" />
                                                                </node>
                                                                <node concept="3TrcHB" id="1jE3pLvrsvu" role="2OqNvi">
                                                                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                                                </node>
                                                              </node>
                                                            </node>
                                                          </node>
                                                          <node concept="Rh6nW" id="1jE3pLvrsvv" role="1bW2Oz">
                                                            <property role="TrG5h" value="it" />
                                                            <node concept="2jxLKc" id="1jE3pLvrsvw" role="1tU5fm" />
                                                          </node>
                                                        </node>
                                                      </node>
                                                    </node>
                                                    <node concept="3uJxvA" id="1jE3pLvrsvx" role="2OqNvi">
                                                      <node concept="Xl_RD" id="1jE3pLvrsvy" role="3uJOhx">
                                                        <property role="Xl_RC" value="," />
                                                      </node>
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                              <node concept="37vLTw" id="1jE3pLvuiZm" role="2GsD0m">
                                                <ref role="3cqZAo" node="1jE3pLvuiZg" resolve="combinations" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="3clFbF" id="1jE3pLv6QGx" role="3cqZAp">
                                            <node concept="37vLTI" id="1jE3pLv6R0P" role="3clFbG">
                                              <node concept="37vLTw" id="1jE3pLv6QGv" role="37vLTJ">
                                                <ref role="3cqZAo" node="1jE3pLuUUEK" resolve="suggested_meal_plan" />
                                              </node>
                                              <node concept="2OqwBi" id="1jE3pLv3RGh" role="37vLTx">
                                                <node concept="35c_gC" id="1jE3pLv3RGi" role="2Oq$k0">
                                                  <ref role="35c_gD" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
                                                </node>
                                                <node concept="2qgKlT" id="1jE3pLv3RGj" role="2OqNvi">
                                                  <ref role="37wK5l" to="xqb1:1jE3pLu_6sr" resolve="suggest_week_meal_plan" />
                                                  <node concept="37vLTw" id="1jE3pLv4lNK" role="37wK5m">
                                                    <ref role="3cqZAo" node="1jE3pLv4g$O" resolve="recipes" />
                                                  </node>
                                                  <node concept="3cmrfG" id="1jE3pLv4mwm" role="37wK5m">
                                                    <property role="3cmrfH" value="5" />
                                                  </node>
                                                  <node concept="37vLTw" id="1jE3pLv4r9l" role="37wK5m">
                                                    <ref role="3cqZAo" node="1jE3pLv4oZj" resolve="pantry" />
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="2xdQw9" id="1jE3pLvc1nJ" role="3cqZAp">
                                        <property role="2xdLsb" value="gZ5fh_4/error" />
                                        <node concept="3cpWs3" id="1jE3pLvc2EP" role="9lYJi">
                                          <node concept="2OqwBi" id="1jE3pLvc3bY" role="3uHU7w">
                                            <node concept="37vLTw" id="1jE3pLvc2Sv" role="2Oq$k0">
                                              <ref role="3cqZAo" node="1jE3pLuUUEK" resolve="suggested_meal_plan" />
                                            </node>
                                            <node concept="2qgKlT" id="1jE3pLvc3MG" role="2OqNvi">
                                              <ref role="37wK5l" to="tpcu:22G2W3WJ92t" resolve="getDetailedPresentation" />
                                            </node>
                                          </node>
                                          <node concept="Xl_RD" id="1jE3pLvc1nL" role="3uHU7B">
                                            <property role="Xl_RC" value="Suggestion:" />
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="3clFbJ" id="1jE3pLvdezk" role="3cqZAp">
                                        <node concept="3clFbS" id="1jE3pLvdezm" role="3clFbx">
                                          <node concept="2xdQw9" id="1jE3pLwxjtc" role="3cqZAp">
                                            <property role="2xdLsb" value="gZ5fksE/warn" />
                                            <node concept="Xl_RD" id="1jE3pLwxjte" role="9lYJi">
                                              <property role="Xl_RC" value="No meal plan found for the current stock" />
                                            </node>
                                          </node>
                                          <node concept="3clFbF" id="1jE3pLvdg4e" role="3cqZAp">
                                            <node concept="37vLTI" id="1jE3pLvdgk4" role="3clFbG">
                                              <node concept="37vLTw" id="1jE3pLvdg4c" role="37vLTJ">
                                                <ref role="3cqZAo" node="1jE3pLuUUEK" resolve="suggested_meal_plan" />
                                              </node>
                                              <node concept="2pJPEk" id="1jE3pLuWQUf" role="37vLTx">
                                                <node concept="2pJPED" id="1jE3pLuWQUh" role="2pJPEn">
                                                  <ref role="2pJxaS" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
                                                  <node concept="2pIpSj" id="1jE3pLuWX5k" role="2pJxcM">
                                                    <ref role="2pIpSl" to="u6v3:5mCwD4fOH8F" resolve="monday" />
                                                    <node concept="36biLy" id="1jE3pLuWXhb" role="28nt2d">
                                                      <node concept="10Nm6u" id="1jE3pLuWXh9" role="36biLW" />
                                                    </node>
                                                  </node>
                                                  <node concept="2pIpSj" id="1jE3pLuWXum" role="2pJxcM">
                                                    <ref role="2pIpSl" to="u6v3:5mCwD4fOHfe" resolve="tuesday" />
                                                    <node concept="36biLy" id="1jE3pLuWXEK" role="28nt2d">
                                                      <node concept="10Nm6u" id="1jE3pLuWXQD" role="36biLW" />
                                                    </node>
                                                  </node>
                                                  <node concept="2pIpSj" id="1jE3pLuWY33" role="2pJxcM">
                                                    <ref role="2pIpSl" to="u6v3:5mCwD4fOHk$" resolve="wednesday" />
                                                    <node concept="36biLy" id="1jE3pLuWYeG" role="28nt2d">
                                                      <node concept="10Nm6u" id="1jE3pLuWYqy" role="36biLW" />
                                                    </node>
                                                  </node>
                                                  <node concept="2pIpSj" id="1jE3pLuWYAZ" role="2pJxcM">
                                                    <ref role="2pIpSl" to="u6v3:5mCwD4fOHuQ" resolve="thursday" />
                                                    <node concept="36biLy" id="1jE3pLuWYN5" role="28nt2d">
                                                      <node concept="10Nm6u" id="1jE3pLuWYYV" role="36biLW" />
                                                    </node>
                                                  </node>
                                                  <node concept="2pIpSj" id="1jE3pLuWZbr" role="2pJxcM">
                                                    <ref role="2pIpSl" to="u6v3:5mCwD4fOHAf" resolve="friday" />
                                                    <node concept="36biLy" id="1jE3pLuWZns" role="28nt2d">
                                                      <node concept="10Nm6u" id="1jE3pLuWZzG" role="36biLW" />
                                                    </node>
                                                  </node>
                                                </node>
                                              </node>
                                            </node>
                                          </node>
                                        </node>
                                        <node concept="2OqwBi" id="1jE3pLvdf9e" role="3clFbw">
                                          <node concept="37vLTw" id="1jE3pLvdeMw" role="2Oq$k0">
                                            <ref role="3cqZAo" node="1jE3pLuUUEK" resolve="suggested_meal_plan" />
                                          </node>
                                          <node concept="3w_OXm" id="1jE3pLvdfI3" role="2OqNvi" />
                                        </node>
                                      </node>
                                      <node concept="3clFbF" id="1jE3pLvlRAa" role="3cqZAp">
                                        <node concept="37vLTI" id="1jE3pLvlTop" role="3clFbG">
                                          <node concept="2OqwBi" id="1jE3pLvlXyM" role="37vLTx">
                                            <node concept="2OqwBi" id="1jE3pLvlUYt" role="2Oq$k0">
                                              <node concept="2OqwBi" id="1jE3pLvlUg9" role="2Oq$k0">
                                                <node concept="pncrf" id="1jE3pLvlU1u" role="2Oq$k0" />
                                                <node concept="3Tsc0h" id="1jE3pLvlUuZ" role="2OqNvi">
                                                  <ref role="3TtcxE" to="u6v3:5mCwD4fO8YZ" resolve="meal_plans" />
                                                </node>
                                              </node>
                                              <node concept="1yVyf7" id="1jE3pLvlXj_" role="2OqNvi" />
                                            </node>
                                            <node concept="3TrcHB" id="1jE3pLvlXT3" role="2OqNvi">
                                              <ref role="3TsBF5" to="u6v3:5mCwD4fO8zh" resolve="year" />
                                            </node>
                                          </node>
                                          <node concept="2OqwBi" id="1jE3pLvlRUY" role="37vLTJ">
                                            <node concept="37vLTw" id="1jE3pLvlRA8" role="2Oq$k0">
                                              <ref role="3cqZAo" node="1jE3pLuUUEK" resolve="suggested_meal_plan" />
                                            </node>
                                            <node concept="3TrcHB" id="1jE3pLvlSl0" role="2OqNvi">
                                              <ref role="3TsBF5" to="u6v3:5mCwD4fO8zh" resolve="year" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="3clFbF" id="1jE3pLvlY86" role="3cqZAp">
                                        <node concept="37vLTI" id="1jE3pLvlY87" role="3clFbG">
                                          <node concept="3cpWs3" id="1jE3pLvom3V" role="37vLTx">
                                            <node concept="3cmrfG" id="1jE3pLvomyT" role="3uHU7w">
                                              <property role="3cmrfH" value="1" />
                                            </node>
                                            <node concept="2OqwBi" id="1jE3pLvlY88" role="3uHU7B">
                                              <node concept="2OqwBi" id="1jE3pLvlY89" role="2Oq$k0">
                                                <node concept="2OqwBi" id="1jE3pLvlY8a" role="2Oq$k0">
                                                  <node concept="pncrf" id="1jE3pLvlY8b" role="2Oq$k0" />
                                                  <node concept="3Tsc0h" id="1jE3pLvlY8c" role="2OqNvi">
                                                    <ref role="3TtcxE" to="u6v3:5mCwD4fO8YZ" resolve="meal_plans" />
                                                  </node>
                                                </node>
                                                <node concept="1yVyf7" id="1jE3pLvlY8d" role="2OqNvi" />
                                              </node>
                                              <node concept="3TrcHB" id="1jE3pLvlY8e" role="2OqNvi">
                                                <ref role="3TsBF5" to="u6v3:5mCwD4fO8Ed" resolve="calendar_week" />
                                              </node>
                                            </node>
                                          </node>
                                          <node concept="2OqwBi" id="1jE3pLvlY8f" role="37vLTJ">
                                            <node concept="37vLTw" id="1jE3pLvlY8g" role="2Oq$k0">
                                              <ref role="3cqZAo" node="1jE3pLuUUEK" resolve="suggested_meal_plan" />
                                            </node>
                                            <node concept="3TrcHB" id="1jE3pLvlY8h" role="2OqNvi">
                                              <ref role="3TsBF5" to="u6v3:5mCwD4fO8Ed" resolve="calendar_week" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                      <node concept="3clFbF" id="1jE3pLuWR6$" role="3cqZAp">
                                        <node concept="2OqwBi" id="1jE3pLuWThB" role="3clFbG">
                                          <node concept="2OqwBi" id="1jE3pLuWRnS" role="2Oq$k0">
                                            <node concept="pncrf" id="1jE3pLuWR6z" role="2Oq$k0" />
                                            <node concept="3Tsc0h" id="1jE3pLuWR_c" role="2OqNvi">
                                              <ref role="3TtcxE" to="u6v3:5mCwD4fO8YZ" resolve="meal_plans" />
                                            </node>
                                          </node>
                                          <node concept="TSZUe" id="1jE3pLuWUXG" role="2OqNvi">
                                            <node concept="37vLTw" id="1jE3pLuWWv2" role="25WWJ7">
                                              <ref role="3cqZAo" node="1jE3pLuUUEK" resolve="suggested_meal_plan" />
                                            </node>
                                          </node>
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTG" id="1jE3pLuUT$J" role="1bW2Oz">
                            <property role="TrG5h" value="e" />
                            <node concept="3uibUv" id="1jE3pLuUT$I" role="1tU5fm">
                              <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="1jE3pLv4eNx" role="L3pyr">
                  <node concept="pncrf" id="1jE3pLv4eaH" role="2Oq$k0" />
                  <node concept="I4A8Y" id="1jE3pLv4feb" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbH" id="1jE3pLv5yL3" role="3cqZAp" />
              <node concept="3clFbF" id="1jE3pLuUQLw" role="3cqZAp">
                <node concept="37vLTw" id="1jE3pLuUQLu" role="3clFbG">
                  <ref role="3cqZAo" node="1jE3pLuULPa" resolve="button" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="VPXOz" id="1jE3pLwXIR6" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5mCwD4fOt9m">
    <ref role="1XX52x" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
    <node concept="3F2HdR" id="5mCwD4fOtf8" role="2wV5jI">
      <ref role="1NtTu8" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
      <node concept="l2Vlx" id="8EeBQEryRy" role="2czzBx" />
      <node concept="2o9xnK" id="8EeBQEprvv" role="2gpyvW">
        <node concept="3clFbS" id="8EeBQEprvw" role="2VODD2">
          <node concept="3clFbF" id="8EeBQEpsOZ" role="3cqZAp">
            <node concept="Xl_RD" id="8EeBQEpsOY" role="3clFbG">
              <property role="Xl_RC" value="," />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5mCwD4fO$fN">
    <ref role="1XX52x" to="u6v3:5mCwD4fO2$5" resolve="Meal" />
    <node concept="3EZMnI" id="5mCwD4fO$gD" role="2wV5jI">
      <node concept="35HoNQ" id="5k$jhsKyUHg" role="3EZMnx">
        <node concept="11LMrY" id="5k$jhsKyUMB" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="5mCwD4fO$iL" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
        <node concept="1sVBvm" id="5mCwD4fO$iN" role="1sWHZn">
          <node concept="3F0A7n" id="5mCwD4fO$kW" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="1HlG4h" id="8EeBQEtDkL" role="3EZMnx">
        <node concept="1HfYo3" id="8EeBQEtDkN" role="1HlULh">
          <node concept="3TQlhw" id="8EeBQEtDkP" role="1Hhtcw">
            <node concept="3clFbS" id="8EeBQEtDkR" role="2VODD2">
              <node concept="3cpWs6" id="8EeBQEtSAK" role="3cqZAp">
                <node concept="2OqwBi" id="8EeBQEuocV" role="3cqZAk">
                  <node concept="2OqwBi" id="8EeBQEunPi" role="2Oq$k0">
                    <node concept="pncrf" id="8EeBQEunCP" role="2Oq$k0" />
                    <node concept="3TrEf2" id="8EeBQEuo4x" role="2OqNvi">
                      <ref role="3Tt5mk" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                    </node>
                  </node>
                  <node concept="2qgKlT" id="8EeBQEuoLL" role="2OqNvi">
                    <ref role="37wK5l" to="xqb1:8EeBQEukI3" resolve="diet_emoji" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="11L4FC" id="8EeBQEu7$W" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VPM3Z" id="1jE3pLxvalk" role="3F10Kt" />
      </node>
      <node concept="l2Vlx" id="5mCwD4fO$gF" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5mCwD4fP8Si">
    <ref role="1XX52x" to="u6v3:6dt6wfGrvip" resolve="MeasuredIngedient" />
    <node concept="3EZMnI" id="5mCwD4fP8UK" role="2wV5jI">
      <node concept="3F0A7n" id="5mCwD4fP92d" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:6dt6wfGrviq" resolve="amount" />
      </node>
      <node concept="1iCGBv" id="5boToVMUb1D" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <ref role="1NtTu8" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
        <node concept="1sVBvm" id="5boToVMUb1F" role="1sWHZn">
          <node concept="1iCGBv" id="5boToVMUb56" role="2wV5jI">
            <ref role="1NtTu8" to="u6v3:5boToVMSz92" resolve="unit" />
            <node concept="1sVBvm" id="5boToVMUb58" role="1sWHZn">
              <node concept="3F0A7n" id="5boToVMUb8v" role="2wV5jI">
                <property role="1Intyy" value="true" />
                <ref role="1NtTu8" to="u6v3:5boToVMJTj6" resolve="symbol" />
              </node>
            </node>
          </node>
        </node>
        <node concept="11L4FC" id="5boToVMZZbR" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="1iCGBv" id="5mCwD4fP9bO" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
        <node concept="1sVBvm" id="5mCwD4fP9bQ" role="1sWHZn">
          <node concept="3F0A7n" id="5mCwD4fP9fg" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="2iRfu4" id="5mCwD4fP8ZI" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5mCwD4fP9n2">
    <ref role="1XX52x" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
    <node concept="3EZMnI" id="5mCwD4fP9ry" role="2wV5jI">
      <node concept="3EZMnI" id="5mCwD4fPsw7" role="3EZMnx">
        <node concept="VPM3Z" id="5mCwD4fPsw9" role="3F10Kt" />
        <node concept="VSNWy" id="8EeBQEsAtS" role="3F10Kt">
          <property role="1lJzqX" value="14" />
        </node>
        <node concept="3F0ifn" id="5mCwD4fPswb" role="3EZMnx">
          <property role="3F0ifm" value="Recipe for" />
        </node>
        <node concept="3F0A7n" id="5mCwD4fPsEW" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="1HlG4h" id="8EeBQEupgw" role="3EZMnx">
          <node concept="1HfYo3" id="8EeBQEupgy" role="1HlULh">
            <node concept="3TQlhw" id="8EeBQEupg$" role="1Hhtcw">
              <node concept="3clFbS" id="8EeBQEupgA" role="2VODD2">
                <node concept="3clFbF" id="8EeBQEupiy" role="3cqZAp">
                  <node concept="2OqwBi" id="8EeBQEupxn" role="3clFbG">
                    <node concept="pncrf" id="8EeBQEupix" role="2Oq$k0" />
                    <node concept="2qgKlT" id="8EeBQEupZL" role="2OqNvi">
                      <ref role="37wK5l" to="xqb1:8EeBQEukI3" resolve="diet_emoji" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="l2Vlx" id="5mCwD4fPswc" role="2iSdaV" />
      </node>
      <node concept="3F0A7n" id="8EeBQEssP0" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:6dt6wfGrviA" resolve="instructions" />
      </node>
      <node concept="35HoNQ" id="8EeBQEsK6i" role="3EZMnx" />
      <node concept="3F0ifn" id="8EeBQEsK78" role="3EZMnx">
        <property role="3F0ifm" value="Ingredients:" />
      </node>
      <node concept="3F2HdR" id="5mCwD4fP9Hx" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:6dt6wfGrvix" resolve="ingredients" />
        <node concept="2iRkQZ" id="5mCwD4fP9Hz" role="2czzBx" />
      </node>
      <node concept="2iRkQZ" id="5mCwD4fP9uu" role="2iSdaV" />
      <node concept="35HoNQ" id="8EeBQEuNNN" role="3EZMnx" />
      <node concept="3EZMnI" id="8EeBQEuOhd" role="3EZMnx">
        <node concept="VPM3Z" id="8EeBQEuOhf" role="3F10Kt" />
        <node concept="3F0ifn" id="8EeBQEuOBk" role="3EZMnx">
          <property role="3F0ifm" value="Total calories:" />
        </node>
        <node concept="1HlG4h" id="8EeBQEuOFS" role="3EZMnx">
          <node concept="1HfYo3" id="8EeBQEuOFU" role="1HlULh">
            <node concept="3TQlhw" id="8EeBQEuOFW" role="1Hhtcw">
              <node concept="3clFbS" id="8EeBQEuOFY" role="2VODD2">
                <node concept="3clFbF" id="8EeBQEuONF" role="3cqZAp">
                  <node concept="3cpWs3" id="8EeBQEuVaN" role="3clFbG">
                    <node concept="Xl_RD" id="8EeBQEuVvT" role="3uHU7w">
                      <property role="Xl_RC" value="kJ" />
                    </node>
                    <node concept="2OqwBi" id="8EeBQEuP2w" role="3uHU7B">
                      <node concept="pncrf" id="8EeBQEuONE" role="2Oq$k0" />
                      <node concept="2qgKlT" id="8EeBQEuPwe" role="2OqNvi">
                        <ref role="37wK5l" to="xqb1:8EeBQEuDge" resolve="calories" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2iRfu4" id="8EeBQEuOhi" role="2iSdaV" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="8EeBQEp_dr">
    <ref role="1XX52x" to="u6v3:6dt6wfGrpXm" resolve="Ingredient" />
    <node concept="3EZMnI" id="8EeBQEp_eh" role="2wV5jI">
      <node concept="3F0A7n" id="8EeBQEp_ir" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="8EeBQEpIJZ" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <node concept="11LMrY" id="8EeBQEpIVE" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VPM3Z" id="8EeBQEq9Ey" role="3F10Kt" />
      </node>
      <node concept="3F0ifn" id="5boToVMSzIS" role="3EZMnx">
        <property role="3F0ifm" value="Unit:" />
      </node>
      <node concept="1iCGBv" id="5boToVMSzMo" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:5boToVMSz92" resolve="unit" />
        <node concept="1sVBvm" id="5boToVMSzMq" role="1sWHZn">
          <node concept="3F0A7n" id="5boToVMSzQS" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3EZMnI" id="5boToVN28Sj" role="3EZMnx">
        <node concept="l2Vlx" id="5boToVN28Sk" role="2iSdaV" />
        <node concept="3F0ifn" id="5boToVN288x" role="3EZMnx">
          <property role="3F0ifm" value="with 1" />
          <node concept="11LMrY" id="5boToVN3_WM" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="1iCGBv" id="5boToVN28gz" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5boToVMSz92" resolve="unit" />
          <node concept="1sVBvm" id="5boToVN28g_" role="1sWHZn">
            <node concept="3F0A7n" id="5boToVN28iF" role="2wV5jI">
              <property role="1Intyy" value="true" />
              <ref role="1NtTu8" to="u6v3:5boToVMJTj6" resolve="symbol" />
            </node>
          </node>
        </node>
        <node concept="3F0A7n" id="5boToVN4Yjv" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="3F0ifn" id="5boToVN3A3Q" role="3EZMnx">
          <property role="3F0ifm" value="=" />
        </node>
        <node concept="3F0A7n" id="5boToVN3AxM" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5boToVN275x" resolve="to_gram_dividend" />
        </node>
        <node concept="3F0ifn" id="5boToVN3AG9" role="3EZMnx">
          <property role="3F0ifm" value="/" />
          <node concept="11L4FC" id="5boToVN4YxV" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="11LMrY" id="5boToVN4YzC" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0A7n" id="5boToVN3AMX" role="3EZMnx">
          <ref role="1NtTu8" to="u6v3:5boToVN27le" resolve="to_gram_divisor" />
        </node>
        <node concept="3F0ifn" id="5boToVN3AQX" role="3EZMnx">
          <property role="3F0ifm" value="g" />
          <node concept="11L4FC" id="5boToVN6sKW" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="pkWqt" id="5boToVN292X" role="pqm2j">
          <node concept="3clFbS" id="5boToVN292Y" role="2VODD2">
            <node concept="3cpWs8" id="5boToVN2iiK" role="3cqZAp">
              <node concept="3cpWsn" id="5boToVN2iiL" role="3cpWs9">
                <property role="TrG5h" value="repository" />
                <node concept="3uibUv" id="5boToVN2iiH" role="1tU5fm">
                  <ref role="3uigEE" to="lui2:~SRepository" resolve="SRepository" />
                </node>
                <node concept="2OqwBi" id="5boToVN2iiM" role="33vP2m">
                  <node concept="1Q80Hx" id="5boToVN2iiN" role="2Oq$k0" />
                  <node concept="liA8E" id="5boToVN2iiO" role="2OqNvi">
                    <ref role="37wK5l" to="cj4x:~EditorContext.getRepository()" resolve="getRepository" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="5boToVN298p" role="3cqZAp">
              <node concept="17QLQc" id="5boToVN2ady" role="3clFbG">
                <node concept="2OqwBi" id="5boToVN29nK" role="3uHU7B">
                  <node concept="pncrf" id="5boToVN298o" role="2Oq$k0" />
                  <node concept="3TrEf2" id="5boToVN29AZ" role="2OqNvi">
                    <ref role="3Tt5mk" to="u6v3:5boToVMSz92" resolve="unit" />
                  </node>
                </node>
                <node concept="2OqwBi" id="5boToVN2j3y" role="3uHU7w">
                  <node concept="2tJFMh" id="5boToVN2iue" role="2Oq$k0">
                    <node concept="ZC_QK" id="5boToVN2iH1" role="2tJFKM">
                      <ref role="2aWVGs" to="cdrp:5boToVMQyDe" resolve="Gram" />
                    </node>
                  </node>
                  <node concept="Vyspw" id="5boToVN2joA" role="2OqNvi">
                    <node concept="37vLTw" id="5boToVN2jxo" role="Vysub">
                      <ref role="3cqZAo" node="5boToVN2iiL" resolve="repository" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="8EeBQErZRi" role="3EZMnx">
        <property role="3F0ifm" value=", Diet:" />
        <node concept="11L4FC" id="8EeBQEs0fp" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VPM3Z" id="8EeBQEs0ht" role="3F10Kt" />
      </node>
      <node concept="3F0A7n" id="8EeBQEp_qn" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:6dt6wfGrvi5" resolve="diet" />
      </node>
      <node concept="3F0ifn" id="8EeBQEs01v" role="3EZMnx">
        <property role="3F0ifm" value=", Calories:" />
        <node concept="11L4FC" id="8EeBQEs09f" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VPM3Z" id="8EeBQEs0bH" role="3F10Kt" />
      </node>
      <node concept="3F0A7n" id="8EeBQEp_np" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:6dt6wfGrvi8" resolve="calories" />
      </node>
      <node concept="3F0ifn" id="8EeBQEpRSo" role="3EZMnx">
        <property role="3F0ifm" value="kJ/100g" />
        <node concept="11L4FC" id="8EeBQEq0Ny" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VPM3Z" id="8EeBQEq9An" role="3F10Kt" />
      </node>
      <node concept="3F0ifn" id="8EeBQErZJR" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <node concept="11L4FC" id="8EeBQErZLx" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VPM3Z" id="8EeBQErZNZ" role="3F10Kt" />
      </node>
      <node concept="l2Vlx" id="8EeBQEp_ej" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7moC6rm28uG">
    <ref role="1XX52x" to="u6v3:7moC6rm27Me" resolve="Pantry" />
    <node concept="3EZMnI" id="7moC6rm3T$6" role="2wV5jI">
      <node concept="3F0ifn" id="7moC6rm3T_s" role="3EZMnx">
        <property role="3F0ifm" value="Currently available staples" />
        <node concept="VSNWy" id="7moC6rm4Mpk" role="3F10Kt">
          <property role="1lJzqX" value="14" />
        </node>
      </node>
      <node concept="2iRkQZ" id="7moC6rm3T$7" role="2iSdaV" />
      <node concept="3F2HdR" id="7moC6rm28wm" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:7moC6rm27TX" resolve="staples" />
        <node concept="2iRkQZ" id="7moC6rm2Vd7" role="2czzBx" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5boToVMGofd">
    <ref role="1XX52x" to="u6v3:5boToVMCwmF" resolve="Unit" />
    <node concept="3EZMnI" id="5boToVMQySl" role="2wV5jI">
      <node concept="3F0A7n" id="5boToVMQz0A" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5boToVMQz3a" role="3EZMnx">
        <property role="3F0ifm" value="with Symbol" />
      </node>
      <node concept="l2Vlx" id="5boToVMQySm" role="2iSdaV" />
      <node concept="3F0A7n" id="5boToVMJT_5" role="3EZMnx">
        <ref role="1NtTu8" to="u6v3:5boToVMJTj6" resolve="symbol" />
      </node>
    </node>
  </node>
</model>


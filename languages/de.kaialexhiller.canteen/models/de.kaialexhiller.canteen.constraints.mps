<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4ef55b2d-0485-4f3c-a06a-5758a49a4a26(de.kaialexhiller.canteen.constraints)">
  <persistence version="9" />
  <languages>
    <use id="5dae8159-ab99-46bb-a40d-0cee30ee7018" name="jetbrains.mps.lang.constraints.rules.kinds" version="0" />
    <use id="ea3159bf-f48e-4720-bde2-86dba75f0d34" name="jetbrains.mps.lang.context.defs" version="0" />
    <use id="e51810c5-7308-4642-bcb6-469e61b5dd18" name="jetbrains.mps.lang.constraints.msg.specification" version="0" />
    <use id="134c38d4-e3af-4d9e-b069-1c7df0a4005d" name="jetbrains.mps.lang.constraints.rules.skeleton" version="0" />
    <use id="b3551702-269c-4f05-ba61-58060cef4292" name="jetbrains.mps.lang.rulesAndMessages" version="0" />
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="6" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="0" />
    <use id="3ad5badc-1d9c-461c-b7b1-fa2fcd0a0ae7" name="jetbrains.mps.lang.context" version="0" />
    <use id="ad93155d-79b2-4759-b10c-55123e763903" name="jetbrains.mps.lang.messages" version="0" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="prp3" ref="r:52ea8481-08b2-4cbd-ad9d-1b42825f7d09(jetbrains.mps.lang.constraints.rules.kinds.constraints)" />
    <import index="u6v3" ref="r:4e321182-9bcb-42b3-94db-47d29f7a99dc(de.kaialexhiller.canteen.structure)" implicit="true" />
    <import index="xqb1" ref="r:04ec1dc0-bcb1-4130-ab2a-ea1eb63d2e83(de.kaialexhiller.canteen.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="ea3159bf-f48e-4720-bde2-86dba75f0d34" name="jetbrains.mps.lang.context.defs">
      <concept id="7291380803376202513" name="jetbrains.mps.lang.context.defs.structure.TypedDefReference" flags="ng" index="3QpRc$">
        <reference id="7291380803376221790" name="declaration" index="3QpVTF" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="1225271221393" name="jetbrains.mps.baseLanguage.structure.NPENotEqualsExpression" flags="nn" index="17QLQc" />
      <concept id="1225271369338" name="jetbrains.mps.baseLanguage.structure.IsEmptyOperation" flags="nn" index="17RlXB" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="b3551702-269c-4f05-ba61-58060cef4292" name="jetbrains.mps.lang.rulesAndMessages">
      <concept id="315923949160549991" name="jetbrains.mps.lang.rulesAndMessages.structure.RuleWithMessage" flags="ng" index="1DCEPf">
        <child id="1400749580825440508" name="rule" index="2j4cqI" />
        <child id="315923949160550022" name="messageProvider" index="1DCEQI" />
      </concept>
      <concept id="315923949160550017" name="jetbrains.mps.lang.rulesAndMessages.structure.InlineMessageProvider" flags="ng" index="1DCEQD">
        <child id="5258059200641510856" name="messagesExpr" index="16N$OO" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="134c38d4-e3af-4d9e-b069-1c7df0a4005d" name="jetbrains.mps.lang.constraints.rules.skeleton">
      <concept id="1867733327984720090" name="jetbrains.mps.lang.constraints.rules.skeleton.structure.RulesConstraintsRoot" flags="ng" index="3Oh7Pa">
        <reference id="1867733327984720094" name="concept" index="3Oh7Pe" />
        <child id="1867733327984720091" name="block" index="3Oh7Pb" />
      </concept>
      <concept id="1867733327985055562" name="jetbrains.mps.lang.constraints.rules.skeleton.structure.RulesBlock" flags="ng" index="3OnDbq">
        <reference id="1867733327985055564" name="kind" index="3OnDbs" />
        <child id="1867733327985055563" name="members" index="3OnDbr" />
      </concept>
    </language>
    <language id="47257bf3-78d3-470b-89d9-8c3261a61d15" name="jetbrains.mps.lang.constraints.rules">
      <concept id="1328301445982517233" name="jetbrains.mps.lang.constraints.rules.structure.ExpressionWrapper" flags="ng" index="2K0Yjh">
        <child id="1328301445982532877" name="expression" index="2K0yoH" />
      </concept>
      <concept id="315923949160453290" name="jetbrains.mps.lang.constraints.rules.structure.RuleIdHolder" flags="ng" index="1DRju2">
        <property id="6714410169261853888" name="ruleId" index="EcuMT" />
      </concept>
      <concept id="7291380803377228245" name="jetbrains.mps.lang.constraints.rules.structure.DefForRule" flags="ng" index="3QlHBw">
        <child id="1328301445982561451" name="expr" index="2K0Fub" />
        <child id="5473446470512342703" name="type" index="3RXm0Z" />
      </concept>
      <concept id="7291380803376279010" name="jetbrains.mps.lang.constraints.rules.structure.Rule" flags="ng" index="3Qq5Rn">
        <child id="1328301445982561464" name="expr" index="2K0Fuo" />
      </concept>
    </language>
    <language id="ad93155d-79b2-4759-b10c-55123e763903" name="jetbrains.mps.lang.messages">
      <concept id="5258059200642172255" name="jetbrains.mps.lang.messages.structure.CombinedMessageExpression" flags="ng" index="16I2mz">
        <child id="5258059200642172257" name="part" index="16I2mt" />
      </concept>
      <concept id="5258059200642278562" name="jetbrains.mps.lang.messages.structure.MacroMessageExpression" flags="ng" index="16Iohu">
        <child id="2716118816014328328" name="defRef" index="9Y7m$" />
      </concept>
      <concept id="5258059200641510853" name="jetbrains.mps.lang.messages.structure.LiteralMessageExpression" flags="ng" index="16N$OT">
        <property id="5258059200641510854" name="message" index="16N$OU" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1966870290088668512" name="jetbrains.mps.lang.smodel.structure.Enum_MemberLiteral" flags="ng" index="2ViDtV">
        <reference id="1966870290088668516" name="memberDeclaration" index="2ViDtZ" />
      </concept>
      <concept id="3562215692195599741" name="jetbrains.mps.lang.smodel.structure.SLinkImplicitSelect" flags="nn" index="13MTOL">
        <reference id="3562215692195600259" name="link" index="13MTZf" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <property id="1238684351431" name="asCast" index="1BlNFB" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
      <concept id="5779574625830813396" name="jetbrains.mps.lang.smodel.structure.EnumerationIdRefExpression" flags="ng" index="1XH99k">
        <reference id="5779574625830813397" name="enumDeclaration" index="1XH99l" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1235566554328" name="jetbrains.mps.baseLanguage.collections.structure.AnyOperation" flags="nn" index="2HwmR7" />
      <concept id="1235566831861" name="jetbrains.mps.baseLanguage.collections.structure.AllOperation" flags="nn" index="2HxqBE" />
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1240687580870" name="jetbrains.mps.baseLanguage.collections.structure.JoinOperation" flags="nn" index="3uJxvA">
        <child id="1240687658305" name="delimiter" index="3uJOhx" />
      </concept>
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="3Oh7Pa" id="2_H6PIKMb9f">
    <ref role="3Oh7Pe" to="u6v3:6dt6wfGrviQ" resolve="DailyMenu" />
    <node concept="3OnDbq" id="2_H6PIKMbdo" role="3Oh7Pb">
      <ref role="3OnDbs" to="prp3:6X8eyFnbIRR" resolve="CanBeChild" />
      <node concept="1DCEPf" id="2_H6PIKMcn_" role="3OnDbr">
        <node concept="3Qq5Rn" id="2_H6PIKMcnA" role="2j4cqI">
          <property role="EcuMT" value="2985072208638428646" />
          <property role="TrG5h" value="RequireVegetarianMeal" />
          <node concept="2K0Yjh" id="2_H6PIKMcnB" role="2K0Fuo">
            <node concept="2OqwBi" id="2_H6PIKMj3D" role="2K0yoH">
              <node concept="2OqwBi" id="2_H6PIKMf7o" role="2Oq$k0">
                <node concept="2OqwBi" id="2_H6PIKMd6E" role="2Oq$k0">
                  <node concept="3QpRc$" id="2_H6PIKMcDI" role="2Oq$k0">
                    <ref role="3QpVTF" to="prp3:6X8eyFnbL3m" resolve="childNode" />
                  </node>
                  <node concept="3Tsc0h" id="2_H6PIKMdmO" role="2OqNvi">
                    <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                  </node>
                </node>
                <node concept="13MTOL" id="2_H6PIKMioh" role="2OqNvi">
                  <ref role="13MTZf" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                </node>
              </node>
              <node concept="2HwmR7" id="2_H6PIKMp$t" role="2OqNvi">
                <node concept="1bVj0M" id="2_H6PIKMp$v" role="23t8la">
                  <node concept="3clFbS" id="2_H6PIKMp$w" role="1bW5cS">
                    <node concept="3clFbF" id="2_H6PIKMp$x" role="3cqZAp">
                      <node concept="3y3z36" id="2_H6PIKMp$y" role="3clFbG">
                        <node concept="2OqwBi" id="2_H6PIKMp$z" role="3uHU7w">
                          <node concept="1XH99k" id="2_H6PIKMp$$" role="2Oq$k0">
                            <ref role="1XH99l" to="u6v3:6dt6wfGrvhS" resolve="Diet" />
                          </node>
                          <node concept="2ViDtV" id="2_H6PIKMp$_" role="2OqNvi">
                            <ref role="2ViDtZ" to="u6v3:6dt6wfGrvhY" resolve="Omnivore" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2_H6PIKMp$A" role="3uHU7B">
                          <node concept="37vLTw" id="2_H6PIKMp$B" role="2Oq$k0">
                            <ref role="3cqZAo" node="2_H6PIKMp$D" resolve="it" />
                          </node>
                          <node concept="2qgKlT" id="2_H6PIKMp$C" role="2OqNvi">
                            <ref role="37wK5l" to="xqb1:8EeBQEta_7" resolve="diet" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="2_H6PIKMp$D" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="2_H6PIKMp$E" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1DCEQD" id="2_H6PIKMcnD" role="1DCEQI">
          <node concept="16I2mz" id="2_H6PIKMqIC" role="16N$OO">
            <node concept="16N$OT" id="2_H6PIKMsHr" role="16I2mt">
              <property role="16N$OU" value="There must be at least one vegetarian meal per day" />
            </node>
            <node concept="16N$OT" id="2_H6PIKMsJz" role="16I2mt">
              <property role="16N$OU" value="" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1DCEPf" id="2_H6PIKMt40" role="3OnDbr">
        <node concept="3Qq5Rn" id="2_H6PIKMt42" role="2j4cqI">
          <property role="EcuMT" value="2985072208638497026" />
          <property role="TrG5h" value="NoDuplicateMeals" />
          <node concept="2K0Yjh" id="2_H6PIKMt44" role="2K0Fuo">
            <node concept="2OqwBi" id="2_H6PIKOPiz" role="2K0yoH">
              <node concept="3QpRc$" id="3mpKCXPW31u" role="2Oq$k0">
                <ref role="3QpVTF" node="3mpKCXPW0SS" resolve="duplicate_meal_days" />
              </node>
              <node concept="17RlXB" id="3mpKCXPXQ8y" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="1DCEQD" id="2_H6PIKMt48" role="1DCEQI">
          <node concept="16I2mz" id="2_H6PIKNFWb" role="16N$OO">
            <node concept="16N$OT" id="3mpKCXPW3aF" role="16I2mt">
              <property role="16N$OU" value="Today’s meal is also served on " />
            </node>
            <node concept="16Iohu" id="3mpKCXPW3aD" role="16I2mt">
              <node concept="3QpRc$" id="3mpKCXPW3aE" role="9Y7m$">
                <ref role="3QpVTF" node="3mpKCXPW0SS" resolve="duplicate_meal_days" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3QlHBw" id="3mpKCXPW0SS" role="3OnDbr">
        <property role="TrG5h" value="duplicate_meal_days" />
        <node concept="17QB3L" id="3mpKCXPWWVU" role="3RXm0Z" />
        <node concept="2K0Yjh" id="3mpKCXPW0SW" role="2K0Fub">
          <node concept="2OqwBi" id="3mpKCXPWX6w" role="2K0yoH">
            <node concept="2OqwBi" id="3mpKCXPW1Cy" role="2Oq$k0">
              <node concept="3QpRc$" id="3mpKCXPW1uk" role="2Oq$k0">
                <ref role="3QpVTF" to="prp3:6X8eyFnbL3m" resolve="childNode" />
              </node>
              <node concept="2qgKlT" id="3mpKCXPW20C" role="2OqNvi">
                <ref role="37wK5l" to="xqb1:2_H6PIKNGOd" resolve="duplicate_meal_days" />
              </node>
            </node>
            <node concept="3uJxvA" id="3mpKCXPWYNn" role="2OqNvi">
              <node concept="Xl_RD" id="3mpKCXPWZxB" role="3uJOhx">
                <property role="Xl_RC" value=", " />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3Oh7Pa" id="1jE3pLwEdgf">
    <ref role="3Oh7Pe" to="u6v3:6dt6wfGrvip" resolve="MeasuredIngedient" />
    <node concept="3OnDbq" id="1jE3pLwEdgi" role="3Oh7Pb">
      <ref role="3OnDbs" to="prp3:6X8eyFnbIRR" resolve="CanBeChild" />
      <node concept="1DCEPf" id="1jE3pLwEdgl" role="3OnDbr">
        <node concept="3Qq5Rn" id="1jE3pLwEdgm" role="2j4cqI">
          <property role="EcuMT" value="1507031990596588566" />
          <property role="TrG5h" value="uniqueInPantry" />
          <node concept="2K0Yjh" id="1jE3pLwEdgn" role="2K0Fuo">
            <node concept="2OqwBi" id="1jE3pLwEhCO" role="2K0yoH">
              <node concept="3QpRc$" id="1jE3pLwEdgI" role="2Oq$k0">
                <ref role="3QpVTF" node="1jE3pLwEdq5" resolve="other_pantry_ingredients" />
              </node>
              <node concept="2HxqBE" id="1jE3pLwEhTH" role="2OqNvi">
                <node concept="1bVj0M" id="1jE3pLwEhTJ" role="23t8la">
                  <node concept="3clFbS" id="1jE3pLwEhTK" role="1bW5cS">
                    <node concept="3clFbF" id="1jE3pLwEhXb" role="3cqZAp">
                      <node concept="17QLQc" id="1jE3pLwEii_" role="3clFbG">
                        <node concept="2OqwBi" id="1jE3pLwJVc6" role="3uHU7w">
                          <node concept="3QpRc$" id="1jE3pLwEipk" role="2Oq$k0">
                            <ref role="3QpVTF" to="prp3:6X8eyFnbL3m" resolve="childNode" />
                          </node>
                          <node concept="3TrEf2" id="1jE3pLwJVpK" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="1jE3pLwJUA2" role="3uHU7B">
                          <node concept="37vLTw" id="1jE3pLwEhXa" role="2Oq$k0">
                            <ref role="3cqZAo" node="1jE3pLwEhTL" resolve="it" />
                          </node>
                          <node concept="3TrEf2" id="1jE3pLwJUOJ" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="1jE3pLwEhTL" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="1jE3pLwEhTM" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1DCEQD" id="1jE3pLwEdgp" role="1DCEQI">
          <node concept="16I2mz" id="1jE3pLwF_F1" role="16N$OO">
            <node concept="16N$OT" id="1jE3pLwF_F2" role="16I2mt">
              <property role="16N$OU" value="Ingredient must be in the pantry only once" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3QlHBw" id="1jE3pLwEdq5" role="3OnDbr">
        <property role="TrG5h" value="other_pantry_ingredients" />
        <node concept="A3Dl8" id="1jE3pLwEhjy" role="3RXm0Z">
          <node concept="3Tqbb2" id="1jE3pLwEhjz" role="A3Ik2">
            <ref role="ehGHo" to="u6v3:6dt6wfGrvip" resolve="MeasuredIngedient" />
          </node>
        </node>
        <node concept="2K0Yjh" id="1jE3pLwEdq9" role="2K0Fub">
          <node concept="2OqwBi" id="1jE3pLwJPxS" role="2K0yoH">
            <node concept="2OqwBi" id="1jE3pLwIh0T" role="2Oq$k0">
              <node concept="1PxgMI" id="1jE3pLwEdzf" role="2Oq$k0">
                <property role="1BlNFB" value="true" />
                <node concept="chp4Y" id="1jE3pLwEdzI" role="3oSUPX">
                  <ref role="cht4Q" to="u6v3:7moC6rm27Me" resolve="Pantry" />
                </node>
                <node concept="3QpRc$" id="1jE3pLwEdqo" role="1m5AlR">
                  <ref role="3QpVTF" to="prp3:yXuRM1RLuH" resolve="parentNode" />
                </node>
              </node>
              <node concept="3Tsc0h" id="1jE3pLwIhfO" role="2OqNvi">
                <ref role="3TtcxE" to="u6v3:7moC6rm27TX" resolve="staples" />
              </node>
            </node>
            <node concept="3zZkjj" id="1jE3pLwJSA2" role="2OqNvi">
              <node concept="1bVj0M" id="1jE3pLwJSA4" role="23t8la">
                <node concept="3clFbS" id="1jE3pLwJSA5" role="1bW5cS">
                  <node concept="3clFbF" id="1jE3pLwJSIt" role="3cqZAp">
                    <node concept="17QLQc" id="1jE3pLwJThk" role="3clFbG">
                      <node concept="3QpRc$" id="1jE3pLwJTvk" role="3uHU7w">
                        <ref role="3QpVTF" to="prp3:6X8eyFnbL3m" resolve="childNode" />
                      </node>
                      <node concept="37vLTw" id="1jE3pLwJSIs" role="3uHU7B">
                        <ref role="3cqZAo" node="1jE3pLwJSA6" resolve="it" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="1jE3pLwJSA6" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="1jE3pLwJSA7" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3Oh7Pa" id="7moC6rlR1Wx">
    <ref role="3Oh7Pe" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
    <node concept="3OnDbq" id="7moC6rlR20C" role="3Oh7Pb">
      <ref role="3OnDbs" to="prp3:6X8eyFnbIRR" resolve="CanBeChild" />
      <node concept="1DCEPf" id="7moC6rlR22j" role="3OnDbr">
        <node concept="3Qq5Rn" id="7moC6rlR22k" role="2j4cqI">
          <property role="EcuMT" value="8473699062432669844" />
          <property role="TrG5h" value="uniqueWeek" />
          <node concept="2K0Yjh" id="7moC6rlRHwL" role="2K0Fuo">
            <node concept="2OqwBi" id="7moC6rlRJ_J" role="2K0yoH">
              <node concept="3QpRc$" id="7moC6rlRJiX" role="2Oq$k0">
                <ref role="3QpVTF" node="7moC6rlR6BU" resolve="simultaneous_meal_plans" />
              </node>
              <node concept="1v1jN8" id="7moC6rlRK4u" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="1DCEQD" id="7moC6rlR22n" role="1DCEQI">
          <node concept="16I2mz" id="7moC6rlRK6A" role="16N$OO">
            <node concept="16N$OT" id="7moC6rlRK6B" role="16I2mt">
              <property role="16N$OU" value="There must be only one meal plan for a given week" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3QlHBw" id="7moC6rlR3cm" role="3OnDbr">
        <property role="TrG5h" value="other_meal_plans" />
        <node concept="2K0Yjh" id="7moC6rlR3cq" role="2K0Fub">
          <node concept="2OqwBi" id="7moC6rlRpL2" role="2K0yoH">
            <node concept="2OqwBi" id="7moC6rlRkQ_" role="2Oq$k0">
              <node concept="1PxgMI" id="7moC6rlR6kj" role="2Oq$k0">
                <property role="1BlNFB" value="true" />
                <node concept="chp4Y" id="7moC6rlR6tt" role="3oSUPX">
                  <ref role="cht4Q" to="u6v3:5mCwD4fNZLy" resolve="MealPlan" />
                </node>
                <node concept="3QpRc$" id="7moC6rlR614" role="1m5AlR">
                  <ref role="3QpVTF" to="prp3:yXuRM1RLuH" resolve="parentNode" />
                </node>
              </node>
              <node concept="3Tsc0h" id="7moC6rlRlai" role="2OqNvi">
                <ref role="3TtcxE" to="u6v3:5mCwD4fO8YZ" resolve="meal_plans" />
              </node>
            </node>
            <node concept="3zZkjj" id="7moC6rlRulg" role="2OqNvi">
              <node concept="1bVj0M" id="7moC6rlRulj" role="23t8la">
                <node concept="3clFbS" id="7moC6rlRulk" role="1bW5cS">
                  <node concept="3clFbF" id="7moC6rlRuwp" role="3cqZAp">
                    <node concept="17QLQc" id="7moC6rlRvs_" role="3clFbG">
                      <node concept="3QpRc$" id="7moC6rlRvEa" role="3uHU7w">
                        <ref role="3QpVTF" to="prp3:6X8eyFnbL3m" resolve="childNode" />
                      </node>
                      <node concept="37vLTw" id="7moC6rlRuwo" role="3uHU7B">
                        <ref role="3cqZAo" node="7moC6rlRull" resolve="it" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="7moC6rlRull" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="7moC6rlRulm" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="A3Dl8" id="7moC6rlRnCY" role="3RXm0Z">
          <node concept="3Tqbb2" id="7moC6rlRnKc" role="A3Ik2">
            <ref role="ehGHo" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
          </node>
        </node>
      </node>
      <node concept="3QlHBw" id="7moC6rlR6BU" role="3OnDbr">
        <property role="TrG5h" value="simultaneous_meal_plans" />
        <node concept="A3Dl8" id="7moC6rlR7Qh" role="3RXm0Z">
          <node concept="3Tqbb2" id="7moC6rlR80w" role="A3Ik2">
            <ref role="ehGHo" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
          </node>
        </node>
        <node concept="2K0Yjh" id="7moC6rlR6BY" role="2K0Fub">
          <node concept="2OqwBi" id="7moC6rlRGt9" role="2K0yoH">
            <node concept="3QpRc$" id="7moC6rlRG7i" role="2Oq$k0">
              <ref role="3QpVTF" node="7moC6rlR3cm" resolve="other_meal_plans" />
            </node>
            <node concept="3zZkjj" id="7moC6rlRGyf" role="2OqNvi">
              <node concept="1bVj0M" id="7moC6rlRGyg" role="23t8la">
                <node concept="3clFbS" id="7moC6rlRGyh" role="1bW5cS">
                  <node concept="3clFbF" id="7moC6rlRGyi" role="3cqZAp">
                    <node concept="1Wc70l" id="7moC6rlRGyj" role="3clFbG">
                      <node concept="3clFbC" id="7moC6rlRLNS" role="3uHU7w">
                        <node concept="2OqwBi" id="7moC6rlRGyo" role="3uHU7B">
                          <node concept="37vLTw" id="7moC6rlRGyp" role="2Oq$k0">
                            <ref role="3cqZAo" node="7moC6rlRGyy" resolve="it" />
                          </node>
                          <node concept="3TrcHB" id="7moC6rlRGyq" role="2OqNvi">
                            <ref role="3TsBF5" to="u6v3:5mCwD4fO8Ed" resolve="calendar_week" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="7moC6rlRGyl" role="3uHU7w">
                          <node concept="3QpRc$" id="7moC6rlRGym" role="2Oq$k0">
                            <ref role="3QpVTF" to="prp3:6X8eyFnbL3m" resolve="childNode" />
                          </node>
                          <node concept="3TrcHB" id="7moC6rlRGyn" role="2OqNvi">
                            <ref role="3TsBF5" to="u6v3:5mCwD4fO8Ed" resolve="calendar_week" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbC" id="7moC6rlRLrH" role="3uHU7B">
                        <node concept="2OqwBi" id="7moC6rlRGys" role="3uHU7B">
                          <node concept="37vLTw" id="7moC6rlRGyt" role="2Oq$k0">
                            <ref role="3cqZAo" node="7moC6rlRGyy" resolve="it" />
                          </node>
                          <node concept="3TrcHB" id="7moC6rlRGyu" role="2OqNvi">
                            <ref role="3TsBF5" to="u6v3:5mCwD4fO8zh" resolve="year" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="7moC6rlRGyv" role="3uHU7w">
                          <node concept="3QpRc$" id="7moC6rlRGyw" role="2Oq$k0">
                            <ref role="3QpVTF" to="prp3:6X8eyFnbL3m" resolve="childNode" />
                          </node>
                          <node concept="3TrcHB" id="7moC6rlRGyx" role="2OqNvi">
                            <ref role="3TsBF5" to="u6v3:5mCwD4fO8zh" resolve="year" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="7moC6rlRGyy" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="7moC6rlRGyz" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>


<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:22b95f16-4e41-4357-9e60-713c8168ffc9(de.kaialexhiller.canteen.generator00.templates@generator)">
  <persistence version="9" />
  <languages>
    <use id="479c7a8c-02f9-43b5-9139-d910cb22f298" name="jetbrains.mps.core.xml" version="0" />
    <use id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext" version="2" />
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="4" />
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="u6v3" ref="r:4e321182-9bcb-42b3-94db-47d29f7a99dc(de.kaialexhiller.canteen.structure)" />
    <import index="iuxj" ref="r:64db3a92-5968-4a73-b456-34504a2d97a6(jetbrains.mps.core.xml.structure)" />
    <import index="xqb1" ref="r:04ec1dc0-bcb1-4130-ab2a-ea1eb63d2e83(de.kaialexhiller.canteen.behavior)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="479c7a8c-02f9-43b5-9139-d910cb22f298" name="jetbrains.mps.core.xml">
      <concept id="2133624044437898907" name="jetbrains.mps.core.xml.structure.XmlDoctypeDeclaration" flags="ng" index="29q25o">
        <property id="2133624044437898910" name="doctypeName" index="29q25t" />
      </concept>
      <concept id="6666499814681515200" name="jetbrains.mps.core.xml.structure.XmlFile" flags="ng" index="2pMbU2">
        <property id="7692057055172140539" name="fileExtension" index="17bj3o" />
        <child id="6666499814681515201" name="document" index="2pMbU3" />
      </concept>
      <concept id="6666499814681541919" name="jetbrains.mps.core.xml.structure.XmlTextValue" flags="ng" index="2pMdtt">
        <property id="6666499814681541920" name="text" index="2pMdty" />
      </concept>
      <concept id="6666499814681299057" name="jetbrains.mps.core.xml.structure.XmlProlog" flags="ng" index="2pNm8N">
        <child id="7604553062773674214" name="elements" index="BGLLu" />
      </concept>
      <concept id="6666499814681415858" name="jetbrains.mps.core.xml.structure.XmlElement" flags="ng" index="2pNNFK">
        <property id="6666499814681415862" name="tagName" index="2pNNFO" />
        <child id="6666499814681415861" name="attributes" index="2pNNFR" />
        <child id="1622293396948928802" name="content" index="3o6s8t" />
      </concept>
      <concept id="6666499814681447923" name="jetbrains.mps.core.xml.structure.XmlAttribute" flags="ng" index="2pNUuL">
        <property id="6666499814681447926" name="attrName" index="2pNUuO" />
        <child id="6666499814681541918" name="value" index="2pMdts" />
      </concept>
      <concept id="1622293396948952339" name="jetbrains.mps.core.xml.structure.XmlText" flags="nn" index="3o6iSG">
        <property id="1622293396948953704" name="value" index="3o6i5n" />
      </concept>
      <concept id="6786756355279841993" name="jetbrains.mps.core.xml.structure.XmlDocument" flags="ng" index="3rIKKV">
        <child id="6666499814681299055" name="rootElement" index="2pNm8H" />
        <child id="6666499814681299060" name="prolog" index="2pNm8Q" />
      </concept>
      <concept id="2301667890727559839" name="jetbrains.mps.core.xml.structure.XmlSingleLineText" flags="ng" index="1Aj0xv" />
      <concept id="3080189811177426492" name="jetbrains.mps.core.xml.structure.XmlNoSpaceValue" flags="ng" index="1UJgpG" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114729360583" name="jetbrains.mps.lang.generator.structure.CopySrcListMacro" flags="ln" index="2b32R4">
        <child id="1168278589236" name="sourceNodesQuery" index="2P8S$" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1200911492601" name="mappingLabel" index="2rTMjI" />
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
        <child id="1167514678247" name="rootMappingRule" index="3lj3bC" />
      </concept>
      <concept id="1168559333462" name="jetbrains.mps.lang.generator.structure.TemplateDeclarationReference" flags="ln" index="j$656" />
      <concept id="1170725621272" name="jetbrains.mps.lang.generator.structure.MapSrcMacro_MapperFunction" flags="in" index="2kFOW8" />
      <concept id="1168619357332" name="jetbrains.mps.lang.generator.structure.RootTemplateAnnotation" flags="lg" index="n94m4">
        <reference id="1168619429071" name="applicableConcept" index="n9lRv" />
      </concept>
      <concept id="1095672379244" name="jetbrains.mps.lang.generator.structure.TemplateFragment" flags="ng" index="raruj" />
      <concept id="1200911316486" name="jetbrains.mps.lang.generator.structure.MappingLabelDeclaration" flags="lg" index="2rT7sh">
        <reference id="1200911342686" name="sourceConcept" index="2rTdP9" />
        <reference id="1200913004646" name="targetConcept" index="2rZz_L" />
      </concept>
      <concept id="1722980698497626400" name="jetbrains.mps.lang.generator.structure.ITemplateCall" flags="ng" index="v9R3L">
        <reference id="1722980698497626483" name="template" index="v9R2y" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1092059087312" name="jetbrains.mps.lang.generator.structure.TemplateDeclaration" flags="ig" index="13MO4I">
        <reference id="1168285871518" name="applicableConcept" index="3gUMe" />
        <child id="1092060348987" name="contentNode" index="13RCb5" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1087833466690" name="jetbrains.mps.lang.generator.structure.NodeMacro" flags="lg" index="17VmuZ">
        <reference id="1200912223215" name="mappingLabel" index="2rW$FS" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1167514355419" name="jetbrains.mps.lang.generator.structure.Root_MappingRule" flags="lg" index="3lhOvk">
        <reference id="1167514355421" name="template" index="3lhOvi" />
      </concept>
      <concept id="1131073187192" name="jetbrains.mps.lang.generator.structure.MapSrcNodeMacro" flags="ln" index="1pdMLZ">
        <child id="1170725844563" name="mapperFunction" index="2kGFt3" />
      </concept>
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1311078761699563727" name="jetbrains.mps.lang.generator.structure.InsertMacro_CreateNodeQuery" flags="in" index="3_AbJw" />
      <concept id="1311078761699563726" name="jetbrains.mps.lang.generator.structure.InsertMacro" flags="ln" index="3_AbJx">
        <child id="1311078761699602381" name="createNodeQuery" index="3_A0Ny" />
      </concept>
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1216860049627" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_GetOutputByLabelAndInput" flags="nn" index="1iwH70">
        <reference id="1216860049628" name="label" index="1iwH77" />
        <child id="1216860049632" name="inputNode" index="1iwH7V" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
      <concept id="3228980938641126117" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_UniqueValidId" flags="ng" index="1AYpvF">
        <child id="2537089342344730415" name="node" index="2QPDDZ" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157993911077" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitProperty" flags="ng" index="2pJxcG">
        <reference id="5455284157993911078" name="property" index="2pJxcJ" />
        <child id="1595412875168045201" name="initValue" index="28ntcv" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
      <concept id="6985522012210254362" name="jetbrains.mps.lang.quotation.structure.NodeBuilderPropertyExpression" flags="nn" index="WxPPo">
        <child id="6985522012210254363" name="expression" index="WxPPp" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1144146199828" name="jetbrains.mps.lang.smodel.structure.Node_CopyOperation" flags="nn" index="1$rogu" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="name_DebugInfo" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="7mpslIOlNP_">
    <property role="TrG5h" value="main" />
    <node concept="2rT7sh" id="3mpKCXRpNda" role="2rTMjI">
      <property role="TrG5h" value="links" />
      <ref role="2rZz_L" to="iuxj:5M4a$b5jfOv" resolve="XmlTextValue" />
      <ref role="2rTdP9" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
    </node>
    <node concept="3aamgX" id="7mpslIOnj9m" role="3acgRq">
      <ref role="30HIoZ" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
      <node concept="j$656" id="7mpslIOnj9s" role="1lVwrX">
        <ref role="v9R2y" node="7mpslIOnj9q" resolve="reduce_WeekMealPlan" />
      </node>
    </node>
    <node concept="3aamgX" id="3mpKCXPc1je" role="3acgRq">
      <ref role="30HIoZ" to="u6v3:6dt6wfGrvip" resolve="MeasuredIngedient" />
      <node concept="j$656" id="3mpKCXPc1ZF" role="1lVwrX">
        <ref role="v9R2y" node="3mpKCXPc1ZD" resolve="reduce_MeasuredIngedient" />
      </node>
    </node>
    <node concept="3aamgX" id="3mpKCXPhoAb" role="3acgRq">
      <ref role="30HIoZ" to="u6v3:5mCwD4fO2$5" resolve="Meal" />
      <node concept="j$656" id="3mpKCXPCiYQ" role="1lVwrX">
        <ref role="v9R2y" node="3mpKCXPCiYO" resolve="reduce_Meal" />
      </node>
    </node>
    <node concept="3aamgX" id="7mpslIOoauM" role="3acgRq">
      <ref role="30HIoZ" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
      <node concept="j$656" id="7mpslIOoauU" role="1lVwrX">
        <ref role="v9R2y" node="7mpslIOoauS" resolve="reduce_Recipe" />
      </node>
    </node>
    <node concept="3lhOvk" id="7mpslIOlNPA" role="3lj3bC">
      <ref role="30HIoZ" to="u6v3:5mCwD4fNZLy" resolve="MealPlan" />
      <ref role="3lhOvi" node="7mpslIOlNPC" resolve="meal_plan" />
    </node>
  </node>
  <node concept="2pMbU2" id="7mpslIOlNPC">
    <property role="TrG5h" value="meal_plan" />
    <property role="17bj3o" value="html" />
    <node concept="3rIKKV" id="7mpslIOlNPD" role="2pMbU3">
      <node concept="2pNNFK" id="7mpslIOlOax" role="2pNm8H">
        <property role="2pNNFO" value="html" />
        <node concept="2pNUuL" id="3mpKCXR6_AB" role="2pNNFR">
          <property role="2pNUuO" value="lang" />
          <node concept="2pMdtt" id="3mpKCXR6_AC" role="2pMdts">
            <property role="2pMdty" value="en" />
          </node>
        </node>
        <node concept="3o6iSG" id="7mpslIOlOdp" role="3o6s8t" />
        <node concept="2pNNFK" id="7mpslIOlOlc" role="3o6s8t">
          <property role="2pNNFO" value="head" />
          <node concept="3o6iSG" id="7mpslIOnhkh" role="3o6s8t" />
          <node concept="2pNNFK" id="7mpslIOnho0" role="3o6s8t">
            <property role="2pNNFO" value="title" />
            <node concept="3o6iSG" id="7mpslIOnhtM" role="3o6s8t">
              <property role="3o6i5n" value="ACME Canteen Meal Plan" />
            </node>
          </node>
          <node concept="2pNNFK" id="3mpKCXPlECe" role="3o6s8t">
            <property role="2pNNFO" value="style" />
            <node concept="3o6iSG" id="3mpKCXPlEDy" role="3o6s8t" />
            <node concept="3o6iSG" id="3mpKCXQXv_T" role="3o6s8t">
              <property role="3o6i5n" value="body {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQXvFB" role="3o6s8t">
              <property role="3o6i5n" value="    font-family: sans-serif;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQXvD$" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
            <node concept="3o6iSG" id="3mpKCXPrYgq" role="3o6s8t">
              <property role="3o6i5n" value="th {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXPrYu7" role="3o6s8t">
              <property role="3o6i5n" value="    text-align: left;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXPyT8z" role="3o6s8t">
              <property role="3o6i5n" value="    padding-right: 1em;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXPrYjS" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
            <node concept="3o6iSG" id="3mpKCXRe25S" role="3o6s8t">
              <property role="3o6i5n" value="a {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXRe29Y" role="3o6s8t">
              <property role="3o6i5n" value="    color: black;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXRo5lA" role="3o6s8t">
              <property role="3o6i5n" value="    display: inline-block;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXRe27x" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQK0eQ" role="3o6s8t">
              <property role="3o6i5n" value="#meals {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQK0lM" role="3o6s8t">
              <property role="3o6i5n" value="    display: grid;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQKLt0" role="3o6s8t">
              <property role="3o6i5n" value="    grid-template-columns: repeat(auto-fill, minmax(19em, 1fr));" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQO61N" role="3o6s8t">
              <property role="3o6i5n" value="    gap: 0.5em;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQK0jJ" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
            <node concept="3o6iSG" id="3mpKCXPp$dw" role="3o6s8t">
              <property role="3o6i5n" value="#meals h2 {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQO6bY" role="3o6s8t">
              <property role="3o6i5n" value="    grid-column: 1/-1;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXPp$ol" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQSv_1" role="3o6s8t">
              <property role="3o6i5n" value="#meals h3, #meals h2 {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQSvHA" role="3o6s8t">
              <property role="3o6i5n" value="    margin-top: 0;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQSvFz" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQTglw" role="3o6s8t">
              <property role="3o6i5n" value="#meals section {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQTgtE" role="3o6s8t">
              <property role="3o6i5n" value="    border: 1px solid grey;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQVM90" role="3o6s8t">
              <property role="3o6i5n" value="    border-radius: 0.75em;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQU5hB" role="3o6s8t">
              <property role="3o6i5n" value="    padding: 0.75em;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQTgrB" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ6BDS" role="3o6s8t">
              <property role="3o6i5n" value="@keyframes highlight {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ6Cyn" role="3o6s8t">
              <property role="3o6i5n" value="    0% {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ6CDi" role="3o6s8t">
              <property role="3o6i5n" value="        background: grey;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ6D25" role="3o6s8t">
              <property role="3o6i5n" value="    }" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ6D7M" role="3o6s8t">
              <property role="3o6i5n" value="    100% {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ6Dfx" role="3o6s8t">
              <property role="3o6i5n" value="        background: none;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ6Dqw" role="3o6s8t">
              <property role="3o6i5n" value="    }" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ6DtL" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
            <node concept="3o6iSG" id="3mpKCXPmsKx" role="3o6s8t">
              <property role="3o6i5n" value="#ingredients section:target {" />
            </node>
            <node concept="3o6iSG" id="3mpKCXQ47cg" role="3o6s8t">
              <property role="3o6i5n" value="    animation: highlight 1s;" />
            </node>
            <node concept="3o6iSG" id="3mpKCXPmsOh" role="3o6s8t">
              <property role="3o6i5n" value="}" />
            </node>
          </node>
        </node>
        <node concept="2pNNFK" id="7mpslIOlOoU" role="3o6s8t">
          <property role="2pNNFO" value="body" />
          <node concept="3o6iSG" id="7mpslIOlOvy" role="3o6s8t" />
          <node concept="2pNNFK" id="7mpslIOlOBJ" role="3o6s8t">
            <property role="2pNNFO" value="header" />
            <node concept="3o6iSG" id="7mpslIOlP0P" role="3o6s8t" />
            <node concept="2pNNFK" id="7mpslIOlP4a" role="3o6s8t">
              <property role="2pNNFO" value="h1" />
              <node concept="3o6iSG" id="7mpslIOlP5s" role="3o6s8t">
                <property role="3o6i5n" value="ACME Canteen" />
              </node>
            </node>
          </node>
          <node concept="2pNNFK" id="7mpslIOmVrg" role="3o6s8t">
            <property role="2pNNFO" value="section" />
            <node concept="2pNUuL" id="3mpKCXPlEQk" role="2pNNFR">
              <property role="2pNUuO" value="id" />
              <node concept="2pMdtt" id="3mpKCXPlEQl" role="2pMdts">
                <property role="2pMdty" value="meals" />
              </node>
            </node>
            <node concept="3o6iSG" id="7mpslIOmVxF" role="3o6s8t" />
            <node concept="2pNNFK" id="7mpslIOmVAC" role="3o6s8t">
              <property role="2pNNFO" value="h2" />
              <node concept="3o6iSG" id="7mpslIOmVCk" role="3o6s8t">
                <property role="3o6i5n" value="Meals" />
              </node>
            </node>
            <node concept="2pNNFK" id="7mpslIOlOQV" role="3o6s8t">
              <property role="2pNNFO" value="section" />
              <node concept="3o6iSG" id="7mpslIOlOSe" role="3o6s8t" />
              <node concept="2pNNFK" id="7mpslIOmWzr" role="3o6s8t">
                <property role="2pNNFO" value="h3" />
                <node concept="3o6iSG" id="7mpslIOmW_7" role="3o6s8t">
                  <property role="3o6i5n" value="2024-W00" />
                </node>
              </node>
              <node concept="2b32R4" id="7mpslIOniEB" role="lGtFl">
                <node concept="3JmXsc" id="7mpslIOniEE" role="2P8S$">
                  <node concept="3clFbS" id="7mpslIOniEF" role="2VODD2">
                    <node concept="3clFbF" id="7mpslIOniEL" role="3cqZAp">
                      <node concept="2OqwBi" id="7mpslIOniEG" role="3clFbG">
                        <node concept="3Tsc0h" id="7mpslIOniEJ" role="2OqNvi">
                          <ref role="3TtcxE" to="u6v3:5mCwD4fO8YZ" resolve="meal_plans" />
                        </node>
                        <node concept="30H73N" id="7mpslIOniEK" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2pNNFK" id="7mpslIOmVv4" role="3o6s8t">
            <property role="2pNNFO" value="section" />
            <node concept="2pNUuL" id="3mpKCXPlF26" role="2pNNFR">
              <property role="2pNUuO" value="id" />
              <node concept="2pMdtt" id="3mpKCXPlF27" role="2pMdts">
                <property role="2pMdty" value="ingredients" />
              </node>
            </node>
            <node concept="3o6iSG" id="7mpslIOmW4r" role="3o6s8t" />
            <node concept="2pNNFK" id="7mpslIOmW7K" role="3o6s8t">
              <property role="2pNNFO" value="h2" />
              <node concept="3o6iSG" id="7mpslIOmW8C" role="3o6s8t">
                <property role="3o6i5n" value="Ingredients" />
              </node>
            </node>
            <node concept="2pNNFK" id="7mpslIOngHY" role="3o6s8t">
              <property role="2pNNFO" value="section" />
              <node concept="3o6iSG" id="7mpslIOngIS" role="3o6s8t" />
              <node concept="2pNNFK" id="7mpslIOngPX" role="3o6s8t">
                <property role="2pNNFO" value="h3" />
                <node concept="3o6iSG" id="7mpslIOngVl" role="3o6s8t">
                  <property role="3o6i5n" value="Lasagna" />
                </node>
              </node>
              <node concept="2b32R4" id="7mpslIOnZC$" role="lGtFl">
                <node concept="3JmXsc" id="7mpslIOnZCB" role="2P8S$">
                  <node concept="3clFbS" id="7mpslIOnZCC" role="2VODD2">
                    <node concept="3clFbF" id="3mpKCXQvJHZ" role="3cqZAp">
                      <node concept="2OqwBi" id="3mpKCXQvJWb" role="3clFbG">
                        <node concept="30H73N" id="3mpKCXQvJHY" role="2Oq$k0" />
                        <node concept="2qgKlT" id="3mpKCXQvKh7" role="2OqNvi">
                          <ref role="37wK5l" to="xqb1:3mpKCXQvqDH" resolve="all_recipes" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2pNm8N" id="7mpslIOlNZT" role="2pNm8Q">
        <node concept="29q25o" id="7mpslIOlO1X" role="BGLLu">
          <property role="29q25t" value="html" />
        </node>
      </node>
    </node>
    <node concept="n94m4" id="7mpslIOlNPF" role="lGtFl">
      <ref role="n9lRv" to="u6v3:5mCwD4fNZLy" resolve="MealPlan" />
    </node>
  </node>
  <node concept="13MO4I" id="7mpslIOnj9q">
    <property role="TrG5h" value="reduce_WeekMealPlan" />
    <ref role="3gUMe" to="u6v3:5mCwD4fO6Xa" resolve="WeekMealPlan" />
    <node concept="2pNNFK" id="7mpslIOnjuf" role="13RCb5">
      <property role="2pNNFO" value="section" />
      <node concept="3o6iSG" id="7mpslIOnjz9" role="3o6s8t" />
      <node concept="2pNNFK" id="7mpslIOnjKE" role="3o6s8t">
        <property role="2pNNFO" value="h3" />
        <node concept="2pNNFK" id="3mpKCXQ$LeL" role="3o6s8t">
          <property role="2pNNFO" value="time" />
          <node concept="3o6iSG" id="3mpKCXQ$LK5" role="3o6s8t">
            <property role="3o6i5n" value="2024-W00" />
            <node concept="17Uvod" id="3mpKCXQ_XYv" role="lGtFl">
              <property role="2qtEX9" value="value" />
              <property role="P4ACc" value="479c7a8c-02f9-43b5-9139-d910cb22f298/1622293396948952339/1622293396948953704" />
              <node concept="3zFVjK" id="3mpKCXQ_XYw" role="3zH0cK">
                <node concept="3clFbS" id="3mpKCXQ_XYx" role="2VODD2">
                  <node concept="3cpWs8" id="3mpKCXQ_Y59" role="3cqZAp">
                    <node concept="3cpWsn" id="3mpKCXQ_Y5a" role="3cpWs9">
                      <property role="TrG5h" value="calendar_week" />
                      <node concept="17QB3L" id="3mpKCXQ_Y5b" role="1tU5fm" />
                      <node concept="2YIFZM" id="3mpKCXQ_Y5c" role="33vP2m">
                        <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...)" resolve="format" />
                        <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                        <node concept="Xl_RD" id="3mpKCXQ_Y5d" role="37wK5m">
                          <property role="Xl_RC" value="%02d" />
                        </node>
                        <node concept="2OqwBi" id="3mpKCXQ_Y5e" role="37wK5m">
                          <node concept="30H73N" id="3mpKCXQ_Y5f" role="2Oq$k0" />
                          <node concept="3TrcHB" id="3mpKCXQ_Y5g" role="2OqNvi">
                            <ref role="3TsBF5" to="u6v3:5mCwD4fO8Ed" resolve="calendar_week" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="3mpKCXQ_Y5h" role="3cqZAp">
                    <node concept="3cpWs3" id="3mpKCXQ_Y5i" role="3clFbG">
                      <node concept="3cpWs3" id="3mpKCXQ_Y5j" role="3uHU7B">
                        <node concept="2OqwBi" id="3mpKCXQ_Y5k" role="3uHU7B">
                          <node concept="30H73N" id="3mpKCXQ_Y5l" role="2Oq$k0" />
                          <node concept="3TrcHB" id="3mpKCXQ_Y5m" role="2OqNvi">
                            <ref role="3TsBF5" to="u6v3:5mCwD4fO8zh" resolve="year" />
                          </node>
                        </node>
                        <node concept="Xl_RD" id="3mpKCXQ_Y5n" role="3uHU7w">
                          <property role="Xl_RC" value="-W" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="3mpKCXQ_Y5o" role="3uHU7w">
                        <ref role="3cqZAo" node="3mpKCXQ_Y5a" resolve="calendar_week" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2pNNFK" id="3mpKCXPfFz2" role="3o6s8t">
        <property role="2pNNFO" value="table" />
        <node concept="3o6iSG" id="3mpKCXPfF$m" role="3o6s8t" />
        <node concept="2pNNFK" id="3mpKCXPfFC5" role="3o6s8t">
          <property role="2pNNFO" value="tr" />
          <node concept="3o6iSG" id="3mpKCXPfFCX" role="3o6s8t" />
          <node concept="2pNNFK" id="3mpKCXPfFGi" role="3o6s8t">
            <property role="2pNNFO" value="th" />
            <node concept="3o6iSG" id="3mpKCXPfFN3" role="3o6s8t">
              <property role="3o6i5n" value="Mon" />
            </node>
          </node>
          <node concept="2pNNFK" id="3mpKCXPfFIo" role="3o6s8t">
            <property role="2pNNFO" value="td" />
            <node concept="3o6iSG" id="3mpKCXPfFXf" role="3o6s8t">
              <property role="3o6i5n" value="Lasagna" />
              <node concept="2b32R4" id="3mpKCXPfH_I" role="lGtFl">
                <node concept="3JmXsc" id="3mpKCXPfH_J" role="2P8S$">
                  <node concept="3clFbS" id="3mpKCXPfH_K" role="2VODD2">
                    <node concept="3clFbF" id="3mpKCXPfHGh" role="3cqZAp">
                      <node concept="2OqwBi" id="3mpKCXPfIzH" role="3clFbG">
                        <node concept="2OqwBi" id="3mpKCXPfHUt" role="2Oq$k0">
                          <node concept="30H73N" id="3mpKCXPfHGg" role="2Oq$k0" />
                          <node concept="3TrEf2" id="3mpKCXPfIjv" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:5mCwD4fOH8F" resolve="monday" />
                          </node>
                        </node>
                        <node concept="3Tsc0h" id="3mpKCXPfIKB" role="2OqNvi">
                          <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2pNNFK" id="3mpKCXPfMrK" role="3o6s8t">
          <property role="2pNNFO" value="tr" />
          <node concept="3o6iSG" id="3mpKCXPfMrL" role="3o6s8t" />
          <node concept="2pNNFK" id="3mpKCXPfMrM" role="3o6s8t">
            <property role="2pNNFO" value="th" />
            <node concept="3o6iSG" id="3mpKCXPfMrN" role="3o6s8t">
              <property role="3o6i5n" value="Tue" />
            </node>
          </node>
          <node concept="2pNNFK" id="3mpKCXPfMrO" role="3o6s8t">
            <property role="2pNNFO" value="td" />
            <node concept="3o6iSG" id="3mpKCXPfMrP" role="3o6s8t">
              <property role="3o6i5n" value="Lasagna" />
              <node concept="2b32R4" id="3mpKCXPfMrQ" role="lGtFl">
                <node concept="3JmXsc" id="3mpKCXPfMrR" role="2P8S$">
                  <node concept="3clFbS" id="3mpKCXPfMrS" role="2VODD2">
                    <node concept="3clFbF" id="3mpKCXPfMrT" role="3cqZAp">
                      <node concept="2OqwBi" id="3mpKCXPfMrU" role="3clFbG">
                        <node concept="2OqwBi" id="3mpKCXPfMrV" role="2Oq$k0">
                          <node concept="30H73N" id="3mpKCXPfMrW" role="2Oq$k0" />
                          <node concept="3TrEf2" id="3mpKCXPfMrX" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:5mCwD4fOHfe" resolve="tuesday" />
                          </node>
                        </node>
                        <node concept="3Tsc0h" id="3mpKCXPfMrY" role="2OqNvi">
                          <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2pNNFK" id="3mpKCXPfMEl" role="3o6s8t">
          <property role="2pNNFO" value="tr" />
          <node concept="3o6iSG" id="3mpKCXPfMEm" role="3o6s8t" />
          <node concept="2pNNFK" id="3mpKCXPfMEn" role="3o6s8t">
            <property role="2pNNFO" value="th" />
            <node concept="3o6iSG" id="3mpKCXPfMEo" role="3o6s8t">
              <property role="3o6i5n" value="Wed" />
            </node>
          </node>
          <node concept="2pNNFK" id="3mpKCXPfMEp" role="3o6s8t">
            <property role="2pNNFO" value="td" />
            <node concept="3o6iSG" id="3mpKCXPfMEq" role="3o6s8t">
              <property role="3o6i5n" value="Lasagna" />
              <node concept="2b32R4" id="3mpKCXPfMEr" role="lGtFl">
                <node concept="3JmXsc" id="3mpKCXPfMEs" role="2P8S$">
                  <node concept="3clFbS" id="3mpKCXPfMEt" role="2VODD2">
                    <node concept="3clFbF" id="3mpKCXPfMEu" role="3cqZAp">
                      <node concept="2OqwBi" id="3mpKCXPfMEv" role="3clFbG">
                        <node concept="2OqwBi" id="3mpKCXPfMEw" role="2Oq$k0">
                          <node concept="30H73N" id="3mpKCXPfMEx" role="2Oq$k0" />
                          <node concept="3TrEf2" id="3mpKCXPfMEy" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:5mCwD4fOHk$" resolve="wednesday" />
                          </node>
                        </node>
                        <node concept="3Tsc0h" id="3mpKCXPfMEz" role="2OqNvi">
                          <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2pNNFK" id="3mpKCXPfMYo" role="3o6s8t">
          <property role="2pNNFO" value="tr" />
          <node concept="3o6iSG" id="3mpKCXPfMYp" role="3o6s8t" />
          <node concept="2pNNFK" id="3mpKCXPfMYq" role="3o6s8t">
            <property role="2pNNFO" value="th" />
            <node concept="3o6iSG" id="3mpKCXPfMYr" role="3o6s8t">
              <property role="3o6i5n" value="Thu" />
            </node>
          </node>
          <node concept="2pNNFK" id="3mpKCXPfMYs" role="3o6s8t">
            <property role="2pNNFO" value="td" />
            <node concept="3o6iSG" id="3mpKCXPfMYt" role="3o6s8t">
              <property role="3o6i5n" value="Lasagna" />
              <node concept="2b32R4" id="3mpKCXPfMYu" role="lGtFl">
                <node concept="3JmXsc" id="3mpKCXPfMYv" role="2P8S$">
                  <node concept="3clFbS" id="3mpKCXPfMYw" role="2VODD2">
                    <node concept="3clFbF" id="3mpKCXPfMYx" role="3cqZAp">
                      <node concept="2OqwBi" id="3mpKCXPfMYy" role="3clFbG">
                        <node concept="2OqwBi" id="3mpKCXPfMYz" role="2Oq$k0">
                          <node concept="30H73N" id="3mpKCXPfMY$" role="2Oq$k0" />
                          <node concept="3TrEf2" id="3mpKCXPfMY_" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:5mCwD4fOHuQ" resolve="thursday" />
                          </node>
                        </node>
                        <node concept="3Tsc0h" id="3mpKCXPfMYA" role="2OqNvi">
                          <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2pNNFK" id="3mpKCXPfN7h" role="3o6s8t">
          <property role="2pNNFO" value="tr" />
          <node concept="3o6iSG" id="3mpKCXPfN7i" role="3o6s8t" />
          <node concept="2pNNFK" id="3mpKCXPfN7j" role="3o6s8t">
            <property role="2pNNFO" value="th" />
            <node concept="3o6iSG" id="3mpKCXPfN7k" role="3o6s8t">
              <property role="3o6i5n" value="Fri" />
            </node>
          </node>
          <node concept="2pNNFK" id="3mpKCXPfN7l" role="3o6s8t">
            <property role="2pNNFO" value="td" />
            <node concept="3o6iSG" id="3mpKCXPfN7m" role="3o6s8t">
              <property role="3o6i5n" value="Lasagna" />
              <node concept="2b32R4" id="3mpKCXPfN7n" role="lGtFl">
                <node concept="3JmXsc" id="3mpKCXPfN7o" role="2P8S$">
                  <node concept="3clFbS" id="3mpKCXPfN7p" role="2VODD2">
                    <node concept="3clFbF" id="3mpKCXPfN7q" role="3cqZAp">
                      <node concept="2OqwBi" id="3mpKCXPfN7r" role="3clFbG">
                        <node concept="2OqwBi" id="3mpKCXPfN7s" role="2Oq$k0">
                          <node concept="30H73N" id="3mpKCXPfN7t" role="2Oq$k0" />
                          <node concept="3TrEf2" id="3mpKCXPfN7u" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:5mCwD4fOHAf" resolve="friday" />
                          </node>
                        </node>
                        <node concept="3Tsc0h" id="3mpKCXPfN7v" role="2OqNvi">
                          <ref role="3TtcxE" to="u6v3:5mCwD4fO2Nb" resolve="meals" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="raruj" id="7mpslIOnjCU" role="lGtFl" />
    </node>
  </node>
  <node concept="13MO4I" id="7mpslIOoauS">
    <property role="TrG5h" value="reduce_Recipe" />
    <ref role="3gUMe" to="u6v3:6dt6wfGrvhK" resolve="Recipe" />
    <node concept="2pNNFK" id="7mpslIOoa$D" role="13RCb5">
      <property role="2pNNFO" value="section" />
      <node concept="2pNUuL" id="3mpKCXRpNve" role="2pNNFR">
        <property role="2pNUuO" value="id" />
        <node concept="2pMdtt" id="3mpKCXRpNvf" role="2pMdts">
          <property role="2pMdty" value="Lasagna" />
          <node concept="3_AbJx" id="3mpKCXRqOJB" role="lGtFl">
            <ref role="2rW$FS" node="3mpKCXRpNda" resolve="links" />
            <node concept="3_AbJw" id="3mpKCXRqOJC" role="3_A0Ny">
              <node concept="3clFbS" id="3mpKCXRqOJD" role="2VODD2">
                <node concept="3cpWs8" id="3mpKCXRqQO6" role="3cqZAp">
                  <node concept="3cpWsn" id="3mpKCXRqQO9" role="3cpWs9">
                    <property role="TrG5h" value="uid" />
                    <node concept="17QB3L" id="3mpKCXRqQO4" role="1tU5fm" />
                    <node concept="2OqwBi" id="3mpKCXRqQrU" role="33vP2m">
                      <node concept="1iwH7S" id="3mpKCXRqQh9" role="2Oq$k0" />
                      <node concept="1AYpvF" id="3mpKCXRqQyd" role="2OqNvi">
                        <node concept="30H73N" id="3mpKCXRqQ$f" role="2QPDDZ" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3mpKCXRqOPP" role="3cqZAp">
                  <node concept="2pJPEk" id="3mpKCXRqOPN" role="3clFbG">
                    <node concept="2pJPED" id="3mpKCXRqOPO" role="2pJPEn">
                      <ref role="2pJxaS" to="iuxj:5M4a$b5jfOv" resolve="XmlTextValue" />
                      <node concept="2pJxcG" id="3mpKCXRqPpU" role="2pJxcM">
                        <ref role="2pJxcJ" to="iuxj:5M4a$b5jfOw" resolve="text" />
                        <node concept="WxPPo" id="3mpKCXRqPsM" role="28ntcv">
                          <node concept="37vLTw" id="3mpKCXRqRx1" role="WxPPp">
                            <ref role="3cqZAo" node="3mpKCXRqQO9" resolve="uid" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3o6iSG" id="7mpslIOoaBV" role="3o6s8t" />
      <node concept="2pNNFK" id="7mpslIOoaFE" role="3o6s8t">
        <property role="2pNNFO" value="h3" />
        <node concept="3o6iSG" id="7mpslIOoaGy" role="3o6s8t">
          <property role="3o6i5n" value="Lasagna" />
          <node concept="17Uvod" id="3mpKCXPbgOG" role="lGtFl">
            <property role="2qtEX9" value="value" />
            <property role="P4ACc" value="479c7a8c-02f9-43b5-9139-d910cb22f298/1622293396948952339/1622293396948953704" />
            <node concept="3zFVjK" id="3mpKCXPbgOJ" role="3zH0cK">
              <node concept="3clFbS" id="3mpKCXPbgOK" role="2VODD2">
                <node concept="3clFbF" id="3mpKCXPbgOQ" role="3cqZAp">
                  <node concept="2OqwBi" id="3mpKCXPbgOL" role="3clFbG">
                    <node concept="3TrcHB" id="3mpKCXPbgOO" role="2OqNvi">
                      <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                    </node>
                    <node concept="30H73N" id="3mpKCXPbgOP" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1Aj0xv" id="3mpKCXQeLn$" role="3o6s8t">
          <property role="3o6i5n" value=" Ingredients" />
        </node>
      </node>
      <node concept="2pNNFK" id="3mpKCXPc0fe" role="3o6s8t">
        <property role="2pNNFO" value="ul" />
        <node concept="3o6iSG" id="3mpKCXPc0oU" role="3o6s8t" />
        <node concept="2pNNFK" id="3mpKCXPc0rr" role="3o6s8t">
          <property role="2pNNFO" value="li" />
          <node concept="2b32R4" id="3mpKCXPc0F5" role="lGtFl">
            <node concept="3JmXsc" id="3mpKCXPc0F8" role="2P8S$">
              <node concept="3clFbS" id="3mpKCXPc0F9" role="2VODD2">
                <node concept="3clFbF" id="3mpKCXPc0Ff" role="3cqZAp">
                  <node concept="2OqwBi" id="3mpKCXPc0Fa" role="3clFbG">
                    <node concept="3Tsc0h" id="3mpKCXPc0Fd" role="2OqNvi">
                      <ref role="3TtcxE" to="u6v3:6dt6wfGrvix" resolve="ingredients" />
                    </node>
                    <node concept="30H73N" id="3mpKCXPc0Fe" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3o6iSG" id="3mpKCXPc0Jt" role="3o6s8t">
            <property role="3o6i5n" value="500g Tomatoes" />
          </node>
        </node>
      </node>
      <node concept="2pNNFK" id="7moC6rlUFSJ" role="3o6s8t">
        <property role="2pNNFO" value="p" />
        <node concept="3o6iSG" id="7moC6rlYqfG" role="3o6s8t">
          <property role="3o6i5n" value="Total calories: " />
        </node>
        <node concept="1Aj0xv" id="7moC6rlYrex" role="3o6s8t">
          <property role="3o6i5n" value="1450kJ" />
          <node concept="17Uvod" id="7moC6rlYrey" role="lGtFl">
            <property role="2qtEX9" value="value" />
            <property role="P4ACc" value="479c7a8c-02f9-43b5-9139-d910cb22f298/1622293396948952339/1622293396948953704" />
            <node concept="3zFVjK" id="7moC6rlYrez" role="3zH0cK">
              <node concept="3clFbS" id="7moC6rlYre$" role="2VODD2">
                <node concept="3clFbF" id="7moC6rlYre_" role="3cqZAp">
                  <node concept="3cpWs3" id="7moC6rlYreA" role="3clFbG">
                    <node concept="Xl_RD" id="7moC6rlYreB" role="3uHU7w">
                      <property role="Xl_RC" value="kJ" />
                    </node>
                    <node concept="2OqwBi" id="7moC6rlYreE" role="3uHU7B">
                      <node concept="30H73N" id="7moC6rlYreF" role="2Oq$k0" />
                      <node concept="2qgKlT" id="7moC6rlYreG" role="2OqNvi">
                        <ref role="37wK5l" to="xqb1:8EeBQEuDge" resolve="calories" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="raruj" id="7mpslIOoaP9" role="lGtFl" />
    </node>
  </node>
  <node concept="13MO4I" id="3mpKCXPc1ZD">
    <property role="TrG5h" value="reduce_MeasuredIngedient" />
    <ref role="3gUMe" to="u6v3:6dt6wfGrvip" resolve="MeasuredIngedient" />
    <node concept="2pNNFK" id="3mpKCXPc2f$" role="13RCb5">
      <property role="2pNNFO" value="li" />
      <node concept="3o6iSG" id="3mpKCXPc2gO" role="3o6s8t">
        <property role="3o6i5n" value="500g Tomatoes" />
        <node concept="17Uvod" id="3mpKCXPc2RR" role="lGtFl">
          <property role="2qtEX9" value="value" />
          <property role="P4ACc" value="479c7a8c-02f9-43b5-9139-d910cb22f298/1622293396948952339/1622293396948953704" />
          <node concept="3zFVjK" id="3mpKCXPc2RS" role="3zH0cK">
            <node concept="3clFbS" id="3mpKCXPc2RT" role="2VODD2">
              <node concept="3clFbF" id="3mpKCXPc30e" role="3cqZAp">
                <node concept="3cpWs3" id="3mpKCXREwdh" role="3clFbG">
                  <node concept="3cpWs3" id="5boToVNknJc" role="3uHU7B">
                    <node concept="Xl_RD" id="3mpKCXPc5uM" role="3uHU7w">
                      <property role="Xl_RC" value=" " />
                    </node>
                    <node concept="3cpWs3" id="3mpKCXPc5j1" role="3uHU7B">
                      <node concept="2OqwBi" id="3mpKCXPc3fZ" role="3uHU7B">
                        <node concept="3TrcHB" id="3mpKCXPc3zy" role="2OqNvi">
                          <ref role="3TsBF5" to="u6v3:6dt6wfGrviq" resolve="amount" />
                        </node>
                        <node concept="30H73N" id="3mpKCXPc48F" role="2Oq$k0" />
                      </node>
                      <node concept="2OqwBi" id="5boToVNkph_" role="3uHU7w">
                        <node concept="2OqwBi" id="5boToVNkoKl" role="2Oq$k0">
                          <node concept="2OqwBi" id="5boToVNkoaW" role="2Oq$k0">
                            <node concept="30H73N" id="5boToVNknNi" role="2Oq$k0" />
                            <node concept="3TrEf2" id="5boToVNkoxz" role="2OqNvi">
                              <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                            </node>
                          </node>
                          <node concept="3TrEf2" id="5boToVNkoZ_" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:5boToVMSz92" resolve="unit" />
                          </node>
                        </node>
                        <node concept="3TrcHB" id="5boToVNkpFu" role="2OqNvi">
                          <ref role="3TsBF5" to="u6v3:5boToVMJTj6" resolve="symbol" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="3mpKCXQoCFH" role="3uHU7w">
                    <node concept="2OqwBi" id="3mpKCXQoCFI" role="2Oq$k0">
                      <node concept="30H73N" id="3mpKCXQoCFJ" role="2Oq$k0" />
                      <node concept="3TrEf2" id="3mpKCXQoCFK" role="2OqNvi">
                        <ref role="3Tt5mk" to="u6v3:6dt6wfGrviu" resolve="ingedient" />
                      </node>
                    </node>
                    <node concept="3TrcHB" id="3mpKCXQoCFL" role="2OqNvi">
                      <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="raruj" id="3mpKCXPc6iU" role="lGtFl" />
    </node>
  </node>
  <node concept="13MO4I" id="3mpKCXPCiYO">
    <property role="TrG5h" value="reduce_Meal" />
    <ref role="3gUMe" to="u6v3:5mCwD4fO2$5" resolve="Meal" />
    <node concept="2pNNFK" id="3mpKCXPCj8b" role="13RCb5">
      <property role="2pNNFO" value="li" />
      <node concept="2pNNFK" id="3mpKCXR8e51" role="3o6s8t">
        <property role="2pNNFO" value="a" />
        <node concept="2pNUuL" id="3mpKCXR8exa" role="2pNNFR">
          <property role="2pNUuO" value="href" />
          <node concept="2pMdtt" id="3mpKCXR$JTj" role="2pMdts">
            <property role="2pMdty" value="#" />
          </node>
          <node concept="1UJgpG" id="3mpKCXR$K1b" role="2pMdts" />
          <node concept="2pMdtt" id="3mpKCXR8exb" role="2pMdts">
            <property role="2pMdty" value="Lasagna" />
            <node concept="1pdMLZ" id="3mpKCXRsmJb" role="lGtFl">
              <node concept="2kFOW8" id="3mpKCXRvCd1" role="2kGFt3">
                <node concept="3clFbS" id="3mpKCXRvCd2" role="2VODD2">
                  <node concept="3clFbF" id="3mpKCXRvCfu" role="3cqZAp">
                    <node concept="2OqwBi" id="3mpKCXRzRml" role="3clFbG">
                      <node concept="2OqwBi" id="3mpKCXRvCry" role="2Oq$k0">
                        <node concept="1iwH7S" id="3mpKCXRvCft" role="2Oq$k0" />
                        <node concept="1iwH70" id="3mpKCXRvCya" role="2OqNvi">
                          <ref role="1iwH77" node="3mpKCXRpNda" resolve="links" />
                          <node concept="2OqwBi" id="3mpKCXRDC5K" role="1iwH7V">
                            <node concept="30H73N" id="3mpKCXRvCMX" role="2Oq$k0" />
                            <node concept="3TrEf2" id="3mpKCXRDCj7" role="2OqNvi">
                              <ref role="3Tt5mk" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="1$rogu" id="3mpKCXRzRry" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3o6iSG" id="3mpKCXPCj9P" role="3o6s8t">
          <property role="3o6i5n" value="Lasagna" />
          <node concept="17Uvod" id="3mpKCXPCjhg" role="lGtFl">
            <property role="2qtEX9" value="value" />
            <property role="P4ACc" value="479c7a8c-02f9-43b5-9139-d910cb22f298/1622293396948952339/1622293396948953704" />
            <node concept="3zFVjK" id="3mpKCXPCjhh" role="3zH0cK">
              <node concept="3clFbS" id="3mpKCXPCjhi" role="2VODD2">
                <node concept="3cpWs8" id="3mpKCXQPQux" role="3cqZAp">
                  <node concept="3cpWsn" id="3mpKCXQPQu$" role="3cpWs9">
                    <property role="TrG5h" value="nnbsp" />
                    <node concept="17QB3L" id="3mpKCXQPQuv" role="1tU5fm" />
                    <node concept="Xl_RD" id="3mpKCXQOYtr" role="33vP2m">
                      <property role="Xl_RC" value=" " />
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="3mpKCXPCl4U" role="3cqZAp">
                  <node concept="3cpWsn" id="3mpKCXPCl4X" role="3cpWs9">
                    <property role="TrG5h" value="emoji" />
                    <node concept="17QB3L" id="3mpKCXPCl4S" role="1tU5fm" />
                    <node concept="2OqwBi" id="3mpKCXPCljm" role="33vP2m">
                      <node concept="2OqwBi" id="3mpKCXPCldD" role="2Oq$k0">
                        <node concept="30H73N" id="3mpKCXPCld0" role="2Oq$k0" />
                        <node concept="3TrEf2" id="3mpKCXPClhC" role="2OqNvi">
                          <ref role="3Tt5mk" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                        </node>
                      </node>
                      <node concept="2qgKlT" id="3mpKCXPClqg" role="2OqNvi">
                        <ref role="37wK5l" to="xqb1:8EeBQEukI3" resolve="diet_emoji" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3mpKCXPCjoN" role="3cqZAp">
                  <node concept="3cpWs3" id="3mpKCXQOYqK" role="3clFbG">
                    <node concept="37vLTw" id="3mpKCXPCoR8" role="3uHU7w">
                      <ref role="3cqZAo" node="3mpKCXPCl4X" resolve="emoji" />
                    </node>
                    <node concept="3cpWs3" id="3mpKCXPCkPi" role="3uHU7B">
                      <node concept="2OqwBi" id="3mpKCXPCk0e" role="3uHU7B">
                        <node concept="2OqwBi" id="3mpKCXPCjC$" role="2Oq$k0">
                          <node concept="30H73N" id="3mpKCXPCjoM" role="2Oq$k0" />
                          <node concept="3TrEf2" id="3mpKCXPCjOb" role="2OqNvi">
                            <ref role="3Tt5mk" to="u6v3:5mCwD4fO2GC" resolve="recipe" />
                          </node>
                        </node>
                        <node concept="3TrcHB" id="3mpKCXPCkio" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="3mpKCXQPR5e" role="3uHU7w">
                        <ref role="3cqZAo" node="3mpKCXQPQu$" resolve="nnbsp" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="raruj" id="3mpKCXRaJUV" role="lGtFl" />
      </node>
    </node>
  </node>
</model>


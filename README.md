# Canteen DSL

MPS-based DSL for a canteen’s meal planning. Includes managing ingredients and
generating an overview website.

## Usage

The project is created in MPS 2022.2 and requires no further dependencies. Open
the project’s base directory in MPS to find the canteen DSL and an example meal
planning solution.

## Task Description: The Canteen

The staff canteen at your workplace needs help with managing their menus and
wants you to develop a DSL based modeling tool for this purpose.

The manager wants to use the tool to
- write down recipes for meals by listing the required amount of each ingredient 
- maintain the list of possible ingredients, and specify for each ingredient its
  calories and whether it’s suitable for vegetarians/vegans
- write down the daily menus for the coming weeks in the tool. On each day,
  there has to be at least one meal option suitable for vegetarians, and no meal
  may be served on more than one day per week.
- export menu plans created in the tool as html files that he could put on the
  canteen website so that they can be viewed in the browser. This website should
  show for each day of the week the available meals, and for each of them
  whether it’s vegetarian/vegan, and the calories of a single serving. From each
  meal in the menus, there should be a link allowing navigation to a list of its
  ingredients. (Doesn’t have to look fancy, but the information and structure
  have to be there)
- (optional) let it automatically create suggestions for the menus for the next
  week, based on the ingredients they have in stock at the moment and the number
  of visitors they expect. If no valid menu can be created, the tool should make
  suggestions on which ingredients to buy to resolve the problem.

It’s up to you how you solve the given task. Choose technologies (programming
language, language workbench, libraries, frameworks, …) you consider to be
suitable. Please make sure to include instructions on how to build and run your
solution for your reviewers.

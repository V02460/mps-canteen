<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d6618aef-265e-4376-a844-1e50f007375a(de.kaialexhiller.canteen.mealplanner.meals)">
  <persistence version="9" />
  <languages>
    <use id="a8f77375-6785-47a0-8a3d-142ab816cb53" name="de.kaialexhiller.canteen" version="0" />
  </languages>
  <imports>
    <import index="cdrp" ref="r:270557f3-36cf-46df-9cab-7e28678872b1(de.kaialexhiller.canteen.units)" />
  </imports>
  <registry>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="a8f77375-6785-47a0-8a3d-142ab816cb53" name="de.kaialexhiller.canteen">
      <concept id="7159907586742787926" name="de.kaialexhiller.canteen.structure.Ingredient" flags="ng" index="2fc8ll">
        <property id="7159907586742809733" name="diet" index="2fceU6" />
        <property id="7159907586742809736" name="calories" index="2fceUb" />
        <property id="5969773708205126990" name="to_gram_divisor" index="2xO930" />
        <property id="5969773708205125985" name="to_gram_dividend" index="2xO9jJ" />
        <reference id="5969773708202619458" name="unit" index="2weHvc" />
      </concept>
      <concept id="7159907586742809712" name="de.kaialexhiller.canteen.structure.Recipe" flags="ng" index="2fceTN">
        <property id="7159907586742809766" name="instructions" index="2fceU_" />
        <child id="7159907586742809761" name="ingredients" index="2fceUy" />
      </concept>
      <concept id="7159907586742809753" name="de.kaialexhiller.canteen.structure.MeasuredIngedient" flags="ng" index="2fceUq">
        <property id="7159907586742809754" name="amount" index="2fceUp" />
        <reference id="7159907586742809758" name="ingedient" index="2fceUt" />
      </concept>
      <concept id="7159907586742809782" name="de.kaialexhiller.canteen.structure.DailyMenu" flags="ng" index="2fceUP">
        <child id="6172326848858434763" name="meals" index="1quaTq" />
      </concept>
      <concept id="8473699062435576974" name="de.kaialexhiller.canteen.structure.Pantry" flags="ng" index="2z$dNC">
        <child id="8473699062435577469" name="staples" index="2z$dSr" />
      </concept>
      <concept id="6172326848858422370" name="de.kaialexhiller.canteen.structure.MealPlan" flags="ng" index="1qpRVN">
        <property id="8473699062437320260" name="number_of_persons" index="2zIVoy" />
        <child id="6172326848858460095" name="meal_plans" index="1qu0OI" />
      </concept>
      <concept id="6172326848858433797" name="de.kaialexhiller.canteen.structure.Meal" flags="ng" index="1quaIk">
        <reference id="6172326848858434344" name="recipe" index="1quaAT" />
      </concept>
      <concept id="6172326848858451786" name="de.kaialexhiller.canteen.structure.WeekMealPlan" flags="ng" index="1queRr">
        <property id="6172326848858458765" name="calendar_week" index="1qu0ws" />
        <property id="6172326848858458321" name="year" index="1qu0D0" />
        <child id="6172326848858608171" name="monday" index="1qu_2U" />
        <child id="6172326848858608590" name="tuesday" index="1qu_5v" />
        <child id="6172326848858609590" name="thursday" index="1qu_kB" />
        <child id="6172326848858608932" name="wednesday" index="1qu_uP" />
        <child id="6172326848858610063" name="friday" index="1qu_Gu" />
      </concept>
    </language>
  </registry>
  <node concept="1qpRVN" id="5mCwD4fO0ye">
    <property role="2zIVoy" value="40" />
    <node concept="1queRr" id="5mCwD4fO9QB" role="1qu0OI">
      <property role="1qu0D0" value="2024" />
      <property role="1qu0ws" value="1" />
      <node concept="2fceUP" id="5mCwD4fONxu" role="1qu_2U">
        <node concept="1quaIk" id="5mCwD4fON_f" role="1quaTq">
          <ref role="1quaAT" node="5mCwD4fO1zS" resolve="Lasagna" />
        </node>
        <node concept="1quaIk" id="5mCwD4fON_h" role="1quaTq">
          <ref role="1quaAT" node="5mCwD4fOH6t" resolve="Pizza" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONxw" role="1qu_5v">
        <node concept="1quaIk" id="8EeBQEssED" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEA" resolve="Mashed Potatoes" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONxy" role="1qu_uP">
        <node concept="1quaIk" id="8EeBQEssEE" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEs99C" resolve="Pea Stew" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONx$" role="1qu_kB">
        <node concept="1quaIk" id="8EeBQEssEF" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEw" resolve="Tomato Soup" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONxA" role="1qu_Gu">
        <node concept="1quaIk" id="8EeBQEssEN" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEH" resolve="Fried Rice" />
        </node>
      </node>
    </node>
    <node concept="1queRr" id="5mCwD4fOvqM" role="1qu0OI">
      <property role="1qu0D0" value="2024" />
      <property role="1qu0ws" value="2" />
      <node concept="2fceUP" id="5mCwD4fONxO" role="1qu_2U">
        <node concept="1quaIk" id="8EeBQEssEO" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEA" resolve="Mashed Potatoes" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONxQ" role="1qu_5v">
        <node concept="1quaIk" id="8EeBQEssEP" role="1quaTq">
          <ref role="1quaAT" node="5mCwD4fO1zS" resolve="Lasagna" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONxS" role="1qu_uP">
        <node concept="1quaIk" id="8EeBQEssEQ" role="1quaTq">
          <ref role="1quaAT" node="5mCwD4fOH6t" resolve="Pizza" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONxU" role="1qu_kB">
        <node concept="1quaIk" id="8EeBQEssER" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEw" resolve="Tomato Soup" />
        </node>
        <node concept="1quaIk" id="8EeBQEssEZ" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEs99C" resolve="Pea Stew" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONxW" role="1qu_Gu">
        <node concept="1quaIk" id="8EeBQEssES" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEH" resolve="Fried Rice" />
        </node>
      </node>
    </node>
    <node concept="1queRr" id="5mCwD4fOvqS" role="1qu0OI">
      <property role="1qu0D0" value="2024" />
      <property role="1qu0ws" value="3" />
      <node concept="2fceUP" id="5mCwD4fONxY" role="1qu_2U">
        <node concept="1quaIk" id="8EeBQEssEU" role="1quaTq">
          <ref role="1quaAT" node="5mCwD4fO1zS" resolve="Lasagna" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONy0" role="1qu_5v">
        <node concept="1quaIk" id="8EeBQEssEV" role="1quaTq">
          <ref role="1quaAT" node="5mCwD4fOH6t" resolve="Pizza" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONy2" role="1qu_uP">
        <node concept="1quaIk" id="8EeBQEssEW" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEs99C" resolve="Pea Stew" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONy4" role="1qu_kB">
        <node concept="1quaIk" id="8EeBQEssEX" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEw" resolve="Tomato Soup" />
        </node>
      </node>
      <node concept="2fceUP" id="5mCwD4fONy6" role="1qu_Gu">
        <node concept="1quaIk" id="8EeBQEssET" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEH" resolve="Fried Rice" />
        </node>
        <node concept="1quaIk" id="8EeBQEssFc" role="1quaTq">
          <ref role="1quaAT" node="8EeBQEssEA" resolve="Mashed Potatoes" />
        </node>
      </node>
    </node>
  </node>
  <node concept="2fceTN" id="5mCwD4fO1zS">
    <property role="TrG5h" value="Lasagna" />
    <property role="3GE5qa" value="recipes" />
    <property role="2fceU_" value="Layer pasta and tomatoes, then add cheese. Put in oven for half an hour." />
    <node concept="2fceUq" id="5mCwD4fO1zT" role="2fceUy">
      <property role="2fceUp" value="100" />
      <ref role="2fceUt" node="5mCwD4fO4uq" resolve="Pasta" />
    </node>
    <node concept="2fceUq" id="5mCwD4fO4uv" role="2fceUy">
      <property role="2fceUp" value="200" />
      <ref role="2fceUt" node="5mCwD4fO4ur" resolve="Tomatoes" />
    </node>
    <node concept="2fceUq" id="5mCwD4fO4$f" role="2fceUy">
      <property role="2fceUp" value="150" />
      <ref role="2fceUt" node="5mCwD4fO4$e" resolve="Cheese" />
    </node>
  </node>
  <node concept="2fc8ll" id="5mCwD4fO4uq">
    <property role="TrG5h" value="Pasta" />
    <property role="2fceU6" value="6dt6wfGrvhT/Vegan" />
    <property role="2fceUb" value="1397" />
    <property role="3GE5qa" value="ingredients" />
    <ref role="2weHvc" to="cdrp:5boToVMQyDe" resolve="Gram" />
  </node>
  <node concept="2fc8ll" id="5mCwD4fO4ur">
    <property role="3GE5qa" value="ingredients" />
    <property role="TrG5h" value="Tomatoes" />
    <property role="2fceU6" value="6dt6wfGrvhT/Vegan" />
    <property role="2fceUb" value="71" />
    <ref role="2weHvc" to="cdrp:5boToVMQyDe" resolve="Gram" />
  </node>
  <node concept="2fc8ll" id="5mCwD4fO4$e">
    <property role="3GE5qa" value="ingredients" />
    <property role="TrG5h" value="Cheese" />
    <property role="2fceU6" value="6dt6wfGrvhV/Vegetarian" />
    <property role="2fceUb" value="1033" />
    <ref role="2weHvc" to="cdrp:5boToVMQyDe" resolve="Gram" />
  </node>
  <node concept="2fceTN" id="5mCwD4fOH6t">
    <property role="3GE5qa" value="recipes" />
    <property role="TrG5h" value="Pizza" />
    <property role="2fceU_" value="Prepare dough overnight. Form a pizza, add tomatoes and cheese. Bake on high heat." />
    <node concept="2fceUq" id="6gAUGziTFHc" role="2fceUy">
      <property role="2fceUp" value="75" />
      <ref role="2fceUt" node="5mCwD4fON_k" resolve="Wheat flour" />
    </node>
    <node concept="2fceUq" id="6gAUGziTFHg" role="2fceUy">
      <property role="2fceUp" value="40" />
      <ref role="2fceUt" node="5mCwD4fPCDc" resolve="Water" />
    </node>
    <node concept="2fceUq" id="5mCwD4fPCDd" role="2fceUy">
      <property role="2fceUp" value="200" />
      <ref role="2fceUt" node="5mCwD4fO4ur" resolve="Tomatoes" />
    </node>
    <node concept="2fceUq" id="5mCwD4fOK5g" role="2fceUy">
      <property role="2fceUp" value="150" />
      <ref role="2fceUt" node="5mCwD4fO4$e" resolve="Cheese" />
    </node>
  </node>
  <node concept="2fc8ll" id="5mCwD4fON_k">
    <property role="3GE5qa" value="ingredients" />
    <property role="TrG5h" value="Wheat flour" />
    <property role="2fceU6" value="6dt6wfGrvhT/Vegan" />
    <property role="2fceUb" value="1397" />
    <ref role="2weHvc" to="cdrp:5boToVMQyDe" resolve="Gram" />
  </node>
  <node concept="2fc8ll" id="5mCwD4fPCDc">
    <property role="3GE5qa" value="ingredients" />
    <property role="TrG5h" value="Water" />
    <property role="2fceU6" value="6dt6wfGrvhT/Vegan" />
    <property role="2fceUb" value="0" />
    <property role="2xO9jJ" value="1" />
    <property role="2xO930" value="1" />
    <ref role="2weHvc" to="cdrp:5boToVN25kF" resolve="Milliliter" />
  </node>
  <node concept="2fc8ll" id="8EeBQEs99A">
    <property role="3GE5qa" value="ingredients" />
    <property role="TrG5h" value="Rice" />
    <property role="2fceU6" value="6dt6wfGrvhT/Vegan" />
    <property role="2fceUb" value="1506" />
    <ref role="2weHvc" to="cdrp:5boToVMQyDe" resolve="Gram" />
  </node>
  <node concept="2fc8ll" id="8EeBQEs99B">
    <property role="3GE5qa" value="ingredients" />
    <property role="TrG5h" value="Peas" />
    <property role="2fceU6" value="6dt6wfGrvhT/Vegan" />
    <property role="2fceUb" value="1448" />
    <ref role="2weHvc" to="cdrp:5boToVMQyDe" resolve="Gram" />
  </node>
  <node concept="2fceTN" id="8EeBQEs99C">
    <property role="3GE5qa" value="recipes" />
    <property role="TrG5h" value="Pea Stew" />
    <property role="2fceU_" value="Add all ingredients to a bowl and cook for an hour." />
    <node concept="2fceUq" id="8EeBQEs99D" role="2fceUy">
      <property role="2fceUp" value="100" />
      <ref role="2fceUt" node="8EeBQEs99B" resolve="Peas" />
    </node>
    <node concept="2fceUq" id="8EeBQEs99H" role="2fceUy">
      <property role="2fceUp" value="100" />
      <ref role="2fceUt" node="8EeBQEs99F" resolve="Potatoes" />
    </node>
    <node concept="2fceUq" id="8EeBQEs99E" role="2fceUy">
      <property role="2fceUp" value="100" />
      <ref role="2fceUt" node="5mCwD4fPCDc" resolve="Water" />
    </node>
  </node>
  <node concept="2fc8ll" id="8EeBQEs99F">
    <property role="3GE5qa" value="ingredients" />
    <property role="TrG5h" value="Potatoes" />
    <property role="2fceU6" value="6dt6wfGrvhT/Vegan" />
    <property role="2fceUb" value="280" />
    <ref role="2weHvc" to="cdrp:5boToVMQyDe" resolve="Gram" />
  </node>
  <node concept="2fceTN" id="8EeBQEssEw">
    <property role="3GE5qa" value="recipes" />
    <property role="TrG5h" value="Tomato Soup" />
    <property role="2fceU_" value="Sauté onions in a bowl, then add remaining ingredients and cook for half an hour." />
    <node concept="2fceUq" id="8EeBQEssEx" role="2fceUy">
      <property role="2fceUp" value="200" />
      <ref role="2fceUt" node="5mCwD4fPCDc" resolve="Water" />
    </node>
    <node concept="2fceUq" id="8EeBQEssEy" role="2fceUy">
      <property role="2fceUp" value="40" />
      <ref role="2fceUt" node="8EeBQEssEz" resolve="Onions" />
    </node>
    <node concept="2fceUq" id="8EeBQEssE$" role="2fceUy">
      <property role="2fceUp" value="400" />
      <ref role="2fceUt" node="5mCwD4fO4ur" resolve="Tomatoes" />
    </node>
  </node>
  <node concept="2fc8ll" id="8EeBQEssEz">
    <property role="3GE5qa" value="ingredients" />
    <property role="TrG5h" value="Onions" />
    <property role="2fceU6" value="6dt6wfGrvhT/Vegan" />
    <property role="2fceUb" value="100" />
    <ref role="2weHvc" to="cdrp:5boToVMQyDe" resolve="Gram" />
  </node>
  <node concept="2fceTN" id="8EeBQEssEA">
    <property role="3GE5qa" value="recipes" />
    <property role="TrG5h" value="Mashed Potatoes" />
    <property role="2fceU_" value="Cook and mash potatoes." />
    <node concept="2fceUq" id="8EeBQEssEB" role="2fceUy">
      <property role="2fceUp" value="200" />
      <ref role="2fceUt" node="8EeBQEs99F" resolve="Potatoes" />
    </node>
    <node concept="2fceUq" id="8EeBQEssEC" role="2fceUy">
      <property role="2fceUp" value="20" />
      <ref role="2fceUt" node="5mCwD4fPCDc" resolve="Water" />
    </node>
  </node>
  <node concept="2fceTN" id="8EeBQEssEH">
    <property role="3GE5qa" value="recipes" />
    <property role="TrG5h" value="Fried Rice" />
    <property role="2fceU_" value="Cook rice while sautéing onions in a pan. Put together and add peas." />
    <node concept="2fceUq" id="8EeBQEssEI" role="2fceUy">
      <property role="2fceUp" value="150" />
      <ref role="2fceUt" node="8EeBQEs99A" resolve="Rice" />
    </node>
    <node concept="2fceUq" id="8EeBQEssEM" role="2fceUy">
      <property role="2fceUp" value="300" />
      <ref role="2fceUt" node="5mCwD4fPCDc" resolve="Water" />
    </node>
    <node concept="2fceUq" id="8EeBQEssEL" role="2fceUy">
      <property role="2fceUp" value="80" />
      <ref role="2fceUt" node="8EeBQEssEz" resolve="Onions" />
    </node>
    <node concept="2fceUq" id="8EeBQEssEJ" role="2fceUy">
      <property role="2fceUp" value="100" />
      <ref role="2fceUt" node="8EeBQEs99B" resolve="Peas" />
    </node>
  </node>
  <node concept="2z$dNC" id="7moC6rm2VaV">
    <node concept="2fceUq" id="1jE3pLvevwS" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="5mCwD4fO4$e" resolve="Cheese" />
    </node>
    <node concept="2fceUq" id="7moC6rm2VaY" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="8EeBQEssEz" resolve="Onions" />
    </node>
    <node concept="2fceUq" id="1jE3pLvevxQ" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="5mCwD4fO4uq" resolve="Pasta" />
    </node>
    <node concept="2fceUq" id="7moC6rm2Vb1" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="8EeBQEs99B" resolve="Peas" />
    </node>
    <node concept="2fceUq" id="1jE3pLvevwX" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="8EeBQEs99F" resolve="Potatoes" />
    </node>
    <node concept="2fceUq" id="1jE3pLvevyA" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="8EeBQEs99A" resolve="Rice" />
    </node>
    <node concept="2fceUq" id="7moC6rm2VaW" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="5mCwD4fO4ur" resolve="Tomatoes" />
    </node>
    <node concept="2fceUq" id="1jE3pLvevyJ" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="5mCwD4fPCDc" resolve="Water" />
    </node>
    <node concept="2fceUq" id="1jE3pLvevyT" role="2z$dSr">
      <property role="2fceUp" value="50000" />
      <ref role="2fceUt" node="5mCwD4fON_k" resolve="Wheat flour" />
    </node>
  </node>
</model>


<?xml version="1.0" encoding="UTF-8"?>
<solution name="de.kaialexhiller.canteen.mealplanner" uuid="10580a37-1835-4764-a11f-4268dd1d7495" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet compile="mps" classes="mps" ext="no" type="java" languageLevel="JAVA_10">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">a8f77375-6785-47a0-8a3d-142ab816cb53(de.kaialexhiller.canteen)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:a8f77375-6785-47a0-8a3d-142ab816cb53:de.kaialexhiller.canteen" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
  </languageVersions>
  <dependencyVersions>
    <module reference="3f233e7f-b8a6-46d2-a57f-795d56775243(Annotations)" version="0" />
    <module reference="6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)" version="0" />
    <module reference="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea(MPS.Core)" version="0" />
    <module reference="8865b7a8-5271-43d3-884c-6fd1d9cfdd34(MPS.OpenAPI)" version="0" />
    <module reference="a8f77375-6785-47a0-8a3d-142ab816cb53(de.kaialexhiller.canteen)" version="0" />
    <module reference="10580a37-1835-4764-a11f-4268dd1d7495(de.kaialexhiller.canteen.mealplanner)" version="0" />
    <module reference="ceab5195-25ea-4f22-9b92-103b95ca8c0c(jetbrains.mps.lang.core)" version="0" />
  </dependencyVersions>
</solution>

